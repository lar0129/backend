FROM dockerhub.lsmcloud.top/library/openjdk:17-alpine

# Install Python 3 and pip
RUN #apk add --no-cache python3 py3-pip

WORKDIR /app

# Copy the compiled Spring Boot application JAR file into the image*

COPY ./target/News_Server-0.0.1-SNAPSHOT.jar /app/News_Server-0.0.1-SNAPSHOT.jar

# Install Python packages
RUN #pip install --no-cache-dir zhipuai

# Expose the port your application runs on*
EXPOSE 8080

# Command to run your application*

CMD ["sh", "-c", "sleep 10 && java -jar News_Server-0.0.1-SNAPSHOT.jar"]