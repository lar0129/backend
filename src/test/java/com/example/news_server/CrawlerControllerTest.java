package com.example.news_server;

import com.example.news_server.VO.CrawledNewsVO;
import com.example.news_server.controller.CrawlerController;
import com.example.news_server.service.CrawlerService;
import com.example.news_server.service.SseService;
import com.example.news_server.utils.ApiResponse;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

class CrawlerControllerTest {

    @Mock
    private CrawlerService crawlerService;

    private CrawlerController crawlerController;

    private SseService sseService;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
        crawlerController = new CrawlerController(crawlerService,sseService);
    }

    @Test
    void testUploadNews_Successful() {
        List<CrawledNewsVO> newsList = new ArrayList<>();

        // 添加测试数据到newsList
        CrawledNewsVO news1 = new CrawledNewsVO();
        news1.setTitle("News Title 1");
        news1.setSource("News Source 1");
        news1.setDate(new Date());
        news1.setContent("News Content 1");
        news1.setUrl("https://example.com/news1");
        newsList.add(news1);

        int successfulCount = 5;
        when(crawlerService.addNews(newsList)).thenReturn(new ArrayList<>());


        ResponseEntity<ApiResponse<?>> responseEntity = crawlerController.uploadNews(newsList);

        assertEquals(HttpStatus.CREATED, responseEntity.getStatusCode());
        assertEquals(successfulCount, responseEntity.getBody().getData());
        assertEquals("News appended successfully", responseEntity.getBody().getMsg());
    }

    @Test
    void testUploadNews_EmptyNewsList() {
        List<CrawledNewsVO> newsList = new ArrayList<>();

        ResponseEntity<ApiResponse<?>> responseEntity = crawlerController.uploadNews(newsList);

        assertEquals(HttpStatus.BAD_REQUEST, responseEntity.getStatusCode());
        assertEquals("News list is empty.", responseEntity.getBody().getMsg());

        verifyNoInteractions(crawlerService);
    }

    @Test
    void testUploadNews_Exception() {
        List<CrawledNewsVO> newsList = new ArrayList<>();

        // 添加测试数据到newsList
        CrawledNewsVO news1 = new CrawledNewsVO();
        news1.setTitle("News Title 1");
        news1.setSource("News Source 1");
        news1.setDate(new Date());
        news1.setContent("News Content 1");
        news1.setUrl("https://example.com/news1");
        newsList.add(news1);

        // 这边通过了测试，只是会抛出一个Test Exception
        when(crawlerService.addNews(newsList)).thenThrow(new RuntimeException("Test Exception"));

        ResponseEntity<ApiResponse<?>> responseEntity = crawlerController.uploadNews(newsList);

        assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, responseEntity.getStatusCode());
        assertEquals("Error processing news data", responseEntity.getBody().getMsg());

        verify(crawlerService, times(1)).addNews(newsList);
    }
}
