package com.example.news_server;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Locale;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.example.news_server.PO.EventNode;
import com.example.news_server.VO.ParsedNREVO;
import com.example.news_server.controller.GraphController;
import com.example.news_server.repository.EventRepository;
import com.example.news_server.service.GraphService;
import java.text.ParseException;
import java.text.SimpleDateFormat;

@SpringBootTest
public class GraphControllerTest {

    // @Autowired
    // private GraphService graphService;

    @Autowired
    private GraphController graphController;

    @Autowired
    private EventRepository eventRepository;

    @Autowired
    private GraphService graphServiceImpl;

    @BeforeEach
    void setUp() {

    }

    @Test
    void testUploadNRE_Successful1() {
        ParsedNREVO nreList = new ParsedNREVO();
        nreList.setData(
                new ArrayList<>(Arrays.asList(new ArrayList<>(Arrays.asList("I", "Person", "1.0", "You", "Person")),
                        new ArrayList<>(Arrays.asList("I", "Person", "-1.0", "Him", "Person")))));
        graphController.uploadNRE(nreList);
    }

    @Test
    void testUploadNRE_Successful2() {
        ParsedNREVO nreList = new ParsedNREVO();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSSSSS");
        nreList.setData(new ArrayList<>(
                Arrays.asList(new ArrayList<>(Arrays.asList("debug", "Person", "0.36", "pupupu", "Idea")),
                        new ArrayList<>(Arrays.asList("gaga", "Person", "-1.0", "gege", "Event")))));
        nreList.setId(2000L);
        try {
            nreList.setDate(dateFormat.parse("2024-03-12 18:58:00.000000"));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        // nreList.setDate(null);
        graphController.uploadNRE(nreList);
    }

    @Test
    void testUploadNRE_Successful3() {
        ParsedNREVO nreList = new ParsedNREVO();
        nreList.setData(new ArrayList<>(
                Arrays.asList(new ArrayList<>(Arrays.asList("刘尧力", "Person", "0.36", "Love Sleeping", "Idea"))
                    )));
        nreList.setId(2000L);
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSSSSS");
        try {
            nreList.setDate(dateFormat.parse("2024-03-12 18:58:00.000000"));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        // nreList.setDate(null);
        nreList.setId(2000L);
        graphController.uploadNRE(nreList);
    }

    @Test
    void testUploadNRE_Fail1() {
        ParsedNREVO nreList = new ParsedNREVO();
        nreList.setData(new ArrayList<>(Arrays.asList(new ArrayList<>(Arrays.asList("I", "Cat", "1.0", "Love cats")),
                new ArrayList<>(Arrays.asList("I", "Person", "-1.0", "PiKaPika", "Boy")))));
        graphController.uploadNRE(nreList);
    }

    @Test
    void getGraphTest_1() {
        graphController.getGraph("201707", null);
    }

    @Test
    void getGraphTest_2() {
        graphController.getGraph(null, "上交所");
    }

    @Test
    void getGraphTest_3() {
        System.out.println(graphController.getRelatedNews(184L));
    }

    @Test
    void repositoryTest_1() {
        EventNode e = eventRepository.findOneByNameLimit50("欧元区工业产值");
        return;
    }
 

    @Test
    void dateFormateTest() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX", Locale.CHINA);
        SimpleDateFormat dateFormat_1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSSSSS");
        try {
            Date date = dateFormat_1.parse("2024-03-12 18:58:00.000000");
            System.out.println(dateFormat.format(date));
        } catch (ParseException e) {
            e.printStackTrace();

        }
    }

    @Test 
    void testDatetimeConvert(){
        
    }

}
