package com.example.news_server;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;


import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.*;
import java.text.*;

import com.example.news_server.VO.QuestionVO;
import com.example.news_server.amqp.CrawledNewsListener;
import com.example.news_server.controller.NewsController;
import com.example.news_server.controller.QAController;
import com.example.news_server.service.GraphService;
import com.example.news_server.service.QAService;
import com.example.news_server.service.impls.GraphServiceImpl;
import com.example.news_server.utils.ApiResponse;

@SpringBootTest
public class IntergratedTests {

    @Autowired
    private NewsController newsControllerReal;

    @Autowired
    private GraphService graphServiceReal;

    @Autowired
    private QAService qaServiceReal;

    @Autowired
    private QAController qaControllerReal;

    @Autowired
    private CrawledNewsListener crawledNewsListenerReal;

    @Test
    void getTimedNews_ValidParams_ReturnsNewsList() {
        // Arrange
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        Date startDate;
        Date endDate;
        try {
            startDate = format.parse("2024-04-01 18:58");
            endDate = format.parse("2024-04-04 12:20");
            // NewsListVO newsListVO = new NewsListVO();
            // when(newsService.getTimedNews(startDate, endDate)).thenReturn(newsListVO);

            // Act
            ResponseEntity<ApiResponse<?>> response = newsControllerReal.getTimedNews(startDate, endDate);

            // Assert
            assertEquals(HttpStatus.OK, response.getStatusCode());
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    @Test
    void analyse_nodes_result(){
        List<List<String>> nodes = new ArrayList<>();
        List<String> node1 = new ArrayList<>();
        node1.add("俄罗斯");
        node1.add("0.9");
        nodes.add(node1);
        List<String> node2 = new ArrayList<>();
        node2.add("美国");
        node2.add("0.8");
        nodes.add(node2);
        List<String> node3 = new ArrayList<>();
        node3.add("中国");
        node3.add("0.7");
        nodes.add(node3);
        graphServiceReal.getGraphFromQAExtracted(nodes);
    }

    @Test
    void process_question_test(){
        String uuid = qaControllerReal.openSSE().getBody().getData().toString();
        QuestionVO questionVO = new QuestionVO();
        questionVO.setQuestion("请问俄罗斯和美国的关系如何？");
        questionVO.setUuid(uuid);
        qaControllerReal.processQuestion(questionVO, null);
    }


    @Test
    void rabbitmqTest(){
        String jsonString = "[{\"news_title\": \"中东战地手记｜苏丹儿童企盼和平？\", \"news_source\": \"每日经济新闻\", \"news_time\": \"2024-06-01 20:26:39\",\"news_content\": \"当地时间6月2日晚，南非独立选举委员会在位于约翰内斯堡的全国计票中心公布了南非大选结果。南非总统拉马福萨和副总统马沙蒂莱等出席了结果宣布仪式。根据最终公布数据，共统计了全国约2700余万张选票。执政党非洲人国民大会(非国大)获得了159个议席。民主联盟位居第二，获得87个议席。民族之矛党获得58个议席，排名第三。经济自由斗士获得了39个议席，位列第四。本次选举将产生新一届国民议会和省级议会。这是南非自1994年结束种族隔离制度以来的第七次大选。选举结果的揭晓也意味着执政30年的非洲人国民大会(ANC)首次失去议会多数席位。根据南非宪法，总统将由国民议会选举产生。新一届国民议会产生后，国民议会议员将举行会议，选举新一届南非总统。由于非国大未能获得过半数的议会席位，必须联合其他政党以确保拉马福萨在新一届国民议会举行的总统选举中获胜。非国大需要与可能的联盟伙伴展开谈判，并在议会开幕前达成联合执政协议。预计这一过程将在接下来的两周内完成，届时将正式宣布组成联合政府。\", \"news_url\": \"https://finance.sina.cn/stock/relnews/hk/2024-06-01/detail-inaxfxmn4748488.d.html\" }]";
        crawledNewsListenerReal.receiveMessage(jsonString);
    }
}
