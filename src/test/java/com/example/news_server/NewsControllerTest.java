package com.example.news_server;

import com.example.news_server.VO.NewsListVO;
import com.example.news_server.VO.SummaryNews;
import com.example.news_server.controller.NewsController;
import com.example.news_server.service.NewsService;
import com.example.news_server.service.SseService;
import com.example.news_server.utils.ApiResponse;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.ArrayList;
import java.util.List;
import java.util.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

class NewsControllerTest {

    @Mock
    private NewsService newsService;

    @InjectMocks
    private NewsController newsController;
    private SseService sseService;


    @BeforeEach
    void setUp() {
        // 初始化测试环境
        newsService = mock(NewsService.class);
        newsController = new NewsController(newsService, sseService);
    }

    @Test
    void getNewsById_ValidId_ReturnsContent() {
        // Arrange
        Long id = 1L;
        String content = "News content";
        when(newsService.getNewsContent(id)).thenReturn(content);

        // Act
        ResponseEntity<ApiResponse<?>> response = newsController.getNewsById(id);

        // Assert
        assertEquals(HttpStatus.OK, response.getStatusCode());

        verify(newsService, times(1)).getNewsContent(id);
        verifyNoMoreInteractions(newsService);
    }

    @Test
    void getNewsById_InvalidId_ReturnsNotFound() {
        // Arrange
        Long id = 1L;
        when(newsService.getNewsContent(id)).thenReturn("");

        // Act
        ResponseEntity<ApiResponse<?>> response = newsController.getNewsById(id);

        // Assert
        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());

        verify(newsService, times(1)).getNewsContent(id);
        verifyNoMoreInteractions(newsService);
    }

    @Test
    void getNewsFilter_ValidParams_ReturnsFilteredNews() {
        // Arrange
        String sentence = "汇丰：等待中国四大电商的，是明年5000亿美元的海外市场";
        Long top = 10L;
        List<SummaryNews> newsList = new ArrayList<>();
        when(newsService.findNewsBySentence(sentence, top)).thenReturn(newsList);

        // Act
        ResponseEntity<ApiResponse<?>> response = newsController.getNewsFilter(sentence, top);

        // Assert
        assertEquals(HttpStatus.OK, response.getStatusCode());

        verify(newsService, times(1)).findNewsBySentence(sentence, top);
        verifyNoMoreInteractions(newsService);
    }

}