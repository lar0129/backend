import requests
import json
import sys
import jieba
import jieba.posseg as pseg


def read_background_from_file(file_path):
    with open(file_path, 'r') as file:
        return file.read()

def extract_person_names(text):
    jieba.load_userdict('src/main/resources/Prompt/custom_dict.txt')  # 加载自定义词典
    words = pseg.cut(text)  # 使用带词性标注的分词模式
    person_names = []
    for word, flag in words:
        if flag == 'nr':  # 词性为人名
            person_names.append(word)
    return person_names

def main():
    url = "https://aip.baidubce.com/rpc/2.0/ai_custom/v1/wenxinworkshop/chat/yi_34b_chat?access_token=" + "24.8370da45160089490baaf0c2da486e63.2592000.1717073450.282335-65480949"
    background = read_background_from_file("src/main/resources/Prompt/background4.txt")
    input_question = sys.stdin.read()
    if input_question == "":
            print("[]")
            return
    names = extract_person_names(input_question)
    # print(names)
    names = list(set(names))  # 使用set去重后转换回列表
    names_str = str(names)  # 将列表转换为字符串
    question = """
    问题：
    ================
    输入：
    """ + input_question + """
    输出：
    ================
    """
    payload = json.dumps({
        "messages": [
            {
                "role": "user",
                "content": background + question + "我给出一个人物列表[" + names_str + "]，请结合我给出的人物列表进行分析，注意，结果限定在50字以内"
            }
        ]
    })
    headers = {
        'Content-Type': 'application/json'
    }
    response = requests.request("POST", url, headers=headers, data=payload)

    response_json = response.json()
    result = response_json.get('result')

    if result:
        print("Response:", result)

if __name__ == '__main__':
    main()
