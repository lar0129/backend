package com.example.news_server.utils.LLM;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

public class SQLExtractor {

    public static String extractSQL(String response) {
        StringBuilder sql = new StringBuilder();
        boolean isSqlStarted = false;

        String[] parts = response.split("\n");
        for (String part : parts) {
            part = part.trim();
            if (!isSqlStarted && part.toLowerCase().startsWith("select ")) {
                isSqlStarted = true;
            }
            if (isSqlStarted) {
                sql.append(part).append(" ");
                if (part.contains(";")) {
                    break;
                }
            }
        }

        String sqlString = sql.toString().trim();
        // 确保SQL语句以分号结尾
        int endIndex = sqlString.indexOf(";");
        if (endIndex != -1) {
            return sqlString.substring(0, endIndex + 1).trim();
        }

        return sqlString;
    }


    public static void main(String[] args) {
        String jsonResponse = "SELECT (COUNT(CASE WHEN DATE(publish_date) = CURDATE() THEN 1 END) - COUNT(CASE WHEN DATE(publish_date) = CURDATE() - INTERVAL 1 DAY THEN 1 END)) AS change_in_political_news_count FROM news WHERE category = 10;<im_end>\n" +
                "\n" +
                "这个查询会返回一个包含变化数量的字段 `change_in_political_news_count`。如果今天的政治新闻数量大于昨天，这个字段将显示正数；如果少于昨天，将显示负数；如果数量相同，则显示为 0。";
        String sqlQuery = extractSQL(jsonResponse);
        System.out.println("Extracted SQL Query: " + sqlQuery);
    }
}
