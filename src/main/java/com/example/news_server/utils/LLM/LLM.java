package com.example.news_server.utils.LLM;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class LLM {
    public static void main(String[] args) {
        try {
            File directory = new File("");
            // System.out.println(directory.getCanonicalPath());
            // 定义要执行的 Python 脚本命令
            String pythonScript = "python3 " + directory.getCanonicalPath() + "/src/main/resources/Prompt/LLM.py\n";
            // System.out.println(pythonScript);

            // 设置要传递给Python脚本的输入问题
            // String inputQuestion = "习近平此次出访，是自从新冠疫情大流行之后首次欧洲行，差不多三年时间，在习近平清零不动摇防疫政策主导下，中国几乎切断了与世界的联系。习近平此行欧洲的访问安排也是很有用心的。在访问法国之后，将访问匈牙利和塞尔维亚。关于塞尔维亚，中国外交部发言人林剑周一毫不掩饰地称“塞尔维亚是中国在中东欧地区首个全面战略伙伴，“两国铁杆友谊深厚”。”塞尔维亚尚在敲欧盟的大门，舆论更关注的是欧盟成员国匈牙利。林剑将匈牙利描述为中东欧地区重要国家，并且是中方推进共建“一带一路”继中国-中东欧国家合作的重要伙伴。";
            String inputQuestion = "张军秘书长曾任中国常驻联合国代表、特命全权大使，具有丰富外交履历和国际组织工作经验。各界期待张军秘书长领导论坛秘书处推动论坛事业取得新的成就。\n" +
                    "\n" +
                    "据博鳌亚洲论坛官网介绍，博鳌亚洲论坛是一个总部设在中国的国际组织，由29个成员国共同发起，每年定期在海南博鳌举行年会。论坛成立的初衷，是促进亚洲经济一体化。论坛当今的使命，是为亚洲和世界发展凝聚正能量。";
            inputQuestion = LLMUtils.ExtractSummary(inputQuestion, 400);
            ProcessBuilder pb = new ProcessBuilder("bash", "-c", pythonScript);

            // 启动进程并等待完成
            Process process = pb.start();

            // 获取进程的输出流
            BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream()));

            // 将输入问题传递给Python脚本
            process.getOutputStream().write(inputQuestion.getBytes());
            process.getOutputStream().flush();
            process.getOutputStream().close();

            // 读取 Python 脚本的输出
            String line;
            StringBuilder output = new StringBuilder();
            while ((line = reader.readLine()) != null) {
                output.append(line).append("\n");
            }

            // 打印 Python 脚本的输出
            System.out.println("Python script output:\n" + extractListFromOutput(output.toString()));

            // 等待进程完成并获取退出值
            int exitCode = process.waitFor();

            // 打印进程的退出值
            System.out.println("Exit Code: " + exitCode);
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static void LLMFunction(String input){
        try {
            File directory = new File("");
            System.out.println(directory.getCanonicalPath());
            // 定义要执行的 Python 脚本命令
            String pythonScript = "python3 " + directory.getCanonicalPath() + "/src/main/resources/Prompt/LLM.py\n";
            System.out.println(pythonScript);

            // 设置要传递给Python脚本的输入问题
            String inputQuestion = input;

            // 创建进程构建器并设置命令
            ProcessBuilder pb = new ProcessBuilder("bash", "-c", pythonScript);

            // 启动进程并等待完成
            Process process = pb.start();

            // 获取进程的输出流
            BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream()));

            // 将输入问题传递给Python脚本
            process.getOutputStream().write(inputQuestion.getBytes());
            process.getOutputStream().flush();
            process.getOutputStream().close();

            // 读取 Python 脚本的输出
            String line;
            StringBuilder output = new StringBuilder();
            while ((line = reader.readLine()) != null) {
                output.append(line).append("\n");
            }

            // 打印 Python 脚本的输出
            System.out.println("Python script output:\n" + extractListFromOutput(output.toString()));

            // 等待进程完成并获取退出值
            int exitCode = process.waitFor();

            // 打印进程的退出值
            System.out.println("Exit Code: " + exitCode);
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
    }
    public static List<List<String>> extractListFromOutput(String output) {
        List<List<String>> extractedList = new ArrayList<>();

        // 使用正则表达式提取方括号内的列表部分
        Pattern pattern = Pattern.compile("\\[(.*?)\\]");
        Matcher matcher = pattern.matcher(output);

        while (matcher.find()) {
            String sublistStr = matcher.group(1);
            List<String> sublist = new ArrayList<>();

            // 将提取的字符串按逗号分隔为单个元素，并去除首尾的单引号和空格
            String[] elements = sublistStr.split(", ");
            for (String element : elements) {
                String cleanedElement = element.replaceAll("^\'|\'$", "").trim();
                sublist.add(cleanedElement);
            }

            extractedList.add(sublist);
        }

        return extractedList;
    }
}
