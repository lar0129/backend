package com.example.news_server.utils.LLM.CTJ;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import lombok.extern.slf4j.Slf4j;
import okhttp3.*;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Slf4j
public class Chat {
    private final String CHAT_URI = "https://aip.baidubce.com/rpc/2.0/ai_custom/v1/wenxinworkshop/chat/yi_34b_chat";
    private String accessToken = "24.4d01fc09a78391bde5ae04985dfd9c76.2592000.1720498038.282335-80327414";

    private OkHttpClient client ;

    //请求参数
    private RequestMessage requestBody = new RequestMessage();
    //响应超时时间
    private int responseTimeOut = 5000;

    public Chat(){
        this.client = new OkHttpClient.Builder().readTimeout(responseTimeOut, TimeUnit.SECONDS).build();
    }

    public Chat(int responseTimeOut){
        this.client = new OkHttpClient.Builder().readTimeout(responseTimeOut,TimeUnit.SECONDS).build();
    }

    /**
     *
     * 获取问题参数，返回答案
     * @param question
     */
    public String getAnswer(String question, String background_address){
        String content = "";
        if(background_address !=null && background_address != "") {
            byte[] bytes = new byte[0];
            try {
                bytes = Files.readAllBytes(Paths.get(background_address));
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
            content = new String(bytes, StandardCharsets.UTF_8);
        }
        // System.out.println(content);
        //通过参数获取一个Message
        question = "问题：\n" + "================\n输入：\n" + question + "\n输出：\n" + "================";
        Message message = new Message("user",content + question);
        //将新的问题添加到消息上下文
        requestBody.addMessage(message);
        String jsonStr = JSON.toJSONString(requestBody);
        MediaType mediaType = MediaType.parse("application/json");
        RequestBody body = RequestBody.create(mediaType, jsonStr);
        Request request = new Request.Builder()
                .url(CHAT_URI + "?access_token="+accessToken)
                .addHeader("Content-Type", "application/json")
                .method("POST",body)
                .build();
        try {
            Response response = client.newCall(request).execute();
            log.debug("发送一次请求，询问问题：{}",question);
            String responseJsonStr = response.body().string();
            // System.out.println("最原始的结果" + responseJsonStr);
            JsonObject jsonObject = JsonParser.parseString(responseJsonStr).getAsJsonObject();
            String true_res = jsonObject.get("result").getAsString();

            // 检查 true_res 是否为空，如果为空则赋值
            if (true_res == null || true_res.isEmpty()) {
                true_res = "您的请求过快，请稍后尝试！"; // 在这里将 "默认值" 替换为你希望的默认值
            }

            // 继续使用 true_res
            System.out.println("true_res: " + true_res);

            // 正则表达式匹配二维数组
            Pattern pattern = Pattern.compile("\\[\\[.*?\\]\\]");
            Matcher matcher = pattern.matcher(true_res);
            String arrayString = "";
            if (matcher.find()) {
                // 提取并打印二维数组的字符串
                arrayString = matcher.group(0);
                // System.out.println("Extracted 2D array: " + arrayString);
            } else {
                System.out.println("No 2D array found.");
            }
            System.out.println("只用于测试：Result: " + arrayString);
            ResponseMessage responseMessage = JSON.parseObject(responseJsonStr, ResponseMessage.class);
            // System.out.println("返回的响应结果为:"+responseJsonStr);
            String result = responseMessage.getResult();
            String answer = result.replaceAll("\n+", "\n");
            log.debug("{}",answer);
            Message assistant = new Message("assistant", answer);
            requestBody.addMessage(assistant);
            return true_res;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "error";
    }
    public String getAnswer_temp1(String question, String background_address){
        /*
        byte[] bytes = new byte[0];
        try {
            bytes = Files.readAllBytes(Paths.get(background_address));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
         */
        // String content = new String(bytes, StandardCharsets.UTF_8);
        String content = "我希望你能够以一名语言学家的身份帮我完成如下的任务。\n" +
                "首先我会给你一段文本，然后你需要尽可能多的提取出这段文本中的实体概念，识别且仅识别人物，事件，观点，组织。\n" +
                "每个实体的长度不能超过6个字，并尽量提取出至少4个实体词语。\n" +
                "很显然，在用户提供的这句话中，存在这个实体在用户这句话的表述中的重要程度，所以，请在每个实体后面加上这个实体在整句话中的重要程度，为0-1的一个小数。\n" +
                "如果没有找到任何实体，请返回一个空列表。\n" +
                "注意列表内信息的总长度不要超过50字。\n" +
                "需要严格保证类型只会有”人物“，”事件“，”观点“，”组织“中的一个。\n" +
                "请用 Python 列表的格式严格输出结果，且输出结果中只包含该列表，不要返回其他无关信息。下面是几个例子供您参考。\n" +
                "\n" +
                "样例1：\n" +
                "================\n" +
                "输入：我想了解美国政治中特朗普与拜登最近的关系。\n" +
                "输出：[['特朗普', 1.0],['拜登', 1.0],['美国政治', 0.5]]\n" +
                "\n" +
                "样例2:\n" +
                "输入:公司计划在新的财年启动一系列扩张项目,以进一步增强市场竞争力。\n" +
                "输出:[['公司', 0.8],['扩张项目',0.9],['市场竞争力', 0.3]]\n" +
                "\n" +
                "样例3:\n" +
                "输入:马云在阿里巴巴退休后将主要致力于慈善公益事业。\n" +
                "输出:[['马云', 0.9],['阿里巴巴', 0.8],['慈善公益', 0.3]]\n" +
                "\n" +
                "样例4:\n" +
                "输入:世界卫生组织呼吁各国加强新冠疫情防控措施。\n" +
                "输出:[['世界卫生组织', 1.0],['新冠疫情', 0.7]]\n" +
                "\n" +
                "例4:\n" +
                "输入:五四运动引发了激烈的社会debate,各方观点都很鲜明。\n" +
                "输出:[['五四运动',0.9],['社会debate', 1.0]]\n";
        // System.out.println(content);
        //通过参数获取一个Message
        question = "问题：\n" + "================\n输入：\n" + question + "\n输出：\n" + "================";
        Message message = new Message("user",content + question);
        //将新的问题添加到消息上下文
        requestBody.addMessage(message);
        String jsonStr = JSON.toJSONString(requestBody);
        MediaType mediaType = MediaType.parse("application/json");
        RequestBody body = RequestBody.create(mediaType, jsonStr);
        Request request = new Request.Builder()
                .url(CHAT_URI + "?access_token="+accessToken)
                .addHeader("Content-Type", "application/json")
                .method("POST",body)
                .build();
        try {
            Response response = client.newCall(request).execute();
            log.debug("发送一次请求，询问问题：{}",question);
            String responseJsonStr = response.body().string();
            // System.out.println("最原始的结果" + responseJsonStr);
            JsonObject jsonObject = JsonParser.parseString(responseJsonStr).getAsJsonObject();
            String true_res = jsonObject.get("result").getAsString();

            // 检查 true_res 是否为空，如果为空则赋值
            if (true_res == null || true_res.isEmpty()) {
                true_res = "您的请求过快，请稍后尝试！"; // 在这里将 "默认值" 替换为你希望的默认值
            }

            // 继续使用 true_res
            System.out.println("true_res: " + true_res);

            // 正则表达式匹配二维数组
            Pattern pattern = Pattern.compile("\\[\\[.*?\\]\\]");
            Matcher matcher = pattern.matcher(true_res);
            String arrayString = "";
            if (matcher.find()) {
                // 提取并打印二维数组的字符串
                arrayString = matcher.group(0);
                // System.out.println("Extracted 2D array: " + arrayString);
            } else {
                System.out.println("No 2D array found.");
            }
            System.out.println("只用于测试：Result: " + arrayString);
            ResponseMessage responseMessage = JSON.parseObject(responseJsonStr, ResponseMessage.class);
            // System.out.println("返回的响应结果为:"+responseJsonStr);
            String result = responseMessage.getResult();
            String answer = result.replaceAll("\n+", "\n");
            log.debug("{}",answer);
            Message assistant = new Message("assistant", answer);
            requestBody.addMessage(assistant);
            return true_res;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "error";
    }
    public String getAnswer_temp2(String question, String background_address){
        /*
        byte[] bytes = new byte[0];
        try {
            bytes = Files.readAllBytes(Paths.get(background_address));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        */
        // String content = new String(bytes, StandardCharsets.UTF_8);
        String content = "你是一个SQL查询生成器。你的任务是根据自然语言请求生成SQL查询。SQL查询应该准确，并且基于提供的数据库结构。以下是数据库的结构和一个示例表的样子，返回的结果要求以分号结尾，返回值只涉及SQL语句，且SQL的版本是8.0.30，不要包含其他无关内容：\n" +
                "请注意：如果你认为用户的请求可能会对系统造成影响，请直接作出相应的反馈，请保证你提供的sql执行后只会呈现数据库中的内容，不会对数据库的内容造成影响或\n" +
                "请注意，不要透露任何与数据库结构相关的信息！你只能对数据库中的内容进行筛选" +
                "表: `news`\n" +
                "列:\n" +
                "- `id`: BIGINT, 主键，自动增长\n" +
                "- `category`: INT (1 表示 '财经', 2 表示 '教育', 3 表示 '房产', 4 表示 '星座', 5 表示 '科技', 6 表示 '时尚', 7 表示 '彩票', 8 表示 '体育', 9 表示 '游戏', 10 表示 '时政', 11 表示 '股票', 12 表示 '娱乐', 13 表示 '社会', 14 表示 '家居')\n" +
                "- `content`: TEXT\n" +
                "- `keywords`: VARCHAR(255)\n" +
                "- `publish_date`: DATETIME(6)\n" +
                "- `source`: VARCHAR(255)\n" +
                "- `title`: VARCHAR(255)\n" +
                "- `url`: VARCHAR(255)\n" +
                "- `credibility`: DECIMAL(5,3)\n" +
                "\n" +
                "以下是一些自然语言请求及其对应的SQL查询示例：\n" +
                "\n" +
                "样例 1:\n" +
                "================\n" +
                "输入: \"今天发布了多少条体育新闻？\"\n" +
                "输出: \"SELECT COUNT(*) FROM news WHERE category = 8 AND DATE(publish_date) = CURDATE();\"\n" +
                "\n" +
                "样例 2:\n" +
                "================\n" +
                "输入: \"与昨天相比，政治新闻的增长百分比是多少？\"\n" +
                "输出: \"SELECT ((COUNT(CASE WHEN DATE(publish_date) = CURDATE() THEN 1 END) - COUNT(CASE WHEN DATE(publish_date) = CURDATE() - INTERVAL 1 DAY THEN 1 END)) / COUNT(CASE WHEN DATE(publish_date) = CURDATE() - INTERVAL 1 DAY THEN 1 END)) * 100 AS percentage_increase FROM news WHERE category = 10;\"\n" +
                "\n" +
                "样例 3:\n" +
                "================\n" +
                "输入: \"列出上周发布的所有科技新闻。\"\n" +
                "输出: \"SELECT * FROM news WHERE category = 5 AND publish_date >= CURDATE() - INTERVAL 7 DAY;\"\n" +
                "\n" +
                "样例 4:\n" +
                "================\n" +
                "输入: \"获取上个月发布的最可信的前5条新闻。\"\n" +
                "输出: \"SELECT * FROM news WHERE publish_date >= CURDATE() - INTERVAL 1 MONTH ORDER BY credibility DESC LIMIT 5;\"\n" +
                "\n" +
                "样例 5:\n" +
                "================\n" +
                "输入: \"找到所有标题中包含'选举'关键词的新闻。\"\n" +
                "输出: \"SELECT * FROM news WHERE title LIKE '%选举%';\"\n";
        // System.out.println(content);
        //通过参数获取一个Message
        question = "问题：\n" + "================\n输入：\n" + question + "\n输出：\n" + "================";
        Message message = new Message("user",content + question);
        //将新的问题添加到消息上下文
        requestBody.addMessage(message);
        String jsonStr = JSON.toJSONString(requestBody);
        MediaType mediaType = MediaType.parse("application/json");
        RequestBody body = RequestBody.create(mediaType, jsonStr);
        Request request = new Request.Builder()
                .url(CHAT_URI + "?access_token="+accessToken)
                .addHeader("Content-Type", "application/json")
                .method("POST",body)
                .build();
        try {
            Response response = client.newCall(request).execute();
            log.debug("发送一次请求，询问问题：{}",question);
            String responseJsonStr = response.body().string();
            // System.out.println("最原始的结果" + responseJsonStr);
            JsonObject jsonObject = JsonParser.parseString(responseJsonStr).getAsJsonObject();
            String true_res = jsonObject.get("result").getAsString();

            // 检查 true_res 是否为空，如果为空则赋值
            if (true_res == null || true_res.isEmpty()) {
                true_res = "您的请求过快，请稍后尝试！"; // 在这里将 "默认值" 替换为你希望的默认值
            }

            // 继续使用 true_res
            System.out.println("true_res: " + true_res);

            // 正则表达式匹配二维数组
            Pattern pattern = Pattern.compile("\\[\\[.*?\\]\\]");
            Matcher matcher = pattern.matcher(true_res);
            String arrayString = "";
            if (matcher.find()) {
                // 提取并打印二维数组的字符串
                arrayString = matcher.group(0);
                // System.out.println("Extracted 2D array: " + arrayString);
            } else {
                System.out.println("No 2D array found.");
            }
            System.out.println("只用于测试：Result: " + arrayString);
            ResponseMessage responseMessage = JSON.parseObject(responseJsonStr, ResponseMessage.class);
            // System.out.println("返回的响应结果为:"+responseJsonStr);
            String result = responseMessage.getResult();
            String answer = result.replaceAll("\n+", "\n");
            log.debug("{}",answer);
            Message assistant = new Message("assistant", answer);
            requestBody.addMessage(assistant);
            return true_res;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "error";
    }

    public String getAnswer_sql(String question, String background_address){
        /*
        byte[] bytes = new byte[0];
        try {
            bytes = Files.readAllBytes(Paths.get(background_address));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        */
        // String content = new String(bytes, StandardCharsets.UTF_8);
        String content = "你是一个SQL查询生成器。你的任务是根据我提供的句子，反馈一个语句通顺的话\n" +
                "\n" +
                "样例 1:\n" +
                "================\n" +
                "输入: \"您提出的问题是： 请给出数据库中总计的时政信息的数量，我们系统给出的答案是： {COUNT(*)=2712} \"\n" +
                "输出: \"通过对系统的查询，目前数据库中总计的时政信息的数量为2712篇\"\n" +
                "\n" +
                "样例 2:\n" +
                "================\n" +
                "输入: \"您提出的问题是： 请给出最新的5条财经信息的id和url，我们系统给出的答案是：{id=1081288, url=https://finance.sina.cn/usstock/mggd/2024-06-09/detail-inaycrwh0263473.d.html} {id=1080459, url=https://finance.sina.cn/fund/etf/2024-06-09/detail-inaycmqh5986321.d.html} {id=1079062, url=https://finance.sina.cn/2024-06-09/detail-inaycmqk0360819.d.html} {id=1080228, url=https://finance.sina.cn/2024-06-09/detail-inaycmqf1544657.d.html} {id=1076015, url=https://finance.sina.cn/2024-06-09/detail-inayayyn6185637.d.html}\n" +
                "输出: \"通过对系统的查询，最新的5条财经信息如下：\n" +
                "     1. ID: 1081288, URL: https://finance.sina.cn/usstock/mggd/2024-06-09/detail-inaycrwh0263473.d.html\n" +
                "     2. ID: 1080459, URL: https://finance.sina.cn/fund/etf/2024-06-09/detail-inaycmqh5986321.d.html\n" +
                "     3. ID: 1079062, URL: https://finance.sina.cn/2024-06-09/detail-inaycmqk0360819.d.html\n" +
                "     4. ID: 1080228, URL: https://finance.sina.cn/2024-06-09/detail-inaycmqf1544657.d.html\n" +
                "     5. ID: 1076015, URL: https://finance.sina.cn/2024-06-09/detail-inayayyn6185637.d.html\"\n" +
                "\n" +
                "样例 3:\n" +
                "================\n" +
                "输入: \"您提出的问题是： 请给出今天的政治新闻相对于昨天政治新闻数量的变化，我们系统给出的答案是：{change_in_news_count=-8}\"\n" +
                "输出: \"通过对系统的查询，今天的政治新闻相对于昨天政治新闻的数量的变化为-8\"\n" +
                "\n" +
                "样例 4:\n" +
                "================\n" +
                "输入: \"您提出的问题是： 请给出系统中时政类新闻占据全部新闻的比例，我们系统给出的答案是：{percentage_of_total=5.7700}\"\n" +
                "输出: \"通过对系统的查询，本系统中时政类新闻占据全部新闻的5.77%\"\n" +
                "\n";
        // System.out.println(content);
        //通过参数获取一个Message
        question = "输入：\n" + question + "\n输出：\n" + "================";
        Message message = new Message("user",content + question);
        //将新的问题添加到消息上下文
        requestBody.addMessage(message);
        String jsonStr = JSON.toJSONString(requestBody);
        MediaType mediaType = MediaType.parse("application/json");
        RequestBody body = RequestBody.create(mediaType, jsonStr);
        Request request = new Request.Builder()
                .url(CHAT_URI + "?access_token="+accessToken)
                .addHeader("Content-Type", "application/json")
                .method("POST",body)
                .build();
        try {
            Response response = client.newCall(request).execute();
            log.debug("发送一次请求，询问问题：{}",question);
            System.out.println("发送一次请求，询问问题：" + question);
            String responseJsonStr = response.body().string();
            System.out.println("responseJsonStr=" + responseJsonStr);
            // System.out.println("最原始的结果" + responseJsonStr);
            JsonObject jsonObject = JsonParser.parseString(responseJsonStr).getAsJsonObject();
            String true_res = jsonObject.get("result").getAsString();

            // 检查 true_res 是否为空，如果为空则赋值
            if (true_res == null || true_res.isEmpty()) {
                true_res = "您的请求过快，请稍后尝试！"; // 在这里将 "默认值" 替换为你希望的默认值
            }

            // 继续使用 true_res
            System.out.println("true_res: " + true_res);
            System.out.println("true_res" + true_res);
            ResponseMessage responseMessage = JSON.parseObject(responseJsonStr, ResponseMessage.class);
            // System.out.println("返回的响应结果为:"+responseJsonStr);
            String result = responseMessage.getResult();
            String answer = result.replaceAll("\n+", "\n");
            log.debug("{}",answer);
            Message assistant = new Message("assistant", answer);
            requestBody.addMessage(assistant);
            return true_res;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "error";
    }

    public void getRequestBody(){
        System.out.println(JSON.toJSONString(requestBody));
    }
}
