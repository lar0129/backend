package com.example.news_server.utils.enums;

import lombok.Getter;

import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;

public enum KeywordType {

    INVALID(-1, "0xcccc"),
    NONE(0, ""), NORMAL(1, "普通"), PERSON(2, "人物"),
    PLACE(3, "地点"),  EVENT(4, "事件"),IDEA(5, "观点"),
    ORGANIZATION(6, "组织"), STATEMENT(7, "声明");

    @Getter
    private final int id;

    @Getter
    private final String type;

    private final static Map<String, KeywordType > stringMapping = new HashMap<>();

    private final static Map<Integer, KeywordType> intMapping = new HashMap<>();
    KeywordType(int id, String type){
        this.id = id;
        this.type = type;
    }

    public static KeywordType parseString(String type){
        for (KeywordType keywordTypeEnum: EnumSet.allOf(KeywordType.class)){
            if (keywordTypeEnum.type.equals(type)){
                return keywordTypeEnum;
            }
        }
        return INVALID;
    }

    public static KeywordType parseInteger(int keywordId){
        for (KeywordType keywordTypeEnum: EnumSet.allOf(KeywordType.class)){
            if (keywordTypeEnum.id == keywordId){
                return keywordTypeEnum;
            }
        }
        return INVALID;
    }
}
