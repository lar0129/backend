package com.example.news_server.utils.enums;

import lombok.Getter;

import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;

public enum Category {
    INVALID(-1, "0xcccc"),
    NONE(0, ""), ECONOMICS(1, "财经"), LOTTERY(2, "彩票"),
    PROPERTY(3, "房产"), STOCK(4, "股票"), HOME(5, "家居"),
    EDUCATION(6, "教育"), TECHNOLOGY(7, "科技"), SOCIETY(8, "社会"),
    FASHION(9, "时尚"), POLITICS(10, "时政"), SPORTS(11, "体育"),
    ZODIAC(12, "星座"), GAMES(13, "游戏"), AMUSEMENT(14, "娱乐");

    @Getter
    private final int id;

    @Getter
    private final String description;

    private final static Map<String, Category > stringMapping = new HashMap<>();

    private final static Map<Integer, Category> intMapping = new HashMap<>();
    Category(int id, String desc){
        this.id = id;
        this.description = desc;
    }

    public static Category parseString(String category){
        for (Category categoryEnum: EnumSet.allOf(Category.class)){
            if (categoryEnum.description.equals(category)){
                return categoryEnum;
            }
        }
        return INVALID;
    }

    public static Category parseInteger(int category){
        for (Category categoryEnum: EnumSet.allOf(Category.class)){
            if (categoryEnum.id == category){
                return categoryEnum;
            }
        }
        return INVALID;
    }
}
