package com.example.news_server.utils.classify;

import com.example.news_server.PO.News;
import com.example.news_server.utils.enums.Category;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.DefaultResourceLoader;
import org.springframework.core.io.Resource;
import org.thunlp.text.classifiers.BasicTextClassifier;
import org.thunlp.text.classifiers.ClassifyResult;
import org.thunlp.text.classifiers.LinearBigramChineseTextClassifier;

import java.io.*;

public class ClassifyModelUtils {


    private static BasicTextClassifier classifier;

    public ClassifyModelUtils() {
        // 设置分类种类，并读取模型
        loadModel();
    }

    // 单例模式
    public static void loadModel(){
        if (classifier == null) {
            classifier = new BasicTextClassifier();
            try {
            String separator = System.getProperty("file.separator");
            String tempPath = System.getProperty("java.io.tmpdir") + separator + "tomcat_" + System.currentTimeMillis();
            Thread copyThread = new Thread(() -> {
                copyResourceToFile(tempPath, "model"+separator+"category");
                copyResourceToFile(tempPath, "model"+separator+"model");
                copyResourceToFile(tempPath, "model"+separator+"maxFeatures");
                copyResourceToFile(tempPath, "model"+separator+"lexicon");
            });
            copyThread.start(); // 启动线程
            copyThread.join(); // 等待线程结束

            String modelPath = tempPath + separator + "model";
            System.out.println("Loading model from " + modelPath);
            classifier.loadCategoryListFromFile(modelPath + File.separator + "category");
            classifier.setTextClassifier(new LinearBigramChineseTextClassifier(classifier.getCategorySize()));
            classifier.getTextClassifier().loadModel(modelPath);
            } catch (Exception e) {
                System.err.println("Error when loading model: " + e.getMessage());
            }
        }
    }

    public static String LoadModelAndUse(String text) {
        // 新建分类器对象
        loadModel();

        int topN = 3;  // 保留最有可能的3个结果
        ClassifyResult[] result = classifier.classifyText(text, topN);
        System.out.println("classify text "+ text.substring(0,10)+ "…… success! " + "Text length: " + text.length()+
                ". Result: " + classifier.getCategoryName(result[0].label) + " with prob: " + result[0].prob )  ;
        return classifier.getCategoryName(result[0].label);
    }

    public static Category runModelAndGetCategory(String text) {
        String result = LoadModelAndUse(text);
        return Category.parseString(result);
    }

    public static void newsClassify (News newsSaved) {
        Category category;
        if (newsSaved.getContent() != null) {
            int classifyToken = Math.min(1000, newsSaved.getContent().length());
            category = runModelAndGetCategory(newsSaved.getTitle() + newsSaved.getContent().substring(0, classifyToken));
        }
        else {
            category = runModelAndGetCategory(newsSaved.getTitle());
        }
        newsSaved.setCategory(category.getId());
    }

    synchronized public static void copyResourceToFile(String tempPath, String fileName) {
        try {
            String tempFilepath = tempPath + File.separator + fileName;
            InputStream inputStream = new DefaultResourceLoader().getResource("classpath:" + System.getProperty("file.separator") + fileName).getInputStream();
            File tempFile = new File(tempFilepath);
            File parentDir = tempFile.getParentFile();
            if (!parentDir.exists()) {
                if (parentDir.mkdirs()) {
                    System.out.println("目录已创建：" + parentDir.getAbsolutePath());
                } else {
                    System.out.println("无法创建目录：" + parentDir.getAbsolutePath());
                }
            }
            tempFile.createNewFile();
            System.out.println("Create new file: " + tempFile);
            FileOutputStream outputStream = new FileOutputStream(tempFile);

            byte[] buffer = new byte[4096];
            int bytesRead;
            while ((bytesRead = inputStream.read(buffer)) != -1) {
                outputStream.write(buffer, 0, bytesRead);
            }
            inputStream.close();
            outputStream.close();
        } catch (Exception e) {
            System.err.println("Error when loading model: " + e.getMessage());
        }
    }
}
