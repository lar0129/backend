package com.example.news_server.utils;

import lombok.Getter;
import lombok.Setter;
import org.springframework.http.HttpStatus;

import java.util.AbstractMap;
import java.util.HashMap;
import java.util.Map;

public class ApiResponse<T> {

    @Getter
    @Setter
    private int code;

    @Getter
    @Setter
    private String msg;

    @Getter
    @Setter
    private T data;

    private static final Map<Integer, String> DEFAULT_MESSAGES = new HashMap<>();

    //在这里添加返回码和对应的信息
    static {
        DEFAULT_MESSAGES.put(HttpStatus.OK.value(), "Success");
        DEFAULT_MESSAGES.put(HttpStatus.CREATED.value(), "Successfully created");
        DEFAULT_MESSAGES.put(HttpStatus.BAD_REQUEST.value(), "Bad request");
        DEFAULT_MESSAGES.put(HttpStatus.NOT_FOUND.value(), "Not found");
        DEFAULT_MESSAGES.put(HttpStatus.INTERNAL_SERVER_ERROR.value(), "Internal server error");
    }

    public ApiResponse(int status, String message, T data) {
        this.code = status;
        if (message.equals("")){
            message = DEFAULT_MESSAGES.getOrDefault(status, "Finished!");
        }
        this.msg = message;
        this.data = data;
    }

    // 方便快速构建成功和错误响应的静态方法
    public static <T> ApiResponse<T> OK(T data) {
        return new ApiResponse<>(HttpStatus.OK.value(), "", data);
    }

    public static <T> ApiResponse<T> OK(T data, String msg) {
        return new ApiResponse<>(HttpStatus.OK.value(), msg, data);
    }

    public static <T> ApiResponse<T> CREATED(T data) {
        return new ApiResponse<>(HttpStatus.CREATED.value(), "", data);
    }

    public static <T> ApiResponse<T> CREATED(T data, String msg ) {
        return new ApiResponse<>(HttpStatus.CREATED.value(), msg, data);
    }

    public static <T> ApiResponse<T> BAD_REQUEST() {
        return new ApiResponse<>(HttpStatus.BAD_REQUEST.value(), "", null);
    }

    public static <T> ApiResponse<T> BAD_REQUEST(String msg ) {
        return new ApiResponse<>(HttpStatus.BAD_REQUEST.value(), msg, null);
    }

    public static <T> ApiResponse<T> BAD_REQUEST(String msg , T data) {
        return new ApiResponse<>(HttpStatus.BAD_REQUEST.value(), msg, data);
    }

    public static <T> ApiResponse<T> INTERNAL_ERROR() {
        return new ApiResponse<>(HttpStatus.INTERNAL_SERVER_ERROR.value(), "", null);
    }

    public static <T> ApiResponse<T> INTERNAL_ERROR(String msg) {
        return new ApiResponse<>(HttpStatus.INTERNAL_SERVER_ERROR.value(), msg, null);
    }

    public static <T> ApiResponse<T> NOT_FOUND() {
        return new ApiResponse<>(HttpStatus.NOT_FOUND.value(), "", null);
    }

    public static <T> ApiResponse<T> NOT_FOUND(String msg) {
        return new ApiResponse<>(HttpStatus.NOT_FOUND.value(), msg, null);
    }

}
