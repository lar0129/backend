package com.example.news_server.utils.nlp;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import com.example.news_server.config.ApikeyConfig;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import org.springframework.beans.factory.annotation.Value;

/**
 * 使用高德解析地名
 */
public class GaodeHttp {
    // 高德api
    public static String MAP_AK = ApikeyConfig.GAODE_ak;
    public static String MAP_URL = "http://restapi.amap.com/v3/geocode/geo?output=JSON&key=" + MAP_AK;


    /**
     * 将地址解析成经纬度
     *
     */
    public static JsonObject getPosition(String address) {
        HttpURLConnection connection = null;
        InputStream is = null;
        BufferedReader br = null;
        String result = null;// 返回结果字符串
        Gson gson = new Gson();
        JsonObject jsonObject = new JsonObject();
        jsonObject.add("lat",null);
        jsonObject.add("lng",null);
        try {
            // 创建远程url连接对象
            URL url = new URL(MAP_URL + "&address=" + address);
            System.out.println(url);
            // 通过远程url连接对象打开一个连接，强转成httpURLConnection类
            connection = (HttpURLConnection) url.openConnection();
            // 设置连接方式：get
            connection.setRequestMethod("GET");
            // 设置连接主机服务器的超时时间：15000毫秒
            connection.setConnectTimeout(15000);
            // 设置读取远程返回的数据时间：60000毫秒
            connection.setReadTimeout(60000);
            // 发送请求
            connection.connect();
            // 通过connection连接，获取输入流
            if (connection.getResponseCode() == 200) {
                is = connection.getInputStream();
                // 封装输入流is，并指定字符集
                br = new BufferedReader(new InputStreamReader(is, "UTF-8"));
                // 存放数据
                StringBuffer sbf = new StringBuffer();
                String temp = null;
                while ((temp = br.readLine()) != null) {
                    sbf.append(temp);
                    sbf.append("\r\n");
                }
                result = sbf.toString();
            }
            connection.disconnect();
            return gson.fromJson(result, JsonObject.class);
        } catch (Exception e) {
            return jsonObject;
        }
    }
}
