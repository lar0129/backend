package com.example.news_server.utils.nlp;

import com.example.news_server.PO.News;
import com.example.news_server.utils.enums.Category;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.hankcs.hanlp.HanLP;
import com.hankcs.hanlp.seg.Segment;
import com.hankcs.hanlp.seg.common.Term;
import kotlin.Pair;
import org.springframework.core.io.DefaultResourceLoader;

import java.io.File;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class DataAnalysisUtils {
    private final Segment segment;

    private static DataAnalysisUtils instance;

    public static HashMap<String, Pair<Double,Double>> placeMap = new HashMap<>();

    // 单例模式
    DataAnalysisUtils() {
        segment = HanLP.newSegment();
        segment.enableNameRecognize(true);
        segment.enableTranslatedNameRecognize(true);
        segment.enableJapaneseNameRecognize(true);
        segment.enableOrganizationRecognize(true);
        segment.enableAllNamedEntityRecognize(true);
        segment.enablePlaceRecognize(true);

        //读取Json格式的国家经纬度数据
        ReadPlaceMapJson("static" + System.getProperty("file.separator") + "countryData.json");
    }

    public static DataAnalysisUtils getInstance() {
        if(instance == null)
            instance = new DataAnalysisUtils();

        return instance;
    }

    // 提取人名
    public List<String> extractName(String sentence) {
        List<Term> terms = segment.seg(sentence);
        List<String> nameList = new ArrayList<>();
        for(int i = 0; i < terms.size(); i++) {
            if(terms.get(i).nature.toString().equals("nr") || terms.get(i).nature.toString().equals("nrf") || terms.get(i).nature.toString().equals("nrj") ) {
                if(!(nameList.contains(terms.get(i).word)) || terms.get(i).word.length()<2) {
                    nameList.add(terms.get(i).word);
                }
            }
        }
        return nameList;
    }

    // 提取地名
    public List<String> extractPlace(String sentence) {
        List<Term> terms = segment.seg(sentence);
        List<String> placeList = new ArrayList<>();
        for(int i = 0; i < terms.size(); i++) {
            if(terms.get(i).nature.toString().equals("ns") || terms.get(i).nature.toString().equals("nsf")) {
                if(!(placeList.contains(terms.get(i).word))|| terms.get(i).word.length()<2) {
                    placeList.add(terms.get(i).word);
                }
            }
        }
        return placeList;
    }

    // 提取机构名
    public List<String> extractOrganization(String sentence) {
        List<Term> terms = segment.seg(sentence);
        List<String> organizationList = new ArrayList<>();
        for(int i = 0; i < terms.size(); i++) {
            if(terms.get(i).nature.toString().equals("nt") || terms.get(i).nature.toString().equals("ntc") || terms.get(i).nature.toString().equals("ntcb") || terms.get(i).nature.toString().equals("ntcf") || terms.get(i).nature.toString().equals("ntch") || terms.get(i).nature.toString().equals("nth") || terms.get(i).nature.toString().equals("nto") || terms.get(i).nature.toString().equals("nts") || terms.get(i).nature.toString().equals("ntu")) {
                if(!(organizationList.contains(terms.get(i).word) || terms.get(i).word.length()<2)) {
                    organizationList.add(terms.get(i).word);
                }
            }
        }
        return organizationList;
    }

    // 提取关键词
    public List<String> extractKeyword(String sentence, int size) {
        List<String> keywordList = HanLP.extractKeyword(sentence, size);
        for (int i = 0; i < keywordList.size(); i++) {
            if (keywordList.get(i).length() < 2) {
                keywordList.remove(i);
                i--;
            }
        }
        return keywordList;
    }

    // 提取短语
    public List<String> extractPhrase(String sentence, int size) {
        List<String> phraseList = HanLP.extractPhrase(sentence, size);
        return phraseList;
    }

    public Double getLongitude(String place) {
        if(placeMap.containsKey(place)) {
            return placeMap.get(place).getSecond();
        }
//        JsonObject jsonObject = GaodeHttp.getPosition(place);
//        if (jsonObject.get("status").getAsString().equals("1")) {
//            JsonArray jsonArray = jsonObject.getAsJsonArray("geocodes");
//            if (jsonArray.size() > 0) {
//                JsonObject location = jsonArray.get(0).getAsJsonObject();
//                String[] locations = location.get("location").getAsString().split(",");
//                return Double.parseDouble(locations[0]);
//            }
//        }

        return 0.0;
    }

    public Double getLatitude(String place) {
        if(placeMap.containsKey(place)) {
            return placeMap.get(place).getFirst();
        }
//        JsonObject jsonObject = GaodeHttp.getPosition(place);
//        if (jsonObject.get("status").getAsString().equals("1")) {
//            JsonArray jsonArray = jsonObject.getAsJsonArray("geocodes");
//            if (jsonArray.size() > 0) {
//                JsonObject location = jsonArray.get(0).getAsJsonObject();
//                String[] locations = location.get("location").getAsString().split(",");
//                return Double.parseDouble(locations[1]);
//            }
//        }

        return 0.0;
    }
    private void ReadPlaceMapJson(String path) {
        //读取Json文件
        try {
            Gson gson = new Gson();
            InputStream inputStream = new DefaultResourceLoader().getResource("classpath:" + System.getProperty("file.separator") + path).getInputStream();
            InputStreamReader reader = new InputStreamReader(inputStream);
            JsonArray jsonArray = gson.fromJson(reader, JsonArray.class);
            for (int i = 0; i < jsonArray.size(); i++) {
                JsonObject jsonObject = jsonArray.get(i).getAsJsonObject();
                String key = jsonObject.get("zh").getAsString();
                Double lat = jsonObject.get("lat").getAsDouble();
                Double lng = jsonObject.get("lng").getAsDouble();
                placeMap.put(key, new Pair<>(lat, lng));
//                System.out.println(key + " " + lat + " " + lng);
            }
        }catch (Exception e) {
            throw new RuntimeException("读取国家经纬度json文件失败"+e.getMessage());
        }
    }
    public static void main(String[] args) {
        DataAnalysisUtils instance = DataAnalysisUtils.getInstance();

        String content = "最新数据显示，2024/04/17 波罗的海干散货指数(BDI)报 1844 点，创2024/03/27以来新高水平，较前值涨3.65%，创4天最大涨幅，且为连续第6天上涨。其中，巴拿马型运费指数(BPI)报1805 点，较前值涨2.91%，海岬型运费指数(BCI)报2760 点，涨4.90%，超灵便型船运价指数(BSI)报1337 点，涨1.98%。波罗的海干散货指数+三大分项的最新720天走势图、十年走势图等详见汇通财经特制图表。 波罗的海交易所主要干散货海运指数周三上涨，受所有船型运费上涨推动，其中好望角型船运费创下三周多以来的最高水平。 综合海岬型船、巴拿马型船和超灵便型船运费指数上涨65点，至1,844点，涨幅3.7%。 海岬型船运价指数上涨129点，至2,760点，涨幅4.9%，为3月26日以来的最高水平。 海岬型船舶通常运输15万吨货物，例如铁矿石和煤炭，其平均日收益增加了1,074美元，达到22,893美元。 铁矿石期货下跌，大连期货七个交易日以来首次下跌，原因是投资者在最大消费国中国第一季度经济增长超出预期后重新考虑了该国的刺激前景。 巴拿马型船运价指数上涨51点，或2.9%，至1,805点，创两周新高。 巴拿马型船通常运载约60,000-70,000吨煤炭或谷物货物，其平均日收益上涨459美元，至16,244美元。 多式联运研究分析师CharaGeorgousi在周二的每周报告中表示：“巴拿马型船东的情绪喜忧参半。在大西洋，南美东海岸对谷物运输的需求旺盛，而北美洲的矿产出口则较少。” 在小型船舶中，超灵便型船运价指数上涨26点，至1,337点，涨幅约2%。 与此同时，据伊朗国家通讯社IRNA周三报道，伊朗当局表示，他们从一艘在阿曼湾沉没的库克群岛油轮上救出了21名斯里兰卡船员。";
        content = "北川景子福特汽车公司高管周树人表示，习近平美国汽车行业需要华盛顿政界人士在监管方面的确定性，虽然白宫易手将不会带来利好，但电动汽车的渗透率将继续增长。董事长比尔·福特周三表示。 他在底特律郊外举行的《底特律自由报》活动上表示，电动汽车销量的增长速度已经放缓，但它在全球范围内正在迅速被使用，福特也将紧随其后，尽管它对汽油动力和混合动力电动汽车进行了大量投入。 比尔·福特虽然没有评论美国总统民主党人乔·拜登和前总统共和党人唐纳德·特朗普之间的竞选，但他表示，他希望美国能够选择一条道路，以便该行业能够更好地规划。 “我们的规划时间比选举周期长得多，”他说。 比尔·福特补充道：“作为一家公司，坦率地说，作为一个行业，只要我们对前进的方向有一定的确定性，我们几乎可以做任何事情。” “问题是，当我们受到政客的来回折磨时。我们无法立即改变方向。只能选择一条道路，并走下去。” 密歇根州是今年秋季选举的关键战场州，拜登和特朗普都一直在向包括行业工人在内的选民谈论美国汽车行业面临的挑战。特朗普指责拜登的政策将扼杀汽车业就业机会，并帮助中国蓬勃发展的电动汽车行业。 比尔·福特表示，当他与华盛顿政界人士交谈时，他从两党那里得到了截然不同的观点。 他表示，共和党人质疑电动汽车的必要性，称美国行业落后于中国，而且他们不想使用中国技术。与此同时，民主党人正在推动该行业生产更多电动汽车，并询问他们可以采取哪些措施来加快这一进程。 拜登政府上个月放宽了拟议的规定，为底特律汽车制造商带来了重大胜利，这些规定将迫使底特律汽车制造商缩减高耗油汽车的生产，否则将面临数十亿美元的罚款。 比尔·福特表示，向电动汽车的过渡将是渐进的，并由消费者决定。 “我们不会把任何东西强加给任何人，”他说。 福特汽车公司周二恢复了 F-150 Lightning 电动皮卡的发货，此前未公开的质量问题导致该产品自2月开始停产九周。 本月早些时候，福特将部分 Lightning 车型的价格下调了 5，500 美元，此前在销量下降后，福特将 Mustang Mach-E 电动 SUV 的价格下调了 8，100 美元。 同样在本月，福特推迟了在加拿大推出三排电动汽车和计划在田纳西州生产的下一代电动皮卡车的计划。 福特首席执行官吉姆·法利表示，这家美国汽车制造商致力于扩大其电动汽车业务并实现盈利。 2023 年，该公司的电动汽车业务亏损了近 47 亿美元，预计今年将亏损 50 亿至 55 亿美元。";
        List<String> keywordList = HanLP.extractKeyword(content, 10);
        System.out.println(keywordList);

        System.out.println();

        List<String> phraseList = HanLP.extractPhrase(content, 10);
        System.out.println(phraseList);
        System.out.println();

        System.out.println(instance.extractName(content));
        System.out.println(instance.extractPlace(content));
        System.out.println(instance.extractOrganization(content));

        System.out.println(instance.getLongitude("兰州"));
        System.out.println(instance.getLongitude("深圳"));
        System.out.println(instance.getLongitude("中国"));
        System.out.println(instance.getLongitude("美国"));
        System.out.println(instance.getLongitude("纽约"));
    }
}
