package com.example.news_server.utils.nlp;

import com.hankcs.hanlp.HanLP;
import com.hankcs.hanlp.dictionary.CoreSynonymDictionary;
import com.hankcs.hanlp.seg.Segment;
import com.hankcs.hanlp.seg.common.Term;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static com.example.news_server.utils.nlp.StopWords.STOP_WORDS;

public class FuzzSearchUtils {
    public static int MIN_LENGTH = 2;

    private final Segment segment;

    private static FuzzSearchUtils instance;

    // 单例模式
    FuzzSearchUtils() {
        segment = HanLP.newSegment();
        segment.enableNameRecognize(true);
        segment.enableTranslatedNameRecognize(true);
        segment.enableJapaneseNameRecognize(true);
        segment.enableOrganizationRecognize(true);
        segment.enableAllNamedEntityRecognize(true);
        segment.enablePlaceRecognize(true);
    }

    public static FuzzSearchUtils getInstance() {
        if(instance == null)
            instance = new FuzzSearchUtils();

        return instance;
    }

    //过滤停用词
    public  List<String> filterStopWords(String sentence) {
        // 分词. 自动识别人名、地名
        List<Term> terms = segment.seg(sentence);


        List<String> filteredTerms = new ArrayList<>();
        for (Term term : terms) {
            if (!Arrays.asList(STOP_WORDS).contains(term.word)) {
                filteredTerms.add(term.word);
            }
        }
        return filteredTerms;
    }

    // 拼音转中文：todo.
    // 找不到拼音-中文的词库，
    public  List<String> pinyinToChinese(List<String> pinyin) {
        return pinyin;
    }

    // 同义词处理:
    public  List<String> synonymProcess(List<String> sentence) {
        List<String> synonymList = new ArrayList<>();
        for (String term : sentence) {
            synonymList.add(term);

            // 使用同义词表获取同义词
            synonymList.add(CoreSynonymDictionary.rewriteQuickly(term));

        }
        return synonymList;
    }

    // 获取相似语句
    public  String getSimilarSentence(String sentence){
        sentence = CoreSynonymDictionary.rewriteQuickly(sentence);
        return sentence;
    }

}


