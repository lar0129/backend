package com.example.news_server.utils.neo4jTesting;

/*
import org.neo4j.driver.v1.Record;
import org.neo4j.driver.v1.*;

public class Neo4jExample {
    public static void main(String[] args) {
        // 创建一个驱动程序
        Driver driver = GraphDatabase.driver("bolt://114.212.175.151:7687", AuthTokens.basic("neo4j", "hardly#2024"));

        // 创建一个会话
        try (Session session = driver.session()) {
            // 执行一个查询
            StatementResult result = session.run("MATCH (n) RETURN n LIMIT 5");

            // 处理查询结果
            while (result.hasNext()) {
                Record record = result.next();
                System.out.println(record.get("n").asNode().asMap());
            }
        }

        // 关闭驱动程序
        driver.close();
    }
}
*/

import org.neo4j.driver.*;
import org.neo4j.driver.Record;

public class Neo4jExample {
    public static void main(String[] args) {
        // 创建一个驱动程序
        Driver driver = GraphDatabase.driver("neo4j+s://7761641e.databases.neo4j.io", AuthTokens.basic("neo4j", "rwOIyIVz_8Bj2lyLh010iuCFigXztgqJLCbIqxX26us"));

        // 创建一个会话
        try (Session session = driver.session()) {
            // 创建节点
            session.run("CREATE (a:Node {id: 1})");
            session.run("CREATE (b:Node {id: 2})");

            // 建立关系
            session.run("MATCH (a:Node {id: 1}), (b:Node {id: 2}) " +
                    "CREATE (a)-[:RELATIONSHIP]->(b)");
        }

        // 关闭驱动程序
        driver.close();
    }
}
