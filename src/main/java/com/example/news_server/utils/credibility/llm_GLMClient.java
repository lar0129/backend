package com.example.news_server.utils.credibility;

import com.alibaba.fastjson.JSON;
import com.example.news_server.config.ApikeyConfig;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.zhipu.oapi.ClientV4;
import com.zhipu.oapi.Constants;
import com.zhipu.oapi.service.v4.model.*;
import org.springframework.beans.factory.annotation.Value;

import java.util.ArrayList;
import java.util.List;

public class llm_GLMClient {

    private static String API_SECRET_KEY = ApikeyConfig.GLM_ak;
    private static ClientV4 client = new ClientV4.Builder(API_SECRET_KEY).build();

    /**
     * 同步调用
     */
    public static String askClient(String prompt, String model){
        try {
            if (!(model.equals(Constants.ModelChatGLM4) || model.equals(Constants.ModelChatGLM3TURBO))) {
                return null;
            }
            List<ChatMessage> messages = new ArrayList<>();
            ChatMessage chatMessage = new ChatMessage(ChatMessageRole.USER.value(), prompt);
            messages.add(chatMessage);
            String requestIdTemplate = "requestId_%s";
            String requestId = String.format(requestIdTemplate, System.currentTimeMillis());

            ChatCompletionRequest chatCompletionRequest = ChatCompletionRequest.builder()
                    .model(model)
                    .stream(Boolean.FALSE)
                    .invokeMethod(Constants.invokeMethod)
                    .messages(messages)
                    .requestId(requestId)
                    .build();
            ModelApiResponse invokeModelApiResp = client.invokeModelApi(chatCompletionRequest);
            if (invokeModelApiResp.isSuccess()) {
                System.out.println("GLM model output:" + invokeModelApiResp.getData().toString().substring(0, 100) + "......\n\n");
                return invokeModelApiResp.getData().getChoices().get(0).getMessage().getContent().toString();
            }
            else{
                System.out.println("GLM model Error" + invokeModelApiResp.getMsg());
                return null;
            }
        }catch (ZhiPuAiHttpException e){
            System.out.println("GLM model Error" + e.getMessage());
            return null;
        }
    }

    /**
     * 异步调用
     */
    private static String askClientAsync(String prompt,String model){
        if(!(model.equals(Constants.ModelChatGLM4) || model.equals(Constants.ModelChatGLM3TURBO))){
            return null;
        }
        List<ChatMessage> messages = new ArrayList<>();
        ChatMessage chatMessage = new ChatMessage(ChatMessageRole.USER.value(), prompt);
        messages.add(chatMessage);
        String requestIdTemplate = "requestId_%s";
        String requestId = String.format(requestIdTemplate, System.currentTimeMillis());

        ChatCompletionRequest chatCompletionRequest = ChatCompletionRequest.builder()
                .model(model)
                .stream(Boolean.FALSE)
                .invokeMethod(Constants.invokeMethodAsync)
                .messages(messages)
                .requestId(requestId)
                .build();
        ModelApiResponse invokeModelApiResp = client.invokeModelApi(chatCompletionRequest);
        System.out.println("GLM model output:" + JSON.toJSONString(invokeModelApiResp));
        return invokeModelApiResp.getData().getTaskId();
    }

    public static void setApiSecretKey(String apiSecretKey) {
        API_SECRET_KEY = apiSecretKey;
    }
}
