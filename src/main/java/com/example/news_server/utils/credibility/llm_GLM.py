import time
from zhipuai import ZhipuAI
import sys

client = ZhipuAI(api_key="6d11ad2881129c843baa56c97f9e438a.0bsePvTK2V1xzM9S") # 请填写您自己的APIKey

prompt = sys.argv[1]
if prompt == "":
    print("-1.000")
    sys.exit(0)

response = client.chat.asyncCompletions.create(
    model="glm-4",  # 填写需要调用的模型名称
    messages=[
        {
            "role": "user",
            "content": prompt
        }
    ],
)
task_id = response.id
task_status = ''
get_cnt = 0

while task_status != 'SUCCESS' and task_status != 'FAILED' and get_cnt <= 40:
    result_response = client.chat.asyncCompletions.retrieve_completion_result(id=task_id)
    task_status = result_response.task_status
    if(result_response.task_status == 'SUCCESS'):
        print(result_response.choices[0].message.content)
        break
    time.sleep(2)
    get_cnt += 1
