package com.example.news_server.utils.credibility;

import com.example.news_server.PO.News;
import com.example.news_server.config.ApikeyConfig;
import com.zhipu.oapi.Constants;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CredibilityLLMAnalysis {

    // 输入：新闻内容
    // 输出：新闻可信度

    public static Double getNewsCredibilityByNews(News news){
        String newsPrompt = "新闻标题："+news.getTitle() + "新闻来源："+news.getSource()
                + "新闻发布时间："+news.getPublishDate() + "新闻内容："+news.getContent();
        return CredibilityLLMAnalysis.getNewsCredibility(newsPrompt);
    }

    public static Double getNewsCredibility(String news){
        String head = "请根据事实、新闻来源、新闻内容，分析新闻可信度。只输出新闻可信度浮点数数字。别输出其他任何字符。对于明显的谣言和非新闻，输出0.001。对于其他新闻，作出0-1之间的新闻可信度合理判断。\n" +
                "\n" +
                "         输入：新闻字符串string [news]\n" +
                "         输出：浮点数double。描述新闻可信度，范围在0-1之间，小数点后保留三位有效数字\n";
        String tail = "\n除了数字的输出，不要用任何东西来回应";

        String output = llm_GLMClient.askClient(head + news + tail, Constants.ModelChatGLM3TURBO);
        if(output != null){
            Double credibility = extractDoubleFromString(output);
            System.out.println("llm_GLM Output credibility: " + credibility);
            return extractDoubleFromString(output.toString());
        }
        else {
            System.out.println("llm_GLM request Error");
            return -1.000;
        }
    }

    // 提取String中第一个出现的浮点数
    public static Double extractDoubleFromString(String str){
        // 使用正则表达式
        String pattern = "^[0-1]\\.\\d+$";

        // 创建 Pattern 对象
        Pattern regex = Pattern.compile(pattern);

        // 创建 Matcher 对象
        Matcher matcher = regex.matcher(str);

        // 查找匹配的浮点数
        if (matcher.find()) {
            String match = matcher.group();
            return Double.parseDouble(match);
        }
        return -1.000;
    }

    public static void main(String[] args) {
        System.out.println(CredibilityLLMAnalysis.getNewsCredibility("芬兰“无限期延长”俄芬边境口岸关闭状态 [新闻发布时间]2024-04-15 15:22:26.0 [新闻来源]新京报"));
    }
}
