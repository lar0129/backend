package com.example.news_server.utils.sse;

import java.io.IOException;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.springframework.http.MediaType;
import org.springframework.web.servlet.mvc.method.annotation.SseEmitter;

public class SseSession {


    private static Map<String,SseEmitter> sessionMap = new ConcurrentHashMap<>();

    public static void add(String sessionKey,SseEmitter sseEmitter){
        if(sessionMap.get(sessionKey) != null){
           throw new IllegalStateException("User exists!");
        }
        sessionMap.put(sessionKey, sseEmitter);
    }

    public static SseEmitter get(String sessionKey){
        return sessionMap.get(sessionKey);
    }
    
    public static boolean exists(String sessionKey){
        return sessionMap.get(sessionKey) != null;
    }

    public static boolean remove(String sessionKey){
        SseEmitter sseEmitter = sessionMap.get(sessionKey);
        if(sseEmitter != null){
            sseEmitter.complete();
        }
        return false;
    }

    public static void onError(String sessionKey,Throwable throwable){
        SseEmitter sseEmitter = sessionMap.get(sessionKey);
        if(sseEmitter != null){
            sseEmitter.completeWithError(throwable);
        }
    }

    public static void send(String sessionKey,Object content, String eventName) throws IOException{
        // 将content-type设置text/event-stream

        sessionMap.get(sessionKey).send(SseEmitter.event().name(eventName).data(content, MediaType.APPLICATION_JSON));
    }

}