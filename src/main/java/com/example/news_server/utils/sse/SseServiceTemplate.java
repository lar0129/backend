package com.example.news_server.utils.sse;

import org.springframework.web.servlet.mvc.method.annotation.SseEmitter;

public interface SseServiceTemplate {
    
    public SseEmitter conect(String userId);
    
    public boolean send(String userId, Object content, String eventName);

    public boolean close(String userId);
}
