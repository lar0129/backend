package com.example.news_server.controller;

import com.example.news_server.VO.data.KeywordAnalysisVO;
import com.example.news_server.VO.data.SummaryNewsAmountVO;
import com.example.news_server.VO.data.SummaryPlacesVO;
import com.example.news_server.service.DataService;
import com.example.news_server.service.NewsService;
import com.example.news_server.service.SseService;
import com.example.news_server.utils.ApiResponse;
import jakarta.servlet.http.HttpServletResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.method.annotation.SseEmitter;

import java.time.LocalDate;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import org.springframework.http.MediaType;

import static java.lang.Thread.sleep;

@RestController
@Slf4j
@RequestMapping("/api/data")
public class DataController {

    private final DataService dataService;
    private final NewsService newsService;

    private SseService sseService;

    @Autowired
    public DataController(DataService dataService, NewsService newsService,SseService sseService) {
        this.dataService = dataService;
        this.newsService = newsService;
        this.sseService = sseService;
    }

//  可返回当日的新闻数量，每周的新闻数量，当日每个类别的新闻数量
    @GetMapping("/news/amount")
    public ResponseEntity<ApiResponse<?>> getWeeklyNewsAmount(@RequestParam(name = "start", required = true) @DateTimeFormat(pattern = "yyyy-MM-dd") Date startDate,
                                                              @RequestParam(name = "end", required = true) @DateTimeFormat(pattern = "yyyy-MM-dd") Date endDate,
                                                              @RequestParam(name = "category", required = false) String category){
        if (startDate.after(endDate)){
            return ResponseEntity.badRequest().body(ApiResponse.BAD_REQUEST("Start date should be before end date"));
        }

        Integer amount = dataService.getNewsAmountData(startDate,endDate,category);
        return ResponseEntity.ok().body(ApiResponse.OK(amount));
    }

    // 自动获取当日的新闻数量（当日00:00 ~ 当日当前时间），前端可选择定时更新
    @GetMapping("/news/amount/daily")
    public ResponseEntity<ApiResponse<?>> getTodayNewsAmount(){
        LocalDate currentDate = LocalDate.now();
        Date today = java.sql.Date.valueOf(currentDate);
        Date tomorrow = new Date(today.getTime() + 24 * 60 * 60 * 1000);
        List<SummaryNewsAmountVO> amountSeries = dataService.getAllNewsAmountData(today,tomorrow);
        return ResponseEntity.ok().body(ApiResponse.OK(amountSeries));
    }

    @GetMapping(path = "/news/amount/daily/sse", produces = MediaType.TEXT_EVENT_STREAM_VALUE)
    public SseEmitter getTodayNewsAmountSSE() {
        String uuid = UUID.randomUUID().toString();
        log.info("新用户连接：getTodayNewsAmountSSE：{}", uuid);
        return sseService.connectDailyAmountSSE(uuid);
    }


    // 获取连续N日的新闻数量序列
    @GetMapping("/news/amount/series")
    public ResponseEntity<ApiResponse<?>> getNewsAmountSeries(@RequestParam(name = "start", required = true) @DateTimeFormat(pattern = "yyyy-MM-dd") Date startDate,
                                                              @RequestParam(name = "end", required = true) @DateTimeFormat(pattern = "yyyy-MM-dd") Date endDate
                                                             ){
        if (startDate.after(endDate)){
            return ResponseEntity.badRequest().body(ApiResponse.BAD_REQUEST("Start date should be before end date"));
        }

        List<SummaryNewsAmountVO> amountSeries = dataService.getAllNewsAmountData(startDate,endDate);

        return ResponseEntity.ok().body(ApiResponse.OK(amountSeries));
    }

    // 自动获取一周内的新闻数量（一周前00:00 ~ 当日00:00），前端可选择定时更新
    @GetMapping("/news/amount/weekly")
    public ResponseEntity<ApiResponse<?>> getWeeklyNewsAmount(@RequestParam(name = "category", required = false) String category){
        LocalDate currentDate = LocalDate.now();
        Date today = java.sql.Date.valueOf(currentDate);

        Date lastWeek = new Date(today.getTime() - 7 * 24 * 60 * 60 * 1000);
        Integer amount = dataService.getNewsAmountData(lastWeek,today,category);
        return ResponseEntity.ok().body(ApiResponse.OK(amount));
    }

    // 获取一周内，出现次数最多的关键词
    @GetMapping("/keyword/weekly")
    public ResponseEntity<ApiResponse<?>> getWeeklyKeywords(@RequestParam(name = "start", required = true) @DateTimeFormat(pattern = "yyyy-MM-dd") Date startDate,
                                                            @RequestParam(name = "end", required = true) @DateTimeFormat(pattern = "yyyy-MM-dd") Date endDate){

        KeywordAnalysisVO keywords = dataService.getHotEventsByNeo4j(startDate,endDate);
        if (keywords == null){
            keywords = dataService.getHotWordsData(startDate,endDate,1);
        }
        if (keywords == null){
            return ResponseEntity.badRequest().body(ApiResponse.BAD_REQUEST("No keyword data available, getWeeklyKeywords failed"));
        }

        return ResponseEntity.ok().body(ApiResponse.OK(keywords));
    }

    // 获取一周内，出现次数最多的人物
    @GetMapping("/keyword/person")
    public ResponseEntity<ApiResponse<?>> getWeeklyPerson(@RequestParam(name = "start", required = true) @DateTimeFormat(pattern = "yyyy-MM-dd") Date startDate,
                                                          @RequestParam(name = "end", required = true) @DateTimeFormat(pattern = "yyyy-MM-dd") Date endDate){

        KeywordAnalysisVO keywords = dataService.getHotPersonByNeo4j(startDate,endDate);
        if (keywords == null){
            keywords = dataService.getHotWordsData(startDate,endDate,2);
        }
        if (keywords == null){
            return ResponseEntity.badRequest().body(ApiResponse.BAD_REQUEST("No person data available, getWeeklyPerson failed"));
        }

        return ResponseEntity.ok().body(ApiResponse.OK(keywords));
    }

    // 获取一周内，出现次数最多的地点
    @GetMapping("/keyword/place")
    public ResponseEntity<ApiResponse<?>> getWeeklyPlace(@RequestParam(name = "start", required = true) @DateTimeFormat(pattern = "yyyy-MM-dd") Date startDate,
                                                         @RequestParam(name = "end", required = true) @DateTimeFormat(pattern = "yyyy-MM-dd") Date endDate){

        List<SummaryPlacesVO> placesVOS = dataService.getPlacesData(startDate,endDate);
        if (placesVOS == null){
            return ResponseEntity.badRequest().body(ApiResponse.BAD_REQUEST("No place data available, getWeeklyPlace failed"));
        }

        return ResponseEntity.ok().body(ApiResponse.OK(placesVOS));
    }

    // 提取最新新闻关键词。用来处理爬虫漏网之鱼
    @GetMapping("/keyword/extract/{size}/{page}")
    public ResponseEntity<ApiResponse<?>> extractLatestNews(@PathVariable(value="size") int size, @PathVariable(value="page") int page){
        Boolean result = newsService.extractLatestNewsKeyWords(size, page);
        if (!result){
            return ResponseEntity.badRequest().body(ApiResponse.BAD_REQUEST("Extract latest news keywords failed"));
        }
        return ResponseEntity.ok().body(ApiResponse.OK("extractLatestNewsKeyWords success"));
    }

    /**
     * 测试SSE连接
     *
     * @return
     */
    @GetMapping(path = "/sse/connect", produces = MediaType.TEXT_EVENT_STREAM_VALUE)
    public ResponseEntity<SseEmitter> sse(HttpServletResponse response) {
        String uuid = UUID.randomUUID().toString();
        log.info("新用户连接：{}", uuid);
        SseEmitter sseEmitter = sseService.connectNewsSSE(uuid);
        response.setContentType("text/event-stream");
        response.setCharacterEncoding("UTF-8");
        response.setHeader("Cache-Control", "no-cache");
        response.setHeader("Connection", "keep-alive");
        response.setHeader("Content-Encoding", "none");
        return ResponseEntity.ok().body(sseEmitter);
    }
    /**
     * 测试SSE广播消息
     *
     */
    @PostMapping("/sse/sendMessage")
    @ResponseBody
    public void sendMessage() {
        for (int i = 0; i < 10; i++) {
            sseService.sendMessage("message: " + i);
            try {
                sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
