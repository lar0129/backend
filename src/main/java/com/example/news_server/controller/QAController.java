package com.example.news_server.controller;

import com.example.news_server.VO.data.ConversationFeedbackVO;
import com.example.news_server.PO.Conversation;
import com.example.news_server.PO.User;
import com.example.news_server.service.ConversationService;
import com.example.news_server.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.mvc.method.annotation.SseEmitter;

import com.example.news_server.VO.QuestionVO;
import com.example.news_server.service.QAService;
import com.example.news_server.utils.ApiResponse;
import com.example.news_server.utils.sse.SseSession;

import cn.dev33.satoken.stp.StpUtil;
import jakarta.servlet.http.HttpServletResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.*;


@RestController
@Slf4j
@RequestMapping("/api/qa")
public class QAController {

    @Autowired
    private UserService userService;

    @Autowired
    private ConversationService conversationService;

    @Autowired
    private QAService qaService;

    @GetMapping("/sse")
    // 这个OK了
    public ResponseEntity<ApiResponse<?>> openSSE() {
        if (!StpUtil.isLogin()) {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(ApiResponse.BAD_REQUEST("Please login first"));
        }

        String uuid = UUID.randomUUID().toString();
        String username = StpUtil.getLoginIdAsString();

        User user = userService.findByUsername(username);
        Conversation conversation = new Conversation();
        conversation.setId(uuid);
        conversation.setUser(user); // 设置 user 以建立关联
        conversation.setTitle("默认标题");
        conversationService.save(conversation);

        qaService.openQASSE(uuid);
        log.info("SSE connection opened with uuid: {}", uuid);

        return ResponseEntity.ok().body(ApiResponse.OK(uuid));
    }

    @PostMapping(path="/question", produces = MediaType.TEXT_EVENT_STREAM_VALUE)
    public SseEmitter processQuestion(@RequestBody QuestionVO question, HttpServletResponse response) {
        if (!StpUtil.isLogin()) {
            return null;
        }
        SseEmitter emitter;
        emitter = SseSession.get(question.getUuid());
        // 检查Repository中是否有conversationId
        if(emitter==null) {
            if (conversationService.findById(question.getUuid()).isPresent()) {
                qaService.openQASSE(question.getUuid());
                emitter = SseSession.get(question.getUuid());
            } else {
                log.warn("Illegal uuid: " + question.getUuid());
                return null;
            }
        }

        qaService.processQuestion(question.getQuestion(), question.getUuid(), StpUtil.getLoginIdAsString());
        // log.info("新用户连接getSummaryNewsByPageSSE：{}", uuid);
        response.setContentType("text/event-stream");
        response.setCharacterEncoding("UTF-8");
        response.setHeader("Cache-Control", "no-cache");
        response.setHeader("Connection", "keep-alive");
        response.setHeader("Content-Encoding", "none");
        // return ResponseEntity.ok().body(ApiResponse.OK("Question received, Processing started"));
        return emitter;
    }

    @PostMapping(path="/sql-question", produces = MediaType.TEXT_EVENT_STREAM_VALUE)
    public SseEmitter processSQLQuestion(@RequestBody QuestionVO question, HttpServletResponse response) {
        if (!StpUtil.isLogin()) {
            return null;
        }
        SseEmitter emitter;
        emitter = SseSession.get(question.getUuid());
        // 检查Repository中是否有conversationId
        if(emitter==null) {
            if (conversationService.findById(question.getUuid()).isPresent()) {
                qaService.openQASSE(question.getUuid());
                emitter = SseSession.get(question.getUuid());
            } else {
                log.warn("Illegal uuid: " + question.getUuid());
                return null;
            }
        }
        qaService.generateAndExecuteSQL(question.getQuestion(), question.getUuid(), StpUtil.getLoginIdAsString());
        // log.info("新用户连接getSummaryNewsByPageSSE：{}", uuid);
        response.setContentType("text/event-stream");
        response.setCharacterEncoding("UTF-8");
        response.setHeader("Cache-Control", "no-cache");
        response.setHeader("Connection", "keep-alive");
        response.setHeader("Content-Encoding", "none");
        // return ResponseEntity.ok().body(ApiResponse.OK("Question received, Processing started"));
        return emitter;
    }

    @PostMapping("/feedback")
    public ResponseEntity<ApiResponse<?>> addFeedback(@RequestBody ConversationFeedbackVO cfb){
        if (!qaService.addFeedback(cfb.getUuid(), cfb.getAttitude(), cfb.getComment())){
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok().body(ApiResponse.OK(cfb.getUuid()));
    }

}
