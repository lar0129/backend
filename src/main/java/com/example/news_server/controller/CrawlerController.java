package com.example.news_server.controller;

import com.example.news_server.PO.News;
import com.example.news_server.VO.CrawledNewsVO;
import com.example.news_server.service.CrawlerService;
import com.example.news_server.service.SseService;
import com.example.news_server.utils.ApiResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping("/api/crawler")
public class CrawlerController {

    private final CrawlerService crawlerService;
    private final SseService sseService;

    @Autowired
    public CrawlerController(CrawlerService crawlerService, SseService sseService) {
        this.crawlerService = crawlerService;
        this.sseService = sseService;
    }

    @PostMapping("/news")
    public ResponseEntity<ApiResponse<?>> uploadNews(@RequestBody List<CrawledNewsVO> newsList) {
        if (newsList.isEmpty()) {
            return ResponseEntity.badRequest().body(ApiResponse.BAD_REQUEST("News list is empty."));
        }

        try {
            List<News> savedNews = crawlerService.addNews(newsList);
            sseService.sendUpdateNews();
            sseService.sendUpdateDailyAmount();
            return ResponseEntity.status(HttpStatus.CREATED).body(ApiResponse.CREATED(savedNews.size(), "News appended successfully"));
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.internalServerError().body(ApiResponse.INTERNAL_ERROR("Error processing news data"));
        }

    }

    // update index when new news uploaded. Mysql will do it automatically?
    @PostMapping("/index/update")
    public ResponseEntity<ApiResponse<?>> updateIndex() {
//        if (crawlerService.updateIndex()){
//            return ResponseEntity.ok().body(ApiResponse.OK("Index updated successfully"));
//        }
        return ResponseEntity.internalServerError().body(ApiResponse.INTERNAL_ERROR("Fail to update index"));
    }

    /*
      deprecated method, might be removed in the future stable version
      */
//    @PostMapping("/news")
//    public ResponseEntity<ApiResponse<?>> uploadCsvFile(@RequestParam("file") MultipartFile file) {
//        if (file.isEmpty()) {
//            return ResponseEntity.badRequest().body(ApiResponse.BAD_REQUEST("The file is empty."));
//        }
//
//        try (InputStreamReader reader = new InputStreamReader(file.getInputStream())) {
//            CsvToBean<CrawledNewsVO> csvToBean = new CsvToBeanBuilder<CrawledNewsVO>(reader)
//                    .withType(CrawledNewsVO.class)
//                    .withIgnoreLeadingWhiteSpace(true)
//                    .build();
//
//            List<CrawledNewsVO> newsList = csvToBean.parse();
//            int successful_cnt = crawlerService.addNews(newsList);
//
//            return ResponseEntity.status(HttpStatus.CREATED).body(ApiResponse.CREATED(successful_cnt,  "News append successfully"));
//        } catch (Exception e) {
//            e.printStackTrace();
//            return ResponseEntity.internalServerError().body(ApiResponse.INTERNAL_ERROR("Error processing CSV file"));
//        }
//
//    }
}
