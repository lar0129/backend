package com.example.news_server.controller;


import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.news_server.VO.ParsedNREVO;
import com.example.news_server.service.GraphService;
import com.example.news_server.utils.ApiResponse;


import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

// 这种写法真的蠢到家了，但是我真的找不到更好的办法，真是愚蠢至极

@RestController
@RequestMapping("api/graph")
public class GraphController {

    private final GraphService graphService;

    public GraphController(GraphService graphService) {
        this.graphService = graphService;
    }

    @PostMapping("/parsednre")
    public ResponseEntity<ApiResponse<?>> uploadNRE(@RequestBody ParsedNREVO nreList) {
        if (nreList.getData().isEmpty() || nreList.getId() == null) {
            return ResponseEntity.badRequest().body(ApiResponse.BAD_REQUEST("NRE list is empty."));
        }

        // GraphService graphService = ...; // 获取服务实例
        Boolean ifSuccessful = null;
        if (nreList.getDate() == null) {
            System.out.println("Inserting without date value.");
            ifSuccessful = graphService.pushNREV2(nreList.getData(), nreList.getId(), null);
        } else {
            System.out.println("Inserting with date value.");
            ifSuccessful = graphService.pushNREV2(nreList.getData(), nreList.getId(), nreList.getDate());
        }
        if(ifSuccessful)
        // 返回一个接受异步结果的响应
            return ResponseEntity.accepted().body(ApiResponse.OK(ifSuccessful, "NRE appending succeeded"));
        else
            return ResponseEntity.internalServerError().body(ApiResponse.INTERNAL_ERROR("Error processing NRE data"));
    }

    @GetMapping("")
    public ResponseEntity<ApiResponse<?>> getGraph(@RequestParam String newsId, @RequestParam String nodeName) {
        try {
            if (!newsId.isEmpty()) {
                return ResponseEntity.ok().body(ApiResponse.OK(graphService.getGraphByNewsId(newsId)));
            } else if (!nodeName.isEmpty()) {
                return ResponseEntity.ok().body(ApiResponse.OK(graphService.getGraphByNodeName(nodeName)));
            } else {
                return ResponseEntity.badRequest()
                        .body(ApiResponse.BAD_REQUEST("Missing RequestParam: newsId or nodeName"));
            }
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.badRequest().body(ApiResponse.BAD_REQUEST("Failed to get graph by newsId or nodeName"));
        }
    }

    @GetMapping("/limited")
    public ResponseEntity<ApiResponse<?>> getGraphLimited(@RequestParam String newsId, @RequestParam String nodeName) {
        try {
            if (!newsId.isEmpty()) {
                return ResponseEntity.ok().body(ApiResponse.OK(graphService.getGraphByNewsIdLimited(newsId)));
            } else if (!nodeName.isEmpty()) {
                return ResponseEntity.ok().body(ApiResponse.OK(graphService.getGraphByNodeNameLimited(nodeName)));
            } else {
                return ResponseEntity.badRequest()
                        .body(ApiResponse.BAD_REQUEST("Missing RequestParam: newsId or nodeName"));
            }
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.badRequest().body(ApiResponse.BAD_REQUEST("Failed to get graph by newsId or nodeName"));
        }
    }

    @GetMapping("/timedNode")
    public ResponseEntity<ApiResponse<?>> getGraphByTime(@RequestParam String startTime, @RequestParam String endTime) {
        try {
            return ResponseEntity.ok().body(ApiResponse.OK(graphService.getGraphByTime(startTime, endTime)));
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.badRequest().body(ApiResponse.BAD_REQUEST("Failed to get graph by time"));
        }
    }

    @GetMapping("/relatedNews")
    public ResponseEntity<ApiResponse<?>> getRelatedNews(@RequestParam Long id) {
        try {
            return ResponseEntity.ok().body(ApiResponse.OK(graphService.getRelatedNewsByNews(id)));
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.badRequest().body(ApiResponse.BAD_REQUEST("Failed to get related news, probably the id is not recorded in our graph database."));
        }
    }
    

    @GetMapping("/hotest/person")
    public ResponseEntity<ApiResponse<?>> getHotestPerson(@RequestParam String startTime, @RequestParam String endTime) {
        try {
            return ResponseEntity.ok().body(ApiResponse.OK(graphService.getHotestPerson(startTime, endTime)));
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.badRequest().body(ApiResponse.BAD_REQUEST("Failed to get hotest person"));
        }
    }

    @GetMapping("/hotest/event")
    public ResponseEntity<ApiResponse<?>> getHotestEvent(@RequestParam String startTime, @RequestParam String endTime) {
        try {
            return ResponseEntity.ok().body(ApiResponse.OK(graphService.getHotestEvent(startTime, endTime)));
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.badRequest().body(ApiResponse.BAD_REQUEST("Failed to get hotest event"));
        }
    }

    @GetMapping("/hotest/organization")
    public ResponseEntity<ApiResponse<?>> getHotestOrganization(@RequestParam String startTime,
            @RequestParam String endTime) {
        try {
            return ResponseEntity.ok().body(ApiResponse.OK(graphService.getHotestOrganization(startTime, endTime)));
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.badRequest().body(ApiResponse.BAD_REQUEST("Failed to get hotest organization"));
        }
    }

    @GetMapping("/hotest/idea")
    public ResponseEntity<ApiResponse<?>> getHotestIdea(@RequestParam String startTime, @RequestParam String endTime) {
        try {
            return ResponseEntity.ok().body(ApiResponse.OK(graphService.getHotestIdea(startTime, endTime)));
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.badRequest().body(ApiResponse.BAD_REQUEST("Failed to get hotest idea"));
        }
    }

    // @GetMapping("/")
    // public ResponseEntity<ApiResponse<?>> getGraphByNodeName(@RequestParam String
    // nodeName) {
    // try {
    // return
    // ResponseEntity.ok().body(ApiResponse.OK(graphService.getGraphByNodeName(nodeName)));
    // } catch (Exception e) {
    // e.printStackTrace();
    // return ResponseEntity.badRequest().body(ApiResponse.BAD_REQUEST("Failed to
    // get graph by nodeName"));
    // }
    // }

}
