package com.example.news_server.controller;

import com.example.news_server.PO.Conversation;
import com.example.news_server.PO.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import com.example.news_server.VO.UserVO;
import com.example.news_server.service.UserService;

import cn.dev33.satoken.stp.StpUtil;
import cn.dev33.satoken.util.SaResult;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/user")
public class UserController {

    @Autowired
    PasswordEncoder passwordEncoder;

    @Autowired
    UserService userService;

    @PostMapping("/login")
    public SaResult login(@RequestBody UserVO userVO) {
        if(userService.authenticate(userVO)) {
            StpUtil.login(userVO.getUsername());
            return SaResult.ok("Login success");
        } else {
            return SaResult.error("Login failed");
        }
    }

    @PostMapping("/register")
    public SaResult register(@RequestBody UserVO userVO) {
        if(userService.register(userVO) != null) {
            return SaResult.ok("Register success");
        } else {
            return SaResult.error("Register failed");
        }
    }

    @GetMapping("/logout")
    public SaResult logout() {
        StpUtil.logout();
        return SaResult.ok("Logout success");
    }

    @GetMapping("/conversations")
    // 这个OK了
    public ResponseEntity<List<String>> getUserConversations() {
        String  username = StpUtil.getLoginIdAsString();
        User user = userService.findByUsername(username);
        if (user == null) {
            return ResponseEntity.notFound().build();
        }

        List<String> conversationIds = user.getConversations().stream()
                .map(Conversation::getId)
                .collect(Collectors.toList());
        return ResponseEntity.ok(conversationIds);
    }

}
