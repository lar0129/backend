package com.example.news_server.controller;
import com.example.news_server.PO.ErrorId;
import com.example.news_server.service.ErrorDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping("/error-ids")
public class ErrorIdController {

    private final ErrorDataService errorDataService;

    @Autowired
    public ErrorIdController(ErrorDataService errorDataService) {
        this.errorDataService = errorDataService;
    }

    @PostMapping("/error-id")
    public ResponseEntity<String> saveErrorId(@RequestBody String errorId) {
        // 执行存储错误ID的操作
        errorDataService.saveErrorId(errorId);
        return ResponseEntity.status(HttpStatus.CREATED).body("Error ID saved successfully");
    }

    @GetMapping("/all-error-ids")
    public ResponseEntity<List<ErrorId>> getAllErrorIds() {
        List<ErrorId> errorIds = errorDataService.getAllErrorIds();
        return ResponseEntity.ok(errorIds);
    }



    // 其他方法...

}
