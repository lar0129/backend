package com.example.news_server.controller;

import com.example.news_server.VO.NewsListVO;
import com.example.news_server.VO.NewsPageVO;
import com.example.news_server.VO.PatchNewsVO;
import com.example.news_server.VO.SummaryNews;
import com.example.news_server.service.NewsService;
import com.example.news_server.service.SseService;
import com.example.news_server.utils.ApiResponse;
import jakarta.servlet.http.HttpServletResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.method.annotation.SseEmitter;

import java.util.Date;
import java.util.List;
import java.util.UUID;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;


@Slf4j
@RestController
@RequestMapping("/api/news")
public class NewsController {

    private final NewsService newsService;
    private final SseService sseService;

    @Autowired
    public NewsController(NewsService newsService, SseService sseService) {
        this.newsService = newsService;
        this.sseService = sseService;
    }

    @GetMapping("/{id}")
    public ResponseEntity<ApiResponse<?>> getNewsById(@PathVariable Long id) {
        String content = newsService.getNewsContent(id);
        if (content.equals("")){
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(ApiResponse.NOT_FOUND("Can not find message with id " + id));
        }
        return ResponseEntity.ok().body(ApiResponse.OK(content));
    }

    @GetMapping("/search")
    public ResponseEntity<ApiResponse<?>> getNewsFilter(@RequestParam(name = "sentence", defaultValue = "") String sentence,
                                                        @RequestParam(name = "top", defaultValue = "10") Long top) {
        if(sentence.isEmpty() || top == 0){
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(ApiResponse.NOT_FOUND("Can not find News with sentence " + sentence));
        }

        List<SummaryNews> content = newsService.findNewsBySentence(sentence,top);
        if (content.isEmpty()){
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(ApiResponse.NOT_FOUND("Can not find News with sentence " + sentence));
        }
        return ResponseEntity.ok().body(ApiResponse.OK(content));
    }

    @PatchMapping("/{id}")
    public ResponseEntity<ApiResponse<?>> updateNews(@PathVariable Long id, @RequestBody PatchNewsVO news) {
        if (newsService.alterNews(id, news)){
            return ResponseEntity.ok().body(ApiResponse.OK(null));
        }
        return ResponseEntity.internalServerError().body(ApiResponse.INTERNAL_ERROR("Fail to update news with id " + id));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<ApiResponse<?>> deleteNews(@PathVariable(value="id") Long id) {
        boolean res = newsService.deleteNewsById(id);
        if (res){
            return ResponseEntity.ok().body(ApiResponse.OK(null));
        }
        return ResponseEntity.internalServerError().body(ApiResponse.INTERNAL_ERROR("Fail to delete news with id "+ id));
    }

    @GetMapping("/classify/{id}")
    public ResponseEntity<ApiResponse<?>> classifyNews(@PathVariable(value="id") Long id) {
        String res = newsService.classifyNewsById(id);
        if (res != null){
            return ResponseEntity.ok().body(ApiResponse.OK(null, res));
        }
        return ResponseEntity.internalServerError().body(ApiResponse.INTERNAL_ERROR("news doesn't exist with id "+ id));
    }

    @GetMapping("/classify/latest/{size}/{page}") // 分类最新新闻。用来处理爬虫漏网之鱼
    public ResponseEntity<ApiResponse<?>> classifyLatestNews(@PathVariable(value="size") int size, @PathVariable(value="page") int page){
        newsService.classifyLatestNews(size, page);
        return ResponseEntity.ok().body(ApiResponse.OK(null));
    }

    // 分析新闻可信度
    @GetMapping("/credibility/{id}")
    public ResponseEntity<ApiResponse<?>> credibilityNews(@PathVariable(value="id") Long id) {
        Double res = newsService.credibilityNewsById(id);
        if (res != null){
            return ResponseEntity.ok().body(ApiResponse.OK(res, "get credibility as " + String.format("%.3f", res)));
        }
        return ResponseEntity.internalServerError().body(ApiResponse.INTERNAL_ERROR("news doesn't exist with id "+ id));
    }

    @GetMapping("/credibility/latest/{size}/{page}") // 提取最新新闻可信度
    public ResponseEntity<ApiResponse<?>> credibilityLatestNews(@PathVariable(value="size") int size, @PathVariable(value="page") int page){
        newsService.credibilityLatestNews(size, page);
        return ResponseEntity.ok().body(ApiResponse.OK(null));
    }

    @GetMapping("/timedList")
    public ResponseEntity<ApiResponse<?>> getTimedNews(@RequestParam (name = "start") @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm") Date startDate,
                                                      @RequestParam (name = "end") @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm") Date endDate) {
        NewsListVO newsListVO = newsService.getTimedNews(startDate, endDate);
        if (newsListVO.list.isEmpty()){
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(ApiResponse.NOT_FOUND("Can not find News with time between " + startDate + " and " + endDate));
        }
        return ResponseEntity.ok().body(ApiResponse.OK(newsListVO));
    }
    


    @GetMapping("/list")
    public ResponseEntity<ApiResponse<?>> getSummaryNewsByPage(
            @RequestParam(name = "size", defaultValue = "0") int size,
            @RequestParam(name = "current", defaultValue = "0") int current,
            @RequestParam(name = "source", required = false) String source,
            @RequestParam(name = "start", required = false) @DateTimeFormat(pattern = "yyyy-MM-dd") Date startDate,
            @RequestParam(name = "end", required = false) @DateTimeFormat(pattern = "yyyy-MM-dd") Date endDate,
            @RequestParam(name = "category", required = false) String category){

        if (size <= 0){
            NewsPageVO effect = newsService.getNewsPage(10, 0, source, startDate, endDate,category);

            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(ApiResponse.BAD_REQUEST("Page size must not be smaller than 1, pages at size of 10 is " + effect.count,
                    effect.count));
        }
        if (current < 0){
            NewsPageVO effect = newsService.getNewsPage(size, 0, source, startDate, endDate, category);
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(ApiResponse.BAD_REQUEST("Current page index smaller than 0", effect.count));
        }
        NewsPageVO pageVO = newsService.getNewsPage(size, current, source, startDate, endDate, category);
        if (pageVO.page.isEmpty()){
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(ApiResponse.BAD_REQUEST("Page index too large!", pageVO.count));
        }
        return ResponseEntity.ok().body(ApiResponse.OK(pageVO));
    }

    @GetMapping(path = "/list/sse", produces = MediaType.TEXT_EVENT_STREAM_VALUE)
    public SseEmitter getSummaryNewsByPageSSE(HttpServletResponse response) {
        String uuid = UUID.randomUUID().toString();
        log.info("新用户连接getSummaryNewsByPageSSE：{}", uuid);
        response.setContentType("text/event-stream");
        response.setCharacterEncoding("UTF-8");
        response.setHeader("Cache-Control", "no-cache");
        response.setHeader("Connection", "keep-alive");
        response.setHeader("Content-Encoding", "none");
        return sseService.connectNewsSSE(uuid);
    }

    // 其他操作方法
}
