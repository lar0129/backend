package com.example.news_server.controller;

import com.example.news_server.PO.QuestionAndAnswer;
import com.example.news_server.VO.QuestionAndAnswerVO;
import com.example.news_server.service.ConversationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/conversations")
public class ConversationController {

    @Autowired
    private ConversationService conversationService;



    @GetMapping("/{uuid}/questions")
    public ResponseEntity<List<Long>> getQuestionAndAnswerIds(@PathVariable String uuid) {
        return conversationService.findById(uuid)
                .map(conversation -> {
                    List<Long> questionAndAnswerIds = conversation.getQuestionAndAnswers().stream()
                            .map(QuestionAndAnswer::getId)
                            .collect(Collectors.toList());
                    return ResponseEntity.ok(questionAndAnswerIds);
                })
                .orElse(ResponseEntity.notFound().build());
    }

    @GetMapping("/{uuid}/questions/content")
    public ResponseEntity<List<QuestionAndAnswerVO>> getQuestionAndAnswerContent(@PathVariable String uuid) {
        List<QuestionAndAnswerVO> qs = conversationService.getQuestionAndAnswerVOs(uuid);
        if (qs.isEmpty()){
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok().body(qs);
    }
}
