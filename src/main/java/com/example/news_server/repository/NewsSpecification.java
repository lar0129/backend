package com.example.news_server.repository;

import com.example.news_server.PO.News;
import com.example.news_server.utils.enums.Category;
import org.springframework.data.jpa.domain.Specification;

import java.util.Date;

public class NewsSpecification {
    public static Specification<News> isFromSource(String source){
        return (root, query, criteriaBuilder) -> source == null ?
                criteriaBuilder.conjunction() :
                source.isEmpty()? criteriaBuilder.conjunction() :
                criteriaBuilder.equal(root.get("source"), source);
    }

    public static Specification<News> laterThanStart(Date date){
        return (root, query, criteriaBuilder) -> date == null?
                criteriaBuilder.conjunction():
                criteriaBuilder.greaterThanOrEqualTo(root.get("publishDate"), date);
    }

    public static Specification<News> earlierThanEnd(Date date){
        return (root, query, criteriaBuilder) -> date == null?
                criteriaBuilder.conjunction():
                criteriaBuilder.lessThanOrEqualTo(root.get("publishDate"), date);
    }

    public static Specification<News> inCategory(String category){
        return (root, query, criteriaBuilder) -> {
            if (category == null) return criteriaBuilder.conjunction();
            if (category.isEmpty())return criteriaBuilder.conjunction();
            int id = Category.parseString(category).getId();
            if (id == -1)return criteriaBuilder.disjunction();
            return criteriaBuilder.equal(root.get("category"), id);
        };
    }
}
