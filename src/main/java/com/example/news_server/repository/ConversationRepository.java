package com.example.news_server.repository;

import com.example.news_server.PO.Conversation;
import org.jetbrains.annotations.NotNull;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface ConversationRepository extends JpaRepository<Conversation, String> {

    @NotNull
    Optional<Conversation> findById(@NotNull String id);
}
