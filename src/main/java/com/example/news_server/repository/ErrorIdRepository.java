package com.example.news_server.repository;

import com.example.news_server.PO.ErrorId;
import org.springframework.data.repository.CrudRepository;

public interface ErrorIdRepository extends CrudRepository<ErrorId, Long> {
    // 可以添加自定义的方法
}
