package com.example.news_server.repository;

import com.example.news_server.PO.News;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

import java.util.Date;
import java.util.List;

public interface NewsRepository extends JpaRepository<News, Long>, JpaSpecificationExecutor<News>{
    List<News> findByCategory(int category);

    Page<News> findAll(Pageable pageable);
    Page<News> findAllByOrderByIdDesc(Pageable pageable);
    // 全文检索SQL语句
    List<News> findNewsByTitleLike(String word);

//     List<NewsforNRE> findNewsforNREsBy

    @Query(value = "SELECT * FROM news WHERE MATCH(title) " +
            "AGAINST (:sentence IN NATURAL LANGUAGE MODE) LIMIT :top", nativeQuery = true)
    List<News> findTitleBySentence(String sentence, Long top);

    @Query(value = "SELECT * FROM news WHERE MATCH(title, content) " +
            "AGAINST (:sentence IN NATURAL LANGUAGE MODE) LIMIT :top", nativeQuery = true)
    List<News> findBySentence(String sentence, Long top);

    // 数据分析
    Integer countByPublishDateBetween(Date startDate, Date endDate);
    Integer countByPublishDateBetweenAndCategory(Date startDate, Date endDate, int categoryId);

}
