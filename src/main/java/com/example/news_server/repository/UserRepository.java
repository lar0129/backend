package com.example.news_server.repository;

import com.example.news_server.PO.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, Long> {
    User findByUsername(String username);

    Long findIdByUsername(String username);
}
