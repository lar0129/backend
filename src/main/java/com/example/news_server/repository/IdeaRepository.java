package com.example.news_server.repository;

import org.springframework.data.neo4j.repository.Neo4jRepository;
import org.springframework.data.neo4j.repository.query.Query;
import org.springframework.data.repository.query.Param; // Import the Param class

import com.example.news_server.PO.IdeaNode;

public interface IdeaRepository extends Neo4jRepository<IdeaNode, String>{
    
    @Query("MATCH (i:Idea {name: $name}) RETURN i Limit 1")
    IdeaNode findOneByNameLimited(@Param("name") String name);

    // @Query("MATCH (i:Idea)-[r*0..1]->(related) WHERE i.name = $name RETURN i, COLLECT({relationship: r, related: related}) AS relatedNodes LIMIT 1")
    IdeaNode findOneByName(@Param("name") String name);

    @Query("MERGE (n1:Idea {name: $nodeName1}) " +
    "MERGE (n2:Idea {name: $nodeName2}) " +
    "CREATE (n1)-[r:IdeaEffectsIdea]->(n2) SET r.strength = $strength " +
    "WITH n1, n2, $newsId AS newsId " +
    "SET n1.related_news = CASE " +
    "WHEN n1.related_news IS NULL THEN [newsId] " +
    "ELSE n1.related_news + [newsId] " +
    "END, " +
    "n2.related_news = CASE " +
    "WHEN n2.related_news IS NULL THEN [newsId] " +
    "ELSE n2.related_news + [newsId] " +
    "END" + 
    "SET n1.time = CASE " +
    "WHEN n1.time IS NULL THEN [datetime(date)] " +
    "ELSE n1.time + [datetime(date)] " +
    "SET n2.time = CASE " +
    "WHEN n2.time IS NULL THEN [datetime(date)] " +
    "ELSE n2.time + [datetime(date)] " +
    "END")
    void createOrMatchNodesAndIdeaEffectsIdea(String nodeName1, String nodeName2, double strength, String newsId, String datetime);

    @Query("MERGE (n1:Idea {name: $nodeName1}) " +
    "MERGE (n2:Idea {name: $nodeName2}) " +
    "CREATE (n1)-[r:IdeaEffectsIdea]->(n2) SET r.strength = $strength " +
    "WITH n1, n2, $newsId AS newsId " +
    "SET n1.related_news = CASE " +
    "WHEN n1.related_news IS NULL THEN [newsId] " +
    "ELSE n1.related_news + [newsId] " +
    "END, " +
    "n2.related_news = CASE " +
    "WHEN n2.related_news IS NULL THEN [newsId] " +
    "ELSE n2.related_news + [newsId] " +
    "END")
    void createOrMatchNodesAndIdeaEffectsIdea(String nodeName1, String nodeName2, double strength, String newsId);

    @Query("MERGE (n1:Idea {name: $nodeName1}) " +
    "MERGE (n2:Organization {name: $nodeName2}) " +
    "CREATE (n1)-[r:IdeaEffectsOrganization]->(n2) SET r.strength = $strength " +
    "WITH n1, n2, $newsId AS newsId, $datetime as date " +
    "SET n1.related_news = CASE " +
    "WHEN n1.related_news IS NULL THEN [newsId] " +
    "ELSE n1.related_news + [newsId] " +
    "END, " +
    "n2.related_news = CASE " +
    "WHEN n2.related_news IS NULL THEN [newsId] " +
    "ELSE n2.related_news + [newsId] " +
    "END" + 
    "SET n1.time = CASE " +
    "WHEN n1.time IS NULL THEN [datetime(date)] " +
    "ELSE n1.time + [datetime(date)] " +
    "SET n2.time = CASE " +
    "WHEN n2.time IS NULL THEN [datetime(date)] " +
    "ELSE n2.time + [datetime(date)] " +
    "END")
    void createOrMatchNodesAndIdeaEffectsOrganization(String nodeName1, String nodeName2, double strength, String newsId, String datetime);

    @Query("MERGE (n1:Idea {name: $nodeName1}) " +
    "MERGE (n2:Organization {name: $nodeName2}) " +
    "CREATE (n1)-[r:IdeaEffectsOrganization]->(n2) SET r.strength = $strength " +
    "WITH n1, n2, $newsId AS newsId " +
    "SET n1.related_news = CASE " +
    "WHEN n1.related_news IS NULL THEN [newsId] " +
    "ELSE n1.related_news + [newsId] " +
    "END, " +
    "n2.related_news = CASE " +
    "WHEN n2.related_news IS NULL THEN [newsId] " +
    "ELSE n2.related_news + [newsId] " +
    "END")
    void createOrMatchNodesAndIdeaEffectsOrganization(String nodeName1, String nodeName2, double strength, String newsId);

    @Query("MERGE (n1:Idea {name: $nodeName1}) " +
    "MERGE (n2:Person {name: $nodeName2}) " +
    "CREATE (n1)-[r:IdeaEffectsPerson]->(n2) SET r.strength = $strength " +
    "WITH n1, n2, $newsId AS newsId $datetime AS date" +
    "SET n1.related_news = CASE " +
    "WHEN n1.related_news IS NULL THEN [newsId] " +
    "ELSE n1.related_news + [newsId] " +
    "END, " +
    "n2.related_news = CASE " +
    "WHEN n2.related_news IS NULL THEN [newsId] " +
    "ELSE n2.related_news + [newsId] " +
    "END" + 
    "SET n1.time = CASE " +
    "WHEN n1.time IS NULL THEN [datetime(date)] " +
    "ELSE n1.time + [datetime(date)] " +
    "SET n2.time = CASE " +
    "WHEN n2.time IS NULL THEN [datetime(date)] " +
    "ELSE n2.time + [datetime(date)] " +
    "END")
    void createOrMatchNodesAndIdeaEffectsPerson(String nodeName1, String nodeName2, double strength, String newsId, String datetime);

    @Query("MERGE (n1:Idea {name: $nodeName1}) " +
    "MERGE (n2:Person {name: $nodeName2}) " +
    "CREATE (n1)-[r:IdeaEffectsPerson]->(n2) SET r.strength = $strength " +
    "WITH n1, n2, $newsId AS newsId " +
    "SET n1.related_news = CASE " +
    "WHEN n1.related_news IS NULL THEN [newsId] " +
    "ELSE n1.related_news + [newsId] " +
    "END, " +
    "n2.related_news = CASE " +
    "WHEN n2.related_news IS NULL THEN [newsId] " +
    "ELSE n2.related_news + [newsId] " +
    "END")
    void createOrMatchNodesAndIdeaEffectsPerson(String nodeName1, String nodeName2, double strength, String newsId);

    @Query("MERGE (n1:Idea {name: $nodeName1}) " +
    "MERGE (n2:Event {name: $nodeName2}) " +
    "CREATE (n1)-[r:IdeaEffectsEvent]->(n2) SET r.strength = $strength " +
    "WITH n1, n2, $newsId AS newsId " +
    "SET n1.related_news = CASE " +
    "WHEN n1.related_news IS NULL THEN [newsId] " +
    "ELSE n1.related_news + [newsId] " +
    "END, " +
    "n2.related_news = CASE " +
    "WHEN n2.related_news IS NULL THEN [newsId] " +
    "ELSE n2.related_news + [newsId] " +
    "END" + 
    "SET n1.time = CASE " +
    "WHEN n1.time IS NULL THEN [datetime(date)] " +
    "ELSE n1.time + [datetime(date)] " +
    "SET n2.time = CASE " +
    "WHEN n2.time IS NULL THEN [datetime(date)] " +
    "ELSE n2.time + [datetime(date)] " +
    "END")
    void createOrMatchNodesAndIdeaEffectsEvent(String nodeName1, String nodeName2, double strength, String newsId, String datetime);

    @Query("MERGE (n1:Idea {name: $nodeName1}) " +
    "MERGE (n2:Event {name: $nodeName2}) " +
    "CREATE (n1)-[r:IdeaEffectsEvent]->(n2) SET r.strength = $strength " +
    "WITH n1, n2, $newsId AS newsId " +
    "SET n1.related_news = CASE " +
    "WHEN n1.related_news IS NULL THEN [newsId] " +
    "ELSE n1.related_news + [newsId] " +
    "END, " +
    "n2.related_news = CASE " +
    "WHEN n2.related_news IS NULL THEN [newsId] " +
    "ELSE n2.related_news + [newsId] " +
    "END")
    void createOrMatchNodesAndIdeaEffectsEvent(String nodeName1, String nodeName2, double strength, String newsId);
}
