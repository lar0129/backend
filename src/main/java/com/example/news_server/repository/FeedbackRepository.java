package com.example.news_server.repository;

import com.example.news_server.PO.FeedbackPO;
import org.springframework.data.jpa.repository.JpaRepository;

public interface FeedbackRepository extends JpaRepository<FeedbackPO, String> {
}
