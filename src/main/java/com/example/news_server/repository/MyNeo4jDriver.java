package com.example.news_server.repository;

import org.neo4j.driver.AuthTokens;
import org.neo4j.driver.Driver;
import org.neo4j.driver.GraphDatabase;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import jakarta.annotation.PreDestroy;
import lombok.Getter;

@Component
public class MyNeo4jDriver {

    // 在经历spring data neo4j屎一样的设计后，我确信糟糕的抽象带来的恶劣影响远比没有任何抽象严重

    // 构造函数，从application properties中获取数据库连接信息并建立数据库连接
    // @Autowired
    public MyNeo4jDriver(Environment env) {
        // 从application properties中获取数据库连接信息
        String url = env.getProperty("spring.neo4j.uri");
        String username = env.getProperty("spring.neo4j.authentication.username");
        String password = env.getProperty("spring.neo4j.authentication.password");
        // 建立数据库连接
        driver = GraphDatabase.driver(url, AuthTokens.basic(username, password));
        // 检查连接
        try {
            driver.verifyConnectivity();
        } catch (Exception e) {
            System.out.println("Neo4j连接失败");
            e.printStackTrace();
        }
    }

    @PreDestroy
    public void close() {
        System.out.println("Neo4j连接关闭");
        if (driver != null) {
            driver.close();
        }
    }

    @Getter
    private Driver driver;


}
