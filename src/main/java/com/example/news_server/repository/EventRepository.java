package com.example.news_server.repository;

import org.springframework.data.neo4j.repository.Neo4jRepository;
import org.springframework.data.neo4j.repository.query.Query;
import org.springframework.data.repository.query.Param;

import com.example.news_server.PO.EventNode;

import java.util.Date;
import java.util.Optional;
// import org.springframework.data.neo4j.repository.Depth;

public interface EventRepository extends Neo4jRepository<EventNode, String> {

        @Query("MATCH (i:Event {name: $name}) RETURN i Limit 1")
        EventNode findOneByNameLimited(@Param("name") String name);

        // @Query("MATCH (e:Event)-[r*0..1]->(related) WHERE e.name = $name RETURN e,
        // COLLECT({relationship: r, related: related}) AS relatedNodes LIMIT 1")
        EventNode findOneByName(@Param("name") String name);

        @Query("MERGE (n1:Event {name: $nodeName1}) " +
        "MERGE (n2:Event {name: $nodeName2}) " +
        "CREATE (n1)-[r:EventEffectsEvent]->(n2) SET r.strength = $strength " +
        "WITH n1, n2, $newsId AS newsId " +
        "SET n1.related_news = CASE " +
        "WHEN n1.related_news IS NULL THEN [newsId] " +
        "ELSE n1.related_news + [newsId] " +
        "END, " +
        "n2.related_news = CASE " +
        "WHEN n2.related_news IS NULL THEN [newsId] " +
        "ELSE n2.related_news + [newsId] " +
        "END")
        void createOrMatchNodesAndEventEffectsEvent(String nodeName1, String nodeName2, double strength, String newsId);

        @Query("MERGE (n1:Event {name: $nodeName1}) " +
        "MERGE (n2:Idea {name: $nodeName2}) " +
        "CREATE (n1)-[r:EventEffectsIdea]->(n2) SET r.strength = $strength " +
        "WITH n1, n2, $newsId AS newsId " +
        "SET n1.related_news = CASE " +
        "WHEN n1.related_news IS NULL THEN [newsId] " +
        "ELSE n1.related_news + [newsId] " +
        "END, " +
        "n2.related_news = CASE " +
        "WHEN n2.related_news IS NULL THEN [newsId] " +
        "ELSE n2.related_news + [newsId] " +
        "END" +
        "SET n1.time = CASE " +
        "WHEN n1.time IS NULL THEN [datetime(date)] " +
        "ELSE n1.time + [datetime(date)] " +
        "SET n2.time = CASE " +
        "WHEN n2.time IS NULL THEN [datetime(date)] " +
        "ELSE n2.time + [datetime(date)] " +
        "END")
        void createOrMatchNodesAndEventEffectsEvent(String nodeName1, String nodeName2, double strength, String newsId, String datetime);

        @Query("MERGE (n1:Event {name: $nodeName1}) " +
        "MERGE (n2:Idea {name: $nodeName2}) " +
        "CREATE (n1)-[r:EventEffectsIdea]->(n2) SET r.strength = $strength " +
        "WITH n1, n2, $newsId AS newsId " +
        "SET n1.related_news = CASE " +
        "WHEN n1.related_news IS NULL THEN [newsId] " +
        "ELSE n1.related_news + [newsId] " +
        "END, " +
        "n2.related_news = CASE " +
        "WHEN n2.related_news IS NULL THEN [newsId] " +
        "ELSE n2.related_news + [newsId] " +
        "END")
        void createOrMatchNodesAndEventEffectsIdea(String nodeName1, String nodeName2, double strength, String newsId);

        @Query("MERGE (n1:Event {name: $nodeName1}) " +
        "MERGE (n2:Idea {name: $nodeName2}) " +
        "CREATE (n1)-[r:EventEffectsIdea]->(n2) SET r.strength = $strength " +
        "WITH n1, n2, $newsId AS newsId " +
        "SET n1.related_news = CASE " +
        "WHEN n1.related_news IS NULL THEN [newsId] " +
        "ELSE n1.related_news + [newsId] " +
        "END, " +
        "n2.related_news = CASE " +
        "WHEN n2.related_news IS NULL THEN [newsId] " +
        "ELSE n2.related_news + [newsId] " +
        "END" +
        "SET n1.time = CASE " +
        "WHEN n1.time IS NULL THEN [datetime(date)] " +
        "ELSE n1.time + [datetime(date)] " +
        "SET n2.time = CASE " +
        "WHEN n2.time IS NULL THEN [datetime(date)] " +
        "ELSE n2.time + [datetime(date)] " +
        "END")
        void createOrMatchNodesAndEventEffectsIdea(String nodeName1, String nodeName2, double strength, String newsId, String datetime);

        @Query("MERGE (n1:Event {name: $nodeName1}) " +
        "MERGE (n2:Organization {name: $nodeName2}) " +
        "CREATE (n1)-[r:EventEffectsOrganization]->(n2) SET r.strength = $strength " +
        "WITH n1, n2, $newsId AS newsId " +
        "SET n1.related_news = CASE " +
        "WHEN n1.related_news IS NULL THEN [newsId] " +
        "ELSE n1.related_news + [newsId] " +
        "END, " +
        "n2.related_news = CASE " +
        "WHEN n2.related_news IS NULL THEN [newsId] " +
        "ELSE n2.related_news + [newsId] " +
        "END")
        void createOrMatchNodesAndEventEffectsOrganization(String nodeName1, String nodeName2, double strength, String newsId);

        @Query("MERGE (n1:Event {name: $nodeName1}) " +
        "MERGE (n2:Organization {name: $nodeName2}) " +
        "CREATE (n1)-[r:EventEffectsOrganization]->(n2) SET r.strength = $strength " +
        "WITH n1, n2, $newsId AS newsId " +
        "SET n1.related_news = CASE " +
        "WHEN n1.related_news IS NULL THEN [newsId] " +
        "ELSE n1.related_news + [newsId] " +
        "END, " +
        "n2.related_news = CASE " +
        "WHEN n2.related_news IS NULL THEN [newsId] " +
        "ELSE n2.related_news + [newsId] " +
        "END" + 
        "SET n1.time = CASE " +
        "WHEN n1.time IS NULL THEN [datetime(date)] " +
        "ELSE n1.time + [datetime(date)] " +
        "SET n2.time = CASE " +
        "WHEN n2.time IS NULL THEN [datetime(date)] " +
        "ELSE n2.time + [datetime(date)] " +
        "END")
        void createOrMatchNodesAndEventEffectsOrganization(String nodeName1, String nodeName2, double strength, String newsId, String datetime);

        @Query("MERGE (n1:Event {name: $nodeName1}) " +
        "MERGE (n2:Person {name: $nodeName2}) " +
        "CREATE (n1)-[r:EventEffectsPerson]->(n2) SET r.strength = $strength " +
        "WITH n1, n2, $newsId AS newsId " +
        "SET n1.related_news = CASE " +
        "WHEN n1.related_news IS NULL THEN [newsId] " +
        "ELSE n1.related_news + [newsId] " +
        "END, " +
        "n2.related_news = CASE " +
        "WHEN n2.related_news IS NULL THEN [newsId] " +
        "ELSE n2.related_news + [newsId] " +
        "END")
        void createOrMatchNodesAndEventEffectsPerson(String nodeName1, String nodeName2, double strength, String newsId);

        @Query("MERGE (n1:Event {name: $nodeName1}) " +
        "MERGE (n2:Person {name: $nodeName2}) " +
        "CREATE (n1)-[r:EventEffectsPerson]->(n2) SET r.strength = $strength " +
        "WITH n1, n2, $newsId AS newsId " +
        "SET n1.related_news = CASE " +
        "WHEN n1.related_news IS NULL THEN [newsId] " +
        "ELSE n1.related_news + [newsId] " +
        "END, " +
        "n2.related_news = CASE " +
        "WHEN n2.related_news IS NULL THEN [newsId] " +
        "ELSE n2.related_news + [newsId] " +
        "END" + 
        "SET n1.time = CASE " +
        "WHEN n1.time IS NULL THEN [datetime(date)] " +
        "ELSE n1.time + [datetime(date)] " +
        "SET n2.time = CASE " +
        "WHEN n2.time IS NULL THEN [datetime(date)] " +
        "ELSE n2.time + [datetime(date)] " +
        "END")
        void createOrMatchNodesAndEventEffectsPerson(String nodeName1, String nodeName2, double strength, String newsId, String datetime);

        // 返回和指定Name时间相关的50个节点及其关系
        @Query("MATCH (m:Event {name: $name}) - [r] -> (e) With m, collect(r) As rc, collect(e) As ec Return m, rc, ec Limit 10")
        EventNode findOneByNameLimit50(@Param("name") String name);
}
