package com.example.news_server.repository;

import com.example.news_server.PO.QuestionAndAnswer;
import org.springframework.data.jpa.repository.JpaRepository;

import java.awt.print.Pageable;
import java.util.List;

public interface QuestionAndAnswerRepository extends JpaRepository<QuestionAndAnswer, Long> {
}
