package com.example.news_server.repository;

import org.springframework.data.neo4j.repository.Neo4jRepository;
import org.springframework.data.neo4j.repository.query.Query;
import org.springframework.data.repository.query.Param;

import com.example.news_server.PO.OrganizationNode;
import java.util.Optional;

public interface OrganizationRepository extends Neo4jRepository<OrganizationNode, String> {

    @Query("MATCH (i:Organization {name: $name}) RETURN i Limit 1")
    OrganizationNode findOneByNameLimited(@Param("name") String name);

    // @Query("MATCH (o:Organization)-[r*0..1]->(related) WHERE o.name = $name RETURN o, COLLECT({relationship: r, related: related}) AS relatedNodes LIMIT 1")
    OrganizationNode findOneByName(@Param("name") String name);
    
    @Query("MERGE (n1:Organization {name: $nodeName1}) " +
    "MERGE (n2:Organization {name: $nodeName2}) " +
    "CREATE (n1)-[r:OrganizationEffectsOrganization]->(n2) SET r.strength = $strength " +
    "WITH n1, n2, $newsId AS newsId " +
    "SET n1.related_news = CASE " +
    "WHEN n1.related_news IS NULL THEN [newsId] " +
    "ELSE n1.related_news + [newsId] " +
    "END, " +
    "n2.related_news = CASE " +
    "WHEN n2.related_news IS NULL THEN [newsId] " +
    "ELSE n2.related_news + [newsId] " +
    "END" + 
    "SET n1.time = CASE " +
    "WHEN n1.time IS NULL THEN [datetime(date)] " +
    "ELSE n1.time + [datetime(date)] " +
    "SET n2.time = CASE " +
    "WHEN n2.time IS NULL THEN [datetime(date)] " +
    "ELSE n2.time + [datetime(date)] " +
    "END")
    void createOrMatchNodesAndOrganizationEffectsOrganization(String nodeName1, String nodeName2, double strength, String newsId, String datetime);

    @Query("MERGE (n1:Organization {name: $nodeName1}) " +
    "MERGE (n2:Organization {name: $nodeName2}) " +
    "CREATE (n1)-[r:OrganizationEffectsOrganization]->(n2) SET r.strength = $strength " +
    "WITH n1, n2, $newsId AS newsId " +
    "SET n1.related_news = CASE " +
    "WHEN n1.related_news IS NULL THEN [newsId] " +
    "ELSE n1.related_news + [newsId] " +
    "END, " +
    "n2.related_news = CASE " +
    "WHEN n2.related_news IS NULL THEN [newsId] " +
    "ELSE n2.related_news + [newsId] " +
    "END")
    void createOrMatchNodesAndOrganizationEffectsOrganization(String nodeName1, String nodeName2, double strength, String newsId);

    @Query("MERGE (n1:Organization {name: $nodeName1}) " +
    "MERGE (n2:Person {name: $nodeName2}) " +
    "CREATE (n1)-[r:OrganizationEffectsPerson]->(n2) SET r.strength = $strength " +
    "WITH n1, n2, $newsId AS newsId " +
    "SET n1.related_news = CASE " +
    "WHEN n1.related_news IS NULL THEN [newsId] " +
    "ELSE n1.related_news + [newsId] " +
    "END, " +
    "n2.related_news = CASE " +
    "WHEN n2.related_news IS NULL THEN [newsId] " +
    "ELSE n2.related_news + [newsId] " +
    "END" + 
    "SET n1.time = CASE " +
    "WHEN n1.time IS NULL THEN [datetime(date)] " +
    "ELSE n1.time + [datetime(date)] " +
    "SET n2.time = CASE " +
    "WHEN n2.time IS NULL THEN [datetime(date)] " +
    "ELSE n2.time + [datetime(date)] " +
    "END")
    void createOrMatchNodesAndOrganizationEffectsPerson(String nodeName1, String nodeName2, double strength, String newsId, String datetime);

    @Query("MERGE (n1:Organization {name: $nodeName1}) " +
    "MERGE (n2:Person {name: $nodeName2}) " +
    "CREATE (n1)-[r:OrganizationEffectsPerson]->(n2) SET r.strength = $strength " +
    "WITH n1, n2, $newsId AS newsId " +
    "SET n1.related_news = CASE " +
    "WHEN n1.related_news IS NULL THEN [newsId] " +
    "ELSE n1.related_news + [newsId] " +
    "END, " +
    "n2.related_news = CASE " +
    "WHEN n2.related_news IS NULL THEN [newsId] " +
    "ELSE n2.related_news + [newsId] " +
    "END")
    void createOrMatchNodesAndOrganizationEffectsPerson(String nodeName1, String nodeName2, double strength, String newsId);

    @Query("MERGE (n1:Organization {name: $nodeName1}) " +
    "MERGE (n2:Idea {name: $nodeName2}) " +
    "CREATE (n1)-[r:OrganizationEffectsIdea]->(n2) SET r.strength = $strength " +
    "WITH n1, n2, $newsId AS newsId " +
    "SET n1.related_news = CASE " +
    "WHEN n1.related_news IS NULL THEN [newsId] " +
    "ELSE n1.related_news + [newsId] " +
    "END, " +
    "n2.related_news = CASE " +
    "WHEN n2.related_news IS NULL THEN [newsId] " +
    "ELSE n2.related_news + [newsId] " +
    "END" + 
    "SET n1.time = CASE " +
    "WHEN n1.time IS NULL THEN [datetime(date)] " +
    "ELSE n1.time + [datetime(date)] " +
    "SET n2.time = CASE " +
    "WHEN n2.time IS NULL THEN [datetime(date)] " +
    "ELSE n2.time + [datetime(date)] " +
    "END")
    void createOrMatchNodesAndOrganizationEffectsIdea(String nodeName1, String nodeName2, double strength, String newsId, String datetime);

    @Query("MERGE (n1:Organization {name: $nodeName1}) " +
    "MERGE (n2:Idea {name: $nodeName2}) " +
    "CREATE (n1)-[r:OrganizationEffectsIdea]->(n2) SET r.strength = $strength " +
    "WITH n1, n2, $newsId AS newsId " +
    "SET n1.related_news = CASE " +
    "WHEN n1.related_news IS NULL THEN [newsId] " +
    "ELSE n1.related_news + [newsId] " +
    "END, " +
    "n2.related_news = CASE " +
    "WHEN n2.related_news IS NULL THEN [newsId] " +
    "ELSE n2.related_news + [newsId] " +
    "END")
    void createOrMatchNodesAndOrganizationEffectsIdea(String nodeName1, String nodeName2, double strength, String newsId);

    @Query("MERGE (n1:Organization {name: $nodeName1}) " +
    "MERGE (n2:Event {name: $nodeName2}) " +
    "CREATE (n1)-[r:OrganizationEffectsEvent]->(n2) SET r.strength = $strength " +
    "WITH n1, n2, $newsId AS newsId " +
    "SET n1.related_news = CASE " +
    "WHEN n1.related_news IS NULL THEN [newsId] " +
    "ELSE n1.related_news + [newsId] " +
    "END, " +
    "n2.related_news = CASE " +
    "WHEN n2.related_news IS NULL THEN [newsId] " +
    "ELSE n2.related_news + [newsId] " +
    "END" + 
    "SET n1.time = CASE " +
    "WHEN n1.time IS NULL THEN [datetime(date)] " +
    "ELSE n1.time + [datetime(date)] " +
    "SET n2.time = CASE " +
    "WHEN n2.time IS NULL THEN [datetime(date)] " +
    "ELSE n2.time + [datetime(date)] " +
    "END")
    void createOrMatchNodesAndOrganizationEffectsEvent(String nodeName1, String nodeName2, double strength, String newsId, String datetime);

    @Query("MERGE (n1:Organization {name: $nodeName1}) " +
    "MERGE (n2:Event {name: $nodeName2}) " +
    "CREATE (n1)-[r:OrganizationEffectsEvent]->(n2) SET r.strength = $strength " +
    "WITH n1, n2, $newsId AS newsId " +
    "SET n1.related_news = CASE " +
    "WHEN n1.related_news IS NULL THEN [newsId] " +
    "ELSE n1.related_news + [newsId] " +
    "END, " +
    "n2.related_news = CASE " +
    "WHEN n2.related_news IS NULL THEN [newsId] " +
    "ELSE n2.related_news + [newsId] " +
    "END")
    void createOrMatchNodesAndOrganizationEffectsEvent(String nodeName1, String nodeName2, double strength, String newsId);
    
}
