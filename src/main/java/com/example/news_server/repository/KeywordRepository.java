package com.example.news_server.repository;

import com.example.news_server.PO.Keyword;
import com.example.news_server.PO.News;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Date;
import java.util.List;

public interface KeywordRepository  extends JpaRepository<Keyword, Long>, JpaSpecificationExecutor<Keyword> {

    List<Keyword> findByTypeAndPublishDateBetween(Integer type, Date startDate, Date endDate);

    @Query(value = "SELECT k.keyword, COUNT(k) AS occurrence FROM Keyword k WHERE k.type = :type GROUP BY k.keyword ORDER BY occurrence DESC LIMIT 50")
    List<Object[]> findKeywordWithMaxOccurrences(@Param("type") Integer type);

    @Query(value = "SELECT k.keyword, COUNT(k) AS occurrence FROM Keyword k WHERE k.type = :type AND k.publishDate < :endDate AND k.publishDate >= :startDate GROUP BY k.keyword ORDER BY occurrence DESC LIMIT 50")
    List<Object[]> findKeywordWithMaxOccurrencesBetweenDates(@Param("type") Integer type, @Param("startDate") Date startDate, @Param("endDate") Date endDate);
}
