package com.example.news_server.repository;

import org.springframework.data.neo4j.repository.Neo4jRepository;
import org.springframework.data.neo4j.repository.query.Query;
import org.springframework.data.repository.query.Param;

import com.example.news_server.PO.PersonNode;
import java.util.Optional;

public interface PersonRepository extends Neo4jRepository<PersonNode, String>{

    @Query("MATCH (i:Person {name: $name}) RETURN i Limit 1")
    PersonNode findOneByNameLimited(@Param("name") String name);

    // @Query("MATCH (p:Person)-[r*0..1]->(related) WHERE p.name = $name RETURN p, COLLECT({relationship: r, related: related}) AS relatedNodes LIMIT 1")
    PersonNode findOneByName(@Param("name") String name);

    @Query("MERGE (n1:Person {name: $nodeName1}) " +
    "MERGE (n2:Person {name: $nodeName2}) " +
    "CREATE (n1)-[r:PersonEffectsPerson]->(n2) SET r.strength = $strength " +
    "WITH n1, n2, $newsId AS newsId " +
    "SET n1.related_news = CASE " +
    "WHEN n1.related_news IS NULL THEN [newsId] " +
    "ELSE n1.related_news + [newsId] " +
    "END, " +
    "n2.related_news = CASE " +
    "WHEN n2.related_news IS NULL THEN [newsId] " +
    "ELSE n2.related_news + [newsId] " +
    "END" + 
    "SET n1.time = CASE " +
    "WHEN n1.time IS NULL THEN [datetime(date)] " +
    "ELSE n1.time + [datetime(date)] " +
    "SET n2.time = CASE " +
    "WHEN n2.time IS NULL THEN [datetime(date)] " +
    "ELSE n2.time + [datetime(date)] " +
    "END")
    void createOrMatchNodesAndPersonEffectsPerson(String nodeName1, String nodeName2, double strength, String newsId, String datetime);

    @Query("MERGE (n1:Person {name: $nodeName1}) " +
    "MERGE (n2:Person {name: $nodeName2}) " +
    "CREATE (n1)-[r:PersonEffectsPerson]->(n2) SET r.strength = $strength " +
    "WITH n1, n2, $newsId AS newsId " +
    "SET n1.related_news = CASE " +
    "WHEN n1.related_news IS NULL THEN [newsId] " +
    "ELSE n1.related_news + [newsId] " +
    "END, " +
    "n2.related_news = CASE " +
    "WHEN n2.related_news IS NULL THEN [newsId] " +
    "ELSE n2.related_news + [newsId] " +
    "END")
    void createOrMatchNodesAndPersonEffectsPerson(String nodeName1, String nodeName2, double strength, String newsId);

    @Query("MERGE (n1:Person {name: $nodeName1}) " +
    "MERGE (n2:Idea {name: $nodeName2}) " +
    "CREATE (n1)-[r:PersonEffectsIdea]->(n2) SET r.strength = $strength " +
    "WITH n1, n2, $newsId AS newsId, " +
    "SET n1.related_news = CASE " +
    "WHEN n1.related_news IS NULL THEN [newsId] " +
    "ELSE n1.related_news + [newsId] " +
    "END, " +
    "n2.related_news = CASE " +
    "WHEN n2.related_news IS NULL THEN [newsId] " +
    "ELSE n2.related_news + [newsId] " +
    "END," + 
    "n1.time = CASE " +
    "WHEN n1.time IS NULL THEN [datetime(date)] " +
    "ELSE n1.time + [datetime(date)] " +
    "END," +
    "n2.time = CASE " +
    "WHEN n2.time IS NULL THEN [datetime(date)] " +
    "ELSE n2.time + [datetime(date)] " +
    "END")
    void createOrMatchNodesAndPersonEffectsIdea(String nodeName1, String nodeName2, double strength, String newsId, String datetime);

    @Query("MERGE (n1:Person {name: $nodeName1}) " +
    "MERGE (n2:Idea {name: $nodeName2}) " +
    "CREATE (n1)-[r:PersonEffectsIdea]->(n2) SET r.strength = $strength " +
    "WITH n1, n2, $newsId AS newsId " +
    "SET n1.related_news = CASE " +
    "WHEN n1.related_news IS NULL THEN [newsId] " +
    "ELSE n1.related_news + [newsId] " +
    "END, " +
    "n2.related_news = CASE " +
    "WHEN n2.related_news IS NULL THEN [newsId] " +
    "ELSE n2.related_news + [newsId] " +
    "END")
    void createOrMatchNodesAndPersonEffectsIdea(String nodeName1, String nodeName2, double strength, String newsId);

    @Query("MERGE (n1:Person {name: $nodeName1}) " +
    "MERGE (n2:Organization {name: $nodeName2}) " +
    "CREATE (n1)-[r:PersonEffectsOrganization]->(n2) SET r.strength = $strength " +
    "WITH n1, n2, $newsId AS newsId " +
    "SET n1.related_news = CASE " +
    "WHEN n1.related_news IS NULL THEN [newsId] " +
    "ELSE n1.related_news + [newsId] " +
    "END, " +
    "n2.related_news = CASE " +
    "WHEN n2.related_news IS NULL THEN [newsId] " +
    "ELSE n2.related_news + [newsId] " +
    "END" + 
    "SET n1.time = CASE " +
    "WHEN n1.time IS NULL THEN [datetime(date)] " +
    "ELSE n1.time + [datetime(date)] " +
    "SET n2.time = CASE " +
    "WHEN n2.time IS NULL THEN [datetime(date)] " +
    "ELSE n2.time + [datetime(date)] " +
    "END")
    void createOrMatchNodesAndPersonEffectsOrganization(String nodeName1, String nodeName2, double strength, String newsId, String datetime);

    @Query("MERGE (n1:Person {name: $nodeName1}) " +
    "MERGE (n2:Organization {name: $nodeName2}) " +
    "CREATE (n1)-[r:PersonEffectsOrganization]->(n2) SET r.strength = $strength " +
    "WITH n1, n2, $newsId AS newsId " +
    "SET n1.related_news = CASE " +
    "WHEN n1.related_news IS NULL THEN [newsId] " +
    "ELSE n1.related_news + [newsId] " +
    "END, " +
    "n2.related_news = CASE " +
    "WHEN n2.related_news IS NULL THEN [newsId] " +
    "ELSE n2.related_news + [newsId] " +
    "END")
    void createOrMatchNodesAndPersonEffectsOrganization(String nodeName1, String nodeName2, double strength, String newsId);

    @Query("MERGE (n1:Person {name: $nodeName1}) " +
    "MERGE (n2:Event {name: $nodeName2}) " +
    "CREATE (n1)-[r:PersonEffectsEvent]->(n2) SET r.strength = $strength " +
    "WITH n1, n2, $newsId AS newsId " +
    "SET n1.related_news = CASE " +
    "WHEN n1.related_news IS NULL THEN [newsId] " +
    "ELSE n1.related_news + [newsId] " +
    "END, " +
    "n2.related_news = CASE " +
    "WHEN n2.related_news IS NULL THEN [newsId] " +
    "ELSE n2.related_news + [newsId] " +
    "END" + 
    "SET n1.time = CASE " +
    "WHEN n1.time IS NULL THEN [datetime(date)] " +
    "ELSE n1.time + [datetime(date)] " +
    "SET n2.time = CASE " +
    "WHEN n2.time IS NULL THEN [datetime(date)] " +
    "ELSE n2.time + [datetime(date)] " +
    "END")
    void createOrMatchNodesAndPersonEffectsEvent(String nodeName1, String nodeName2, double strength, String newsId, String datetime);

    @Query("MERGE (n1:Person {name: $nodeName1}) " +
    "MERGE (n2:Event {name: $nodeName2}) " +
    "CREATE (n1)-[r:PersonEffectsEvent]->(n2) SET r.strength = $strength " +
    "WITH n1, n2, $newsId AS newsId " +
    "SET n1.related_news = CASE " +
    "WHEN n1.related_news IS NULL THEN [newsId] " +
    "ELSE n1.related_news + [newsId] " +
    "END, " +
    "n2.related_news = CASE " +
    "WHEN n2.related_news IS NULL THEN [newsId] " +
    "ELSE n2.related_news + [newsId] " +
    "END")
    void createOrMatchNodesAndPersonEffectsEvent(String nodeName1, String nodeName2, double strength, String newsId);

}
