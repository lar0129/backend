package com.example.news_server.amqp;

import java.time.ZonedDateTime;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import java.lang.reflect.Type;

import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.example.news_server.PO.News;
import com.example.news_server.VO.CrawledNewsVO;
import com.example.news_server.config.RabbitMQConfig;
import com.example.news_server.service.CrawlerService;
import com.example.news_server.service.SseService;
import com.example.news_server.utils.gsonSupport.GsonTimeSupport;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import lombok.AllArgsConstructor;
import lombok.Data;

@Component
public class CrawledNewsListener {
    @AllArgsConstructor
    @Data
    class NewsforNRE {
        @JsonProperty
        private Long id;
        @JsonProperty
        private String content;
        @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSSSS")
        private Date date;
    }

    @Autowired
    private CrawlerService crawlerService;
    @Autowired
    private SseService sseService;
    @Autowired
    private RabbitTemplate rabbitTemplate;
    @RabbitListener(queues = RabbitMQConfig.QUEUE_NAME)
    public void receiveMessage(String message) {
        // Gson gson = new GsonBuilder().registerTypeAdapter(ZonedDateTime.class, new GsonTimeSupport()).create();
        // // 将json字符串转为List<CrawledNewsVO>
        ObjectMapper objectMapper = new ObjectMapper();
        // Type listType = new TypeToken<List<CrawledNewsVO>>() {}.getType();
        // List<CrawledNewsVO> newsList = gson.fromJson(message, listType);
        // 转为list
        // List<CrawledNewsVO> newsList = Arrays.asList(newsArray);
        try {
            List<CrawledNewsVO> newsList = objectMapper.readValue(message, new TypeReference<List<CrawledNewsVO>>() {});
            List<News> savedNews = crawlerService.addNews(newsList);
            List<NewsforNRE> newsforNREList = savedNews.stream().map(news -> new NewsforNRE(news.getId(), news.getContent(), news.getPublishDate())).collect(Collectors.toList());
            // Gson gson = new GsonBuilder().registerTypeAdapter(ZonedDateTime.class, new GsonTimeSupport()).create();
            String newsforNREListJson = objectMapper.writeValueAsString(newsforNREList);
            rabbitTemplate.convertAndSend(RabbitMQConfig.EXCHANGE_NAME, RabbitMQConfig.ROUTING_KEY_FOR_NRE, newsforNREListJson);
            sseService.sendUpdateNews();
            sseService.sendUpdateDailyAmount();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
