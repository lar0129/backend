package com.example.news_server.config;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.core.Queue;
// import org.springframework.amqp.core.TopicExchange;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RabbitMQConfig {

    public static final String QUEUE_NAME = "crawledNewsQueue";
    public static final String TEST_QUEUE = "testQueue";
    public static final String EXCHANGE_NAME = "newsForNreExchange";
    public static final String ROUTING_KEY_FOR_NRE = "newsForNRE";

    @Bean
    public Queue queue() {
        return new Queue(QUEUE_NAME);
    }

    @Bean
    public Queue testQueue() {
        return new Queue(TEST_QUEUE);
    }

    @Bean
    public DirectExchange exchange() {
        return new DirectExchange(EXCHANGE_NAME);
    }

    // @Bean
    // public Binding binding(Queue queue, DirectExchange exchange) {
    //     return BindingBuilder.bind(queue).to(exchange).with("crawledNewsQueue");
    // }
}
