package com.example.news_server.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ApikeyConfig {
    public static String  GLM_ak;

    @Value("${glm.apikey}")
    public  void setGLM_ak(String gLM_ak)
    {
        ApikeyConfig.GLM_ak = gLM_ak;
    }

    public static String GAODE_ak;
    @Value("${gaode.apikey}")
    public  void setGAODE_ak(String gAODE_ak)
    {
        ApikeyConfig.GAODE_ak = gAODE_ak;
    }


}