package com.example.news_server.PO;

import java.util.List;
import java.util.ArrayList;
import org.springframework.data.neo4j.core.schema.GeneratedValue;
import org.springframework.data.neo4j.core.schema.Id;
import org.springframework.data.neo4j.core.schema.Node;
import org.springframework.data.neo4j.core.schema.Property;
import org.springframework.data.neo4j.core.schema.Relationship;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Node("Organization")
@Getter
@Setter
// @ToString
public class OrganizationNode {

    @Id 
    private String id;

    @Property("name")
    private List<String> name;

    @Property("related_news")
    private List<String> related_news = new ArrayList<>();;

    public void add_related_news(Long id){
        related_news.add(Long.toString(id));
    }

    // effects others
    @Relationship(type = "OrganizationEffectsEvent", direction = Relationship.Direction.OUTGOING)
    private List<OrganizationEffectsEvent> effects_event;

    @Relationship(type = "OrganizationEffectsPerson", direction = Relationship.Direction.OUTGOING)
    private List<OrganizationEffectsPerson> effects_person;

    @Relationship(type = "OrganizationEffectsOrganization", direction = Relationship.Direction.OUTGOING)
    private List<OrganizationEffectsOrganization> effects_organization;

    @Relationship(type = "OrganizationEffectsIdea", direction = Relationship.Direction.OUTGOING)
    private List<OrganizationEffectsIdea> effects_idea;

    // be effected by others
    @Relationship(type = "EventEffectsOrganization", direction = Relationship.Direction.INCOMING)
    private List<EventEffectsOrganization> effected_by_event;

    @Relationship(type = "PersonEffectsOrganization", direction = Relationship.Direction.INCOMING)
    private List<PersonEffectsOrganization> effected_by_person;

    @Relationship(type = "OrganizationEffectsOrganization", direction = Relationship.Direction.INCOMING)
    private List<OrganizationEffectsOrganization> effected_by_organization;

    @Relationship(type = "IdeaEffectsOrganization", direction = Relationship.Direction.INCOMING)
    private List<IdeaEffectsOrganization> effected_by_idea;
    
}
