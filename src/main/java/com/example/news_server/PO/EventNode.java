package com.example.news_server.PO;

import org.springframework.data.neo4j.core.schema.Node;
import org.springframework.data.neo4j.core.schema.Property;
import org.springframework.data.neo4j.core.schema.Relationship;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.List;
import java.util.ArrayList;
import org.springframework.data.neo4j.core.schema.GeneratedValue;
import org.springframework.data.neo4j.core.schema.Id;

// 这里预留了几个属性，或许以后可以完善
@Node("Event")
@Getter
@Setter
// @ToString
public class EventNode {
    
    @Id 
    private String id;

    @Property("name")
    private String name;

    @Property("time")
    private List<String> time;

    @Property("location")
    private String location;

    @Property("related_news")
    private List<String> related_news=new ArrayList<>(); // 用于索引这个事件出现过的新闻

    public void add_related_news(Long id){
        related_news.add(Long.toString(id));
    }

    // effects others
    @Relationship(type = "EventEffectsEvent", direction = Relationship.Direction.OUTGOING)
    private List<EventEffectsEvent> effects_event;

    @Relationship(type = "EventEffectsPerson", direction = Relationship.Direction.OUTGOING)
    private List<EventEffectsPerson> effects_person;

    @Relationship(type = "EventEffectsOrganization", direction = Relationship.Direction.OUTGOING)
    private List<EventEffectsOrganization> effects_organization;

    @Relationship(type = "EventEffectsIdea", direction = Relationship.Direction.OUTGOING)
    private List<EventEffectsIdea> effects_idea;

    // be effected by others
    @Relationship(type = "EventEffectsEvent", direction = Relationship.Direction.INCOMING)
    private List<EventEffectsEvent> effected_by_event;

    @Relationship(type = "PersonEffectsEvent", direction = Relationship.Direction.INCOMING)
    private List<PersonEffectsEvent> effected_by_person;

    @Relationship(type = "OrganizationEffectsEvent", direction = Relationship.Direction.INCOMING)
    private List<OrganizationEffectsEvent> effected_by_organization;

    @Relationship(type = "IdeaEffectsEvent", direction = Relationship.Direction.INCOMING)
    private List<IdeaEffectsEvent> effected_by_idea;

}
