package com.example.news_server.PO;

import com.example.news_server.VO.SummaryNews;
import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
public class WithNodeNews {
    @Getter
    private String nodeName;

    @Getter
    private SummaryNews summaryNews;

}
