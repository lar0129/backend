package com.example.news_server.PO;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ConversationDTO {
    private Long id;
    private String message;
    private String response;

    public ConversationDTO(Long id, String message, String response) {
        this.id = id;
        this.message = message;
        this.response = response;
    }
}

