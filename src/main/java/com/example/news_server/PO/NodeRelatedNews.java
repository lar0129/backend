package com.example.news_server.PO;

import java.util.List;

public class NodeRelatedNews {
    private String node;
    private List<Long> related_news;

    // 构造函数、Getter 和 Setter 方法
    public NodeRelatedNews(String node, List<Long> relatedNews) {
        this.node = node;
        this.related_news = relatedNews;
    }

    public String getNode() {
        return node;
    }

    public void setNode(String node) {
        this.node = node;
    }

    public List<Long> getRelatedNews() {
        return related_news;
    }

    public void setRelatedNews(List<Long> relatedNews) {
        this.related_news = relatedNews;
    }
}
