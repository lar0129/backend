package com.example.news_server.PO;

import java.util.List;
import java.util.ArrayList;
import org.springframework.data.neo4j.core.schema.GeneratedValue;
import org.springframework.data.neo4j.core.schema.Id;
import org.springframework.data.neo4j.core.schema.Node;
import org.springframework.data.neo4j.core.schema.Property;
import org.springframework.data.neo4j.core.schema.Relationship;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;




@Node("Idea")
@Getter
@Setter
// @ToString
public class IdeaNode {

    @Id 
    private String id;

    @Property("name")
    private String name;

    @Property("time")
    private List<String> time;

    @Property("related_news")
    private List<String> related_news = new ArrayList<>(); // 用于索引这个事件出现过的新闻

    public void add_related_news(Long id){
        related_news.add(Long.toString(id));
    }

    // effects others
    @Relationship(type = "IdeaEffectsEvent", direction = Relationship.Direction.OUTGOING)
    private List<IdeaEffectsEvent> effects_event;

    @Relationship(type = "IdeaEffectsPerson", direction = Relationship.Direction.OUTGOING)
    private List<IdeaEffectsPerson> effects_person;

    @Relationship(type = "IdeaEffectsOrganization", direction = Relationship.Direction.OUTGOING)
    private List<IdeaEffectsOrganization> effects_organization;

    @Relationship(type = "IdeaEffectsIdea", direction = Relationship.Direction.OUTGOING)
    private List<IdeaEffectsIdea> effects_idea;

    // be effected by others
    @Relationship(type = "EventEffectsIdea", direction = Relationship.Direction.INCOMING)
    private List<EventEffectsIdea> effected_by_event;

    @Relationship(type = "PersonEffectsIdea", direction = Relationship.Direction.INCOMING)
    private List<PersonEffectsIdea> effected_by_person;

    @Relationship(type = "OrganizationEffectsIdea", direction = Relationship.Direction.INCOMING)
    private List<OrganizationEffectsIdea> effected_by_organization;

    @Relationship(type = "IdeaEffectsIdea", direction = Relationship.Direction.INCOMING)
    private List<IdeaEffectsIdea> effected_by_idea;

    
    
}
