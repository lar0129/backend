package com.example.news_server.PO;

import org.springframework.data.neo4j.core.schema.GeneratedValue;
import org.springframework.data.neo4j.core.schema.Property;
import org.springframework.data.neo4j.core.schema.RelationshipId;
import org.springframework.data.neo4j.core.schema.RelationshipProperties;
import org.springframework.data.neo4j.core.schema.TargetNode;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@RelationshipProperties 
@Getter
@Setter
// @ToString
public class IdeaEffectsOrganization {

    @GeneratedValue
    @RelationshipId
    private String id;

    @TargetNode
    private OrganizationNode organization;

    @Property("strength")
    private double strength;
    
}
