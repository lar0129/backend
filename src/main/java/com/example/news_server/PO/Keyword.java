package com.example.news_server.PO;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Entity
@Table(name = "keywords", indexes = {@Index(name = "index_keyword", columnList = "keyword")})
@Getter
@Setter
public class Keyword {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "news_id")
    private Long newsId;

    private String keyword;

    private Integer type;

    @Column(name = "publish_date")
    private Date publishDate;

    public String getWord() {
        return keyword;
    }
}
