package com.example.news_server.PO;

import org.springframework.data.neo4j.core.schema.Node;
import org.springframework.data.neo4j.core.schema.Property;
import org.springframework.data.neo4j.core.schema.Relationship;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.List;
import org.springframework.data.neo4j.core.schema.GeneratedValue;
import org.springframework.data.neo4j.core.schema.Id;

import java.util.ArrayList;

@Node("Person")
@Getter
@Setter
// @ToString
public class PersonNode {
    @Id
    private String id;

    @Property("name")
    private List<String> name;

    @Property("related_news")
    private List<String> related_news = new ArrayList<>(); // 用于索引这个人物出现过的新闻

    public void add_related_news(Long id){
        related_news.add(Long.toString(id));
    }

    // effects others
    @Relationship(type = "PersonEffectsEvent", direction = Relationship.Direction.OUTGOING)
    private List<PersonEffectsEvent> effects_event;

    @Relationship(type = "PersonEffectsPerson", direction = Relationship.Direction.OUTGOING)
    private List<PersonEffectsPerson> effects_person;

    @Relationship(type = "PersonEffectsOrganization", direction = Relationship.Direction.OUTGOING)
    private List<PersonEffectsOrganization> effects_organization;

    @Relationship(type = "PersonEffectsIdea", direction = Relationship.Direction.OUTGOING)
    private List<PersonEffectsIdea> effects_idea;

    // be effected by others
    @Relationship(type = "EventEffectsPerson", direction = Relationship.Direction.INCOMING)
    private List<EventEffectsPerson> effected_by_event;

    @Relationship(type = "PersonEffectsPerson", direction = Relationship.Direction.INCOMING)
    private List<PersonEffectsPerson> effected_by_person;

    @Relationship(type = "OrganizationEffectsPerson", direction = Relationship.Direction.INCOMING)
    private List<OrganizationEffectsPerson> effected_by_organization;

    @Relationship(type = "IdeaEffectsPerson", direction = Relationship.Direction.INCOMING)
    private List<IdeaEffectsPerson> effected_by_idea;
}
