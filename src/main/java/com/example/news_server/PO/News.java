package com.example.news_server.PO;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;
//import org.hibernate.search.mapper.pojo.mapping.definition.annotation.FullTextField;

import java.util.Date;
import java.util.List;

@Entity
@Table(name = "news", indexes = {@Index(name = "index_title", columnList = "title", unique = true)})
@Getter
@Setter
//@Indexed
public class News {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    //注意表级完整性定义，这个列是unique的
//    @FullTextField
    private String title;

    //如果不够长，改成MEDIUMTEXT
    @Column(length = 65535)
//    @FullTextField
    private String content;

    private String source;

    private Date publishDate;

    private int category;

    private String url;

    private String keywords;

    // 三位定点数
    @Column(columnDefinition = "DECIMAL(5,3)")
    private Double credibility;

    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true) // 级联操作只会在父实体上执行，对于子实体来说并不会触发级联操作。
    @JoinColumn(name = "news_id")
    private List<Keyword> keywordList;

}
