package com.example.news_server.PO;

import com.example.news_server.VO.QuestionAndAnswerVO;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter
public class QuestionAndAnswer {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Lob
    @Column(columnDefinition = "TEXT")
    private String question;

    @Lob
    @Column(columnDefinition = "TEXT")
    private String answer;

    @ManyToOne
    @JoinColumn(name = "conversation_id")
    private Conversation conversation;

    public QuestionAndAnswerVO getQuestionAndAnswerVO() {
        return new QuestionAndAnswerVO(this.id, this.question, this.answer);
    }
}
