package com.example.news_server.PO;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter
public class FeedbackPO {

    //某一轮问答的id
    @Id
    Long id;

    Integer attitude;

    String comment;
}
