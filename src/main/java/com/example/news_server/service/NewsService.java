package com.example.news_server.service;

import com.example.news_server.VO.NewsListVO;
import com.example.news_server.VO.NewsPageVO;
import com.example.news_server.VO.PatchNewsVO;
import com.example.news_server.VO.SummaryNews;
import com.example.news_server.utils.enums.Category;

import java.util.Date;
import java.util.List;

public interface NewsService {
    NewsPageVO getNewsPage(int size, int page, String source, Date startDate, Date endDate, String category);
    NewsListVO getTimedNews(Date starDate, Date endDate);
    String getNewsContent(Long id);

    List<SummaryNews> findNewsBySentence(String sentence, Long top);

    boolean deleteNewsById(Long id);
    boolean alterNews(Long id, PatchNewsVO patchNewsVO);

    String classifyNewsById(Long id);

    void classifyLatestNews(int size, int page);

    Double credibilityNewsById(Long id);

    void credibilityLatestNews(int size, int page);
    Boolean extractLatestNewsKeyWords(int size, int page);

    List<SummaryNews> findNewsByCategory(String category);


}
