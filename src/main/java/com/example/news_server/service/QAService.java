package com.example.news_server.service;

import org.springframework.scheduling.annotation.Async;
import org.springframework.web.servlet.mvc.method.annotation.SseEmitter;

public interface QAService {

    public SseEmitter openQASSE(String uuid);

    @Async
    public void processQuestion(String question, String uuid, String username);
    @Async
    public void generateAndExecuteSQL(String question, String uuid, String username);

    public boolean addFeedback(String cvId, Integer attitude, String comment);
}
