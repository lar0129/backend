package com.example.news_server.service;

import com.example.news_server.PO.Conversation;
import com.example.news_server.PO.QuestionAndAnswer;

import com.example.news_server.VO.QuestionAndAnswerVO;


import java.util.List;
import java.util.Optional;

public interface ConversationService {
    QuestionAndAnswer addQuestionAndAnswer(String username, String uuid, String question, String answer);

    Optional<Conversation> findById(String uuid);

    List<QuestionAndAnswerVO> getQuestionAndAnswerVOs(String uuid);

    Conversation save(Conversation cv);

}
