package com.example.news_server.service.impls;

import com.example.news_server.VO.data.*;
import com.example.news_server.repository.KeywordRepository;
import com.example.news_server.repository.MyNeo4jDriver;
import com.example.news_server.repository.NewsRepository;
import com.example.news_server.service.DataService;
import com.example.news_server.utils.enums.Category;
import com.example.news_server.utils.enums.KeywordType;
import com.example.news_server.utils.gsonSupport.GsonTimeSupport;
import com.example.news_server.utils.nlp.DataAnalysisUtils;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.neo4j.driver.Driver;
import org.neo4j.driver.EagerResult;
import org.neo4j.driver.Record;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Service
public class DataServiceImpl implements DataService {
    private final NewsRepository newsRepository;

    private final KeywordRepository keywordRepository;

    private final MyNeo4jDriver myNeo4jDriver;
    private Gson gson = new GsonBuilder().registerTypeAdapter(ZonedDateTime.class, new GsonTimeSupport()).create();

    @Autowired
    public DataServiceImpl(NewsRepository newsRepository, KeywordRepository keywordRepository, MyNeo4jDriver myNeo4jDriver) {
        this.newsRepository = newsRepository;
        this.keywordRepository = keywordRepository;
        this.myNeo4jDriver = myNeo4jDriver;
    }

    // 数据分析方法
    // 例如：
    // 获取3月26日的新闻数量：getNewsAmountData("2024-03-26", "2024-03-27", "")
    // 获取3月26日的体育新闻数量：getNewsAmountData("2024-03-26", "2024-03-27", "体育")
    // 获取一周的新闻数量：getNewsAmountData("2024-03-20", "2024-03-27", "")
    @Override
    public Integer getNewsAmountData(Date startDate, Date endDate, String category) {
        if(category == null || category.equals("all") || category.equals("")){
            return newsRepository.countByPublishDateBetween(startDate, endDate);
        }

        int categoryId = Category.parseString(category).getId();
        if (categoryId == -1 || categoryId == 0) {
            return 0;
        }
        return newsRepository.countByPublishDateBetweenAndCategory(startDate, endDate, categoryId);
    }

    @Override
    public List<SummaryNewsAmountVO> getAllNewsAmountData(Date startDate, Date endDate) {
        List<SummaryNewsAmountVO> summaryNewsAmount = new ArrayList<>();

        for(int i=0;i<=14;i++) {
            List<NewsAmountVO> amountSeries = new ArrayList<>();
            String category = (i==0) ? "全部" :Category.parseInteger(i).getDescription();
            Long total = 0L;
            for (Date date = startDate; date.before(endDate); date = new Date(date.getTime() + 24 * 60 * 60 * 1000)) {
                Date nextDate = new Date(date.getTime() + 24 * 60 * 60 * 1000);
                Integer amount = getNewsAmountData(date, nextDate, Category.parseInteger(i).getDescription());
                total += amount;
                amountSeries.add(new NewsAmountVO(amount, date));
            }
            summaryNewsAmount.add(new SummaryNewsAmountVO(amountSeries,category,total));
        }

        return summaryNewsAmount;
    }

    // 如果要修改为查询呢neo4j：
    // 1. 获取时间范围内的新闻ID List（可以查sql获取，或者图数据库新增Date项）
    // 2. 根据新闻ID List获取neo4j中的节点
    // 3. 统计节点中符合type的关键词，计算其Occurrences次数，并递减返回给service层
    @Override
    public KeywordAnalysisVO getHotWordsData(Date startDate, Date endDate,Integer type) {
        List<SummaryKeywordVO> keywords = new ArrayList<>();
        Long total = 0L;
        List<Object[]> keywordCount = keywordRepository.findKeywordWithMaxOccurrencesBetweenDates(type, startDate, endDate);
        for (Object[] obj : keywordCount) {
            SummaryKeywordVO keyword = new SummaryKeywordVO(String.valueOf(obj[0]), Long.parseLong(String.valueOf(obj[1])));
            keywords.add(keyword);
            total += keyword.getCount();
        }
        KeywordAnalysisVO keywordAnalysisVO = new KeywordAnalysisVO(keywords, total,KeywordType.parseInteger(type).getType(),endDate);
        return keywordAnalysisVO;
    }

    @Override
    public KeywordAnalysisVO getHotEventsByNeo4j(Date startDate, Date endDate){
        return getHotNodeByNeo4j(startDate, endDate,"Event");
    }

    @Override
    public KeywordAnalysisVO getHotPersonByNeo4j(Date startDate, Date endDate){
        return getHotNodeByNeo4j(startDate, endDate,"Person");
    }

    @Override
    public List<SummaryPlacesVO> getPlacesData(Date startDate, Date endDate) {
        List<SummaryPlacesVO> places = new ArrayList<>();
        Long total = 0L;
        List<Object[]> keywordCount = keywordRepository.findKeywordWithMaxOccurrencesBetweenDates(3, startDate, endDate);
        for (Object[] obj : keywordCount) {
            SummaryKeywordVO keyword = new SummaryKeywordVO(String.valueOf(obj[0]), Long.parseLong(String.valueOf(obj[1])));
            Double longitude = DataAnalysisUtils.getInstance().getLongitude(String.valueOf(obj[0]));
            Double latitude = DataAnalysisUtils.getInstance().getLatitude(String.valueOf(obj[0]));
            if (longitude.equals(0.0) || latitude.equals(0.0)) {
                continue;
            }
            SummaryPlacesVO place = new SummaryPlacesVO(String.valueOf(obj[0]), Long.parseLong(String.valueOf(obj[1])),longitude,latitude);
            places.add(place);
            total += keyword.getCount();
        }
        return places;
    }

    public KeywordAnalysisVO getHotNodeByNeo4j(Date startDate, Date endDate,String nodeType){
        List<SummaryKeywordVO> keywords = new ArrayList<>();
        Long total = 0L;
        List<Object[]> keywordCount = getHotDataByGraph(startDate, endDate, nodeType);;
        if(keywordCount.isEmpty() || keywordCount == null){
            System.out.println("Neo4j No data available");
            return null;
        }
        for (Object[] obj : keywordCount) {
            SummaryKeywordVO keyword = new SummaryKeywordVO(String.valueOf(obj[0]), Long.parseLong(String.valueOf(obj[1])));
            keywords.add(keyword);
            total += keyword.getCount();
        }
        KeywordAnalysisVO keywordAnalysisVO = new KeywordAnalysisVO(keywords, total,nodeType,endDate);
        return keywordAnalysisVO;
    }


    public List<Object[]> getHotDataByGraph(Date startDate, Date endDate, String nodeType) {
        Driver driver = myNeo4jDriver.getDriver();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
        String startDateString = dateFormat.format(startDate);
        String endDateString = dateFormat.format(endDate);
        String cypherQuery =
                "MATCH (p:" + nodeType +") " +
                        "WHERE p.related_news IS NOT NULL " +
                        "  AND ANY(time IN p.time WHERE datetime(time) >= datetime($startDate) AND datetime(time) <= datetime($endDate)) " +
                        "WITH p, size(apoc.coll.toSet(p.related_news)) AS uniqueNewsCount " +
                        "RETURN p.name AS name, uniqueNewsCount " +
                        "ORDER BY uniqueNewsCount DESC " +
                        "LIMIT 20";

        List<Object[]> keywordCount = new ArrayList<>();
        try {
            // 执行查询
            EagerResult cypherResult = driver.executableQuery(cypherQuery).withParameters(Map.of("startDate", startDateString, "endDate", endDateString)).execute();
            List<Record> records = cypherResult.records();
            for (Record record : records) {
                Object[] obj = new Object[2];
                obj[0] = record.get("name").asString();
                obj[1] = record.get("uniqueNewsCount").asLong();
                keywordCount.add(obj);
            }

            return keywordCount;
        } catch (Exception e) {
            System.out.println("getGraphByNewsId Error: " + e.getMessage());
        }
        return null;
    }
}
