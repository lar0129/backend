package com.example.news_server.service.impls;

import com.example.news_server.PO.Conversation;
import com.example.news_server.PO.QuestionAndAnswer;
import com.example.news_server.PO.User;
import com.example.news_server.VO.QuestionAndAnswerVO;
import com.example.news_server.repository.ConversationRepository;
import com.example.news_server.repository.QuestionAndAnswerRepository;
import com.example.news_server.repository.UserRepository;
import com.example.news_server.service.ConversationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class ConversationServiceImpl implements ConversationService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private ConversationRepository conversationRepository;

    @Autowired
    private QuestionAndAnswerRepository questionAndAnswerRepository;

    public QuestionAndAnswer addQuestionAndAnswer(String username, String uuid, String question, String answer) {
        User user = userRepository.findByUsername(username);
        if (user == null) {
            throw new RuntimeException("User not found");
        }

        Optional<Conversation> conversationOptional = conversationRepository.findById(uuid);
        Conversation conversation;
        if (conversationOptional.isPresent()) {
            conversation = conversationOptional.get();
        } else {
            conversation = new Conversation();
            conversation.setId(uuid);
            conversation.setUser(user);
            conversation = conversationRepository.save(conversation);
        }

        QuestionAndAnswer qa = new QuestionAndAnswer();
        qa.setQuestion(question);
        qa.setAnswer(answer);
        qa.setConversation(conversation);

        return questionAndAnswerRepository.save(qa);
    }


    @Override
    public Optional<Conversation> findById(String uuid) {
        return conversationRepository.findById(uuid);
    }


    @Override
    public List<QuestionAndAnswerVO> getQuestionAndAnswerVOs(String uuid) {
        return conversationRepository.findById(uuid)
                .map(conversation -> conversation.getQuestionAndAnswers().stream()
                        .map(QuestionAndAnswer::getQuestionAndAnswerVO)
                        .collect(Collectors.toList()))
                .orElse(Collections.emptyList());
    }

    @Override
    public Conversation save(Conversation cv) {
        return conversationRepository.save(cv);
    }
}
