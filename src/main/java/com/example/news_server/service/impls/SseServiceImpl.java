package com.example.news_server.service.impls;

import com.example.news_server.PO.News;
import com.example.news_server.VO.NewsPageVO;
import com.example.news_server.VO.data.SummaryNewsAmountVO;
import com.example.news_server.service.DataService;
import com.example.news_server.service.NewsService;
import com.example.news_server.service.SseService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.servlet.mvc.method.annotation.SseEmitter;

import java.io.IOException;
import java.time.LocalDate;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Slf4j
@Service
public class SseServiceImpl implements SseService {
    /**
     * messageId的 SseEmitter对象映射集
     */
    private static Map<String, SseEmitter> sseEmitterMapNews = new ConcurrentHashMap<>();
    private static Map<String, SseEmitter> sseEmitterMapDailyAmount = new ConcurrentHashMap<>();
    private final DataService dataService;
    private final NewsService newsService;

    @Autowired
    public SseServiceImpl(DataService dataService, NewsService newsService) {
        this.dataService = dataService;
        this.newsService = newsService;
    }
    /**
     * 连接sse
     * @param uuid
     * @return
     */
    @Override
    public SseEmitter connectNewsSSE(String uuid) {
        return connect(uuid, sseEmitterMapNews);
    }

    @Override
    public SseEmitter connectDailyAmountSSE(String uuid) {
        return connect(uuid, sseEmitterMapDailyAmount);
    }

    private SseEmitter connect(String uuid, Map<String, SseEmitter> sseEmitterMap) {
        // 超时时间设置为10分钟
        SseEmitter sseEmitter = new SseEmitter(600000L);
        // 连接成功需要返回数据，否则会出现待处理状态
        try {
            sseEmitter.send(SseEmitter.event().comment("welcome"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        // 连接断开
        sseEmitter.onCompletion(() -> {
            sseEmitterMap.remove(uuid);
        });
        // 连接超时
        sseEmitter.onTimeout(() -> {
            sseEmitterMap.remove(uuid);
        });
        // 连接报错
        sseEmitter.onError((throwable) -> {
            sseEmitterMap.remove(uuid);
        });
        sseEmitterMap.put(uuid, sseEmitter);
        return sseEmitter;
    }

    /**
     * 发送消息
     * @param message
     */
    @Override
    public void sendMessage(String message) {
//        message.setTotal(sseEmitterMap.size());

        sseEmitterMapNews.forEach((uuid, sseEmitter) -> {
            try {
                sseEmitter.send(message, MediaType.APPLICATION_JSON);
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }

    @Override
    public void sendUpdateNews() {
        NewsPageVO pageVO = newsService.getNewsPage(20, 0, null, null, null, null);
        sseEmitterMapNews.forEach((uuid, sseEmitter) -> {
            try {
                sseEmitter.send(pageVO, MediaType.APPLICATION_JSON);
            } catch (IOException e) {
                e.printStackTrace();
            }
        });

    }

    @Override
    public void sendUpdateDailyAmount() {
        LocalDate currentDate = LocalDate.now();
        Date today = java.sql.Date.valueOf(currentDate);
        Date tomorrow = new Date(today.getTime() + 24 * 60 * 60 * 1000);
        List<SummaryNewsAmountVO> amountSeries = dataService.getAllNewsAmountData(today,tomorrow);
        sseEmitterMapDailyAmount.forEach((uuid, sseEmitter) -> {
            try {
                sseEmitter.send(amountSeries, MediaType.APPLICATION_JSON);
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }
}
