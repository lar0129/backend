package com.example.news_server.service.impls;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.nio.charset.StandardCharsets;
import java.sql.*;
import java.time.ZonedDateTime;

import cn.dev33.satoken.stp.StpUtil;
import com.example.news_server.PO.*;
import com.example.news_server.VO.QuestionAndAnswerVO;
import com.example.news_server.VO.SummaryNews;
import com.example.news_server.repository.*;
import com.example.news_server.utils.LLM.CTJ.Chat;
import com.example.news_server.utils.LLM.LLMUtils;
import com.example.news_server.utils.LLM.SQLExtractor;
import com.example.news_server.utils.credibility.CredibilityLLMAnalysis;
import com.example.news_server.utils.credibility.llm_GLMClient;
import com.example.news_server.utils.enums.Category;
import com.example.news_server.utils.gsonSupport.GsonTimeSupport;

import com.google.gson.JsonArray;
import com.google.gson.reflect.TypeToken;
import com.hankcs.hanlp.HanLP;
import com.hankcs.hanlp.seg.common.Term;
import com.zhipu.oapi.Constants;
import org.neo4j.driver.EagerResult;
import org.neo4j.driver.Record;
import org.neo4j.driver.Value;
import org.neo4j.driver.types.Node;
import org.neo4j.driver.types.Relationship;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.servlet.mvc.method.annotation.SseEmitter;

import com.example.news_server.service.GraphService;
import com.example.news_server.service.QAService;
import com.example.news_server.utils.LLM.PreExecute;
import com.example.news_server.utils.sse.SseServiceTemplate;
import com.example.news_server.utils.sse.SseSession;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import lombok.extern.slf4j.Slf4j;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;


@Slf4j
@Service
public class QAServiceImpl implements SseServiceTemplate, QAService {

    @Autowired
    GraphService graphService;

    @Autowired
    private NewsRepository newsRepository;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private ConversationRepository conversationRepository;
    @Autowired
    private QuestionAndAnswerRepository questionAndAnswerRepository;

    @Autowired
    private FeedbackRepository feedbackRepository;

    private Set<String> stopWords = new HashSet<>();


    @org.springframework.beans.factory.annotation.Value("${spring.datasource.url}")
    private String dbUrl;

    @org.springframework.beans.factory.annotation.Value("${spring.datasource.username}")
    private String dbUsername;

    @org.springframework.beans.factory.annotation.Value("${spring.datasource.password}")
    private String dbPassword;

    // 敏感词过滤
    public static final String[] SENSITIVE_WORDS = {"香港", "台湾", "习近平"};

    @Override
    public SseEmitter conect(String userId) {
        if(SseSession.exists(userId)){
            SseSession.remove(userId);
        }
        SseEmitter sseEmitter = new SseEmitter(1800000L);
        sseEmitter.onError((err)-> {
            log.error("type: SseSession Error, msg: {} session Id : {}",err.getMessage(), userId);
            SseSession.onError(userId, err);
        });

        sseEmitter.onTimeout(() -> {
            log.info("type: SseSession Timeout, session Id : {}", userId);
            SseSession.remove(userId);
        });

        sseEmitter.onCompletion(() -> {
            log.info("type: SseSession Completion, session Id : {}", userId);
            SseSession.remove(userId);
        });
        SseSession.add(userId, sseEmitter);
        return sseEmitter;
    }

    @Override
    public boolean send(String uuid, Object content, String eventName) {
        if(SseSession.exists(uuid)){
            try{
                SseSession.send(uuid, content, eventName);
                return true;
            }catch(IOException exception){
                log.error("type: SseSession send Erorr:IOException, msg: {} session Id : {}",exception.getMessage(), uuid);
            }
        }else{
            throw new IllegalStateException("uuid" + uuid + " not Found");
        }
        return false;
    }

    @Override
    public boolean close(String userId) {
        log.info("type: SseSession Close, session Id : {}", userId);
        return SseSession.remove(userId);
    }

//    public List<List<String>> transferInput(String input){
//        List<List<String>> result = new ArrayList<>();
//        // 大模型的返回结果是 "[["我", 0.8], ["你", 0.7]]"这样的字符串
//        String output = PreExecute.execute(input);
//        if(output.startsWith("输出：")){
//            output = output.substring(3);
//        }
//        // 将字符串转换为二维数组
//        String[] temp = output.split("],");
//        for (String s : temp) {
//            s = s.replace("[", "").replace("]", "").replace("\"", "");
//            System.err.println("after rpl: "+ s);
//            String[] temp2 = s.split(",");
//            // 去除空格和标点符号
//            temp2[0] = temp2[0].replaceAll("[\\p{Punct}\\s]", "");
//            List<String> temp3 = new ArrayList<>();
//            temp3.add(temp2[0]);
//            temp3.add(temp2[1]);
//            result.add(temp3);
//        }
//        return result;
//    }

    public static List<List<String>> transferInput(String input) {
        String llmout = PreExecute.execute(input);
        if(llmout.startsWith("输出：")){
            llmout = llmout.substring(3);
        }
        List<List<String>> result = new ArrayList<>();
        // 使用正则表达式提取第一个 [ 到最后一个 ] 之间的内容
        Pattern pattern = Pattern.compile("\\[(.*)]");
        Matcher matcher = pattern.matcher(llmout);
        System.out.println(llmout);
        if (matcher.find()) {
            String output = matcher.group(1);
            // 将字符串转换为二维数组
            String[] temp = output.split("],");
            for (String s : temp) {
                s = s.replace("[", "").replace("]", "").replace("\"", "");
                System.out.println("after rpl: " + s);
                String[] temp2 = s.split(",");
                if (temp2.length < 2)continue;
                // 去除空格和标点符号
                temp2[0] = temp2[0].replaceAll("[\\p{Punct}\\s]", "");
                List<String> temp3 = new ArrayList<>();
                temp3.add(temp2[0]);
                temp3.add(temp2[1]);
                result.add(temp3);
            }
        }
//        System.err.println(result);
        return result;
    }

    @Override
    public SseEmitter openQASSE(String uuid){
        return conect(uuid);
    }

    @Override
    @Async
    public void processQuestion(String question, String uuid, String username) {
//        ArrayList<ArrayList<String>> result = transferInput(question);
        System.out.println("CurrentUser: " + username);

        // 获取当前用户对象
        User user = userRepository.findByUsername(username);
        if (user == null) {
            throw new RuntimeException("User not found");
        }

        // 获取或创建Conversation对象
        Optional<Conversation> conversationOptional = conversationRepository.findById(uuid);
        Conversation conversation;
        if (conversationOptional.isPresent()) {
            conversation = conversationOptional.get();
        } else {
            conversation = new Conversation();
            conversation.setId(uuid);
            conversation.setUser(user);
            conversation = conversationRepository.save(conversation);
        }
        System.setOut(new PrintStream(System.out, true, StandardCharsets.UTF_8));
        List<List<String>> nodeInQuestion = transferInput(question);
        List<Value> relatedGraph = graphService.getGraphFromQAExtracted(nodeInQuestion);
        // 对节点进行排序，按照权重从大到小, 保留权重最高的前5个节点
        nodeInQuestion = sortNodeInQuestion(nodeInQuestion);

        Gson gson = new GsonBuilder().registerTypeAdapter(ZonedDateTime.class, new GsonTimeSupport()).create();
        send(uuid, gson.toJson(relatedGraph), "graph");
        JsonArray jsonArray = gson.fromJson(relatedGraph.get(2).toString(), JsonArray.class);
        System.out.println(jsonArray);
        List<NodeRelatedNews> relatedNewsList=gson.fromJson(jsonArray, new TypeToken<List<NodeRelatedNews>>() {}.getType());
        //这是一会要用到的东西，可能后续还要用这些存下来的新闻做进一步处理
        int limitRelatedNews = 5;
        List<WithNodeNews> summaryNewsList = new ArrayList<>();
        for (NodeRelatedNews relatedNews: relatedNewsList){
            // 过滤掉不在问题中的节点
            String nodeName = relatedNews.getNode();
            boolean find = false;
            for (List<String> nameWeight: nodeInQuestion){
                String nameReal = nameWeight.get(0);
                if (nameReal.equals(nodeName)){
                    find = true;
                    System.out.println(nameReal);
                    break;
                }
            }
            if (!find)continue;
            // 限制每个节点的相关新闻数量
            if (relatedNews.getRelatedNews().size() > limitRelatedNews){
                // 取id最大的前5个新闻, 也就是最新的5个新闻
                List<Long> temp = relatedNews.getRelatedNews();
                temp.sort(Comparator.reverseOrder());
                relatedNews.setRelatedNews(temp.subList(0, limitRelatedNews));
            }
            // 为每个节点找到相关新闻
            for (Long id: relatedNews.getRelatedNews()) {
                Optional<News> newsOptional = newsRepository.findById(id);
                if (newsOptional.isPresent()) {
                    News news = newsOptional.get();
                    System.out.println(id);
                    // 过滤掉敏感词
                    boolean sensitive = false;
                    for(String word: SENSITIVE_WORDS){
                        if(news.getTitle().contains(word)){
                            sensitive = true;
                        }
                    }
                    if (sensitive) continue;
                    // 分析可信度。大致需要1-2s。如果做成sse，可以将新闻可信度和SummaryNews分开发送，做出视觉加载效果
                    if(news.getCredibility() == null || news.getCredibility() < 0.0){
                        Double credibility = CredibilityLLMAnalysis.getNewsCredibilityByNews(news);
//                        Double credibility = -1.0;
                        news.setCredibility(credibility);
                        newsRepository.save(news);
                    }
                    SummaryNews smNews = new SummaryNews(
                            news.getId(),
                            news.getTitle(),
                            news.getPublishDate(),
                            news.getSource(),
                            news.getUrl(),
                            Category.parseInteger(news.getCategory()).getDescription(),
                            news.getKeywords(),
                            news.getCredibility()
                    );
                    WithNodeNews curNews = new WithNodeNews(nodeName, smNews);
                    summaryNewsList.add(curNews);
                    send(uuid, gson.toJson(curNews), "related-news");
                }
            }
        }
        List<QuestionAndAnswer> historyQuestionAndAnswer = conversation.getQuestionAndAnswers();
        List<QuestionAndAnswerVO> historyQuestionAndAnswerVO = new ArrayList<>();
        if(historyQuestionAndAnswer!=null) {
            historyQuestionAndAnswerVO = historyQuestionAndAnswer.stream()
                    .map(QuestionAndAnswer::getQuestionAndAnswerVO)
                    .toList();
        }
        String summaryNewsResult;
        if (summaryNewsList.isEmpty()) {
            String resultString = llm_GLMClient.askClient(question, Constants.ModelChatGLM4);
            send(uuid, 114514, "no_nodes");
            if (resultString == null) {
                summaryNewsResult = "诶呀！抽风了，非常抱歉，请再试一次吧";
            } else {
                summaryNewsResult = "没能找到相关的新闻信息，但是我们仍然将尝试为您回答：\n" + resultString;
            }
        } else {
            summaryNewsResult = processNodeAndRelationAndNews(question, nodeInQuestion, relatedGraph, summaryNewsList, historyQuestionAndAnswerVO);
        }
        // 保存QuestionAndAnswer对象
        QuestionAndAnswer qa = new QuestionAndAnswer();
        qa.setQuestion(question);
        qa.setAnswer(summaryNewsResult);
        qa.setConversation(conversation);
        questionAndAnswerRepository.save(qa);
        send(uuid, summaryNewsResult, "analysis-result");
        List<String> nextQuestionList = predictNextQuestion(nodeInQuestion, question);
        send(uuid, gson.toJson(nextQuestionList), "next-question");
    }

    // 拼出一个prompt发给大模型，总结所有新闻节点的关系和新闻具体内容，输出成用户易于理解的文字，返回给前端
    private String processNodeAndRelationAndNews(String Question, List<List<String>> nodeInQuestion,
                                                 List<Value> relatedGraph,List<WithNodeNews> summaryNewsList,
                                                 List<QuestionAndAnswerVO> historyQuestionAndAnswer){
        StringBuilder prompt = new StringBuilder();
        StringBuilder result = new StringBuilder();

        Gson gson = new GsonBuilder().registerTypeAdapter(ZonedDateTime.class, new GsonTimeSupport()).create();

        prompt.append("回答[用户问题]：总结所有新闻节点实体的内容、节点实体间关系、新闻标题、新闻时间、新闻部分内容、新闻可信度。\n");
        prompt.append("根据以下所有信息，输出成用户易于理解的文字，1000-2000字左右。如果本轮问题和[用户的上下文询问]有关，请一并考虑上下文到结果中。\n");
        prompt.append("请只输出：" +
                "1.最后的总结，markdown格式。并将涉及到的[新闻]、[实体]按脚注形式[1][2...]标注在总结正文中)" +
                "2.根据新闻信息，预测问题相关的未来趋势" +
                "3.最后列出引用新闻列表（包括新闻来源、新闻标题、新闻发布时间、新闻可信度(0.000-1.000之间)、新闻可信或不可信的原因)\n\n");

        prompt.append("[用户问题]：").append(Question).append("\n\n");
        // 限制输出的新闻数量
        int limitRelatedNews = 5;
        int limitAllRelatedNews = 25;
        int limitRelations = 5;
        int limitAllRelations = 25;
        // 计数
        int countAllRelatedNews = 0;
        int countAllRelations = 0;

        // 根据用户问题中的实体，筛选出relatedGraph中对应的实体和关系
        Value nodes = relatedGraph.get(0);
        Value relations = relatedGraph.get(1);
        Map<String, String> filteredNodes = new HashMap<>();
        Map<String, String> allNodes = new HashMap<>();
        for(List<String> nameWeight: nodeInQuestion){
            String name = nameWeight.get(0);
            for(int i=0;i<nodes.size();i++){
                Node node = nodes.get(i).asNode();
                Map<String, Object> properties = node.asMap();
                if(properties.get("name").equals(name)){
                    filteredNodes.put(String.valueOf(node.id()),name);
                }
                allNodes.put(String.valueOf(node.id()),String.valueOf(properties.get("name")));
            }
        }
        // 找出筛选出的实体的关系和新闻
        for(String nodeId : filteredNodes.keySet()){
            String nodeName = filteredNodes.get(nodeId);
            prompt.append("实体id: ").append(nodeId).append(" 实体名称: ").append(nodeName).append("\n");
            // 找出实体相关新闻
            prompt.append(nodeName).append("的相关新闻：\n");
            int count = 0;
            for (WithNodeNews withNodeNews: summaryNewsList){
                if (withNodeNews.getNodeName().equals(nodeName)){
                    SummaryNews news = withNodeNews.getSummaryNews();
                    // 输出新闻prompt
                    prompt.append("[新闻标题]").append(news.getTitle()).append(" ");
                    prompt.append("[新闻发布时间]").append(news.getDate()).append(" ");
                    prompt.append("[新闻来源]").append(news.getSource()).append(" ");
                    if(! (news.getCredibility() < 0.0) && news.getCredibility() != null) {
                        prompt.append("[新闻可信度]").append(news.getCredibility());
                    }
                    prompt.append("\n");
                    count++;
                    countAllRelatedNews++;
                }
                if (count > limitRelatedNews)break;
                if (countAllRelatedNews > limitAllRelatedNews)break;
            }
            // 找出relatedGraph中对应的关系
            prompt.append("该实体与其他实体的关系如下：\n");
            int countRelations = 0;
            for(int i=0;i<relations.size();i++){
                Relationship relationship = relations.get(i).asRelationship();
                String startNodeId = String.valueOf(relationship.startNodeId());
                String endNodeId = String.valueOf(relationship.endNodeId());
                if(startNodeId.equals(nodeId) || endNodeId.equals(nodeId)){
                    prompt.append("关系起始点: ").append(startNodeId).append(allNodes.get(startNodeId)).append(" ");
                    prompt.append("关系终点: ").append(endNodeId).append(allNodes.get(endNodeId)).append(" ");
                    prompt.append("关系名称: ").append(relationship.type()).append("\n");
                    countRelations++;
                    countAllRelations++;
                }
                if(countRelations > limitRelations)break;
                if(countAllRelations > limitAllRelations)break;
            }
            prompt.append("\n");
        }

        System.out.println("Prompt: "+prompt);
        // 上下文问题和回答
        if(historyQuestionAndAnswer!= null && !historyQuestionAndAnswer.isEmpty()){
            prompt.append("[用户的上下文询问]:\n");
            int limitHistoryQA = 3;
            for(int i=historyQuestionAndAnswer.size()-1; i>=0 &&  i>historyQuestionAndAnswer.size()-limitHistoryQA-1; i--){
                prompt.append("{");
                String question = historyQuestionAndAnswer.get(i).getQuestion();
                String answer = historyQuestionAndAnswer.get(i).getAnswer();
                prompt.append("id: ").append(historyQuestionAndAnswer.get(i).getId()).append("\n");
                prompt.append("question：").append(question.length()>1000 ? LLMUtils.ExtractSummary(question,1000):question).append("\n");
                prompt.append("answer：").append(answer.length()>1000 ? LLMUtils.ExtractSummary(answer,1000):answer).append("\n");
                prompt.append("}\n");
            }
        }

        prompt.append("所有实体的所有信息如上，输出1000-2000字左右的总结、未来预测，其中未来预测尽可能预测实体之间可能会发生的事件，可能会支持的观点等。最后列出引用新闻列表（包括新闻来源、新闻标题、新闻发布时间、新闻可信度(0.000-1.000之间)、新闻可信或不可信的原因)\n");
        boolean addFeedback = Math.random() < 0.2;
        if (addFeedback){
            prompt.append(getFeedbackSummarize(3));
        }
        // 大模型处理
        result.append("您的问题是：").append(Question).append("\n\n");
        result.append("您的问题中，涉及到的实体有：");
        for (List<String> nameWeight: nodeInQuestion){
            result.append('[').append(nameWeight.get(0)).append("]");
        }

        result.append("\n\n我们查询了知识图谱中的节点，相关节点有：");
        for(String nodeId : filteredNodes.keySet()){
            result.append('[').append(nodeId).append("]").append(filteredNodes.get(nodeId));
        }
        result.append("……");
        result.append("\n\n根据您的问题，PRISM智能情报分析助手为您查询知识图谱，总结了以下内容~\n\n");
        String resultString = llm_GLMClient.askClient(prompt.toString(), Constants.ModelChatGLM4);
        if(resultString == null) {
            result.append("大模型查询知识图谱错误\n\n");
            result.append( "您的问题涉及到的新闻可能存在敏感信息，PRISM智能情报分析助手为您自动更换了百度大模型再次进行分析~\n\n");
            Chat chat = new Chat();
            resultString = chat.getAnswer(prompt.toString(), "");
        }
        if(resultString == null){
            result.append("您的问题实在是太敏感或者太复杂了，PRISM智能情报分析助手无法为您提供答案，请尝试更换问题。\n\n");
        }
        else {
            result.append(resultString);
        }
        System.out.println("Result: "+result+"……\n");
        return result.toString();
    }

    // 对节点进行排序，按照权重从大到小
    private List<List<String>> sortNodeInQuestion(List<List<String>> nodeInQuestion){
        nodeInQuestion.sort((o1, o2) -> {
            double w1 = Double.parseDouble(o1.get(1));
            double w2 = Double.parseDouble(o2.get(1));
            return Double.compare(w2, w1);
        });
        // 保留权重最高的前5个节点
        if (nodeInQuestion.size() > 5){
            nodeInQuestion = nodeInQuestion.subList(0, 5);
        }
        return nodeInQuestion;
    }

    // 预测用户下一个问题
    List<String> predictNextQuestion(List<List<String>> nodeInQuestion,String question){
        List<String> result = new ArrayList<>();
        int limit = 3;
        int count = 0;
        StringBuilder possibleNodes = new StringBuilder();
        possibleNodes.append("用户问题：").append(question).append("\n");
        possibleNodes.append("结合知识图谱中【用户关心的节点】，猜测用户下一个可能的问题：\n");
        possibleNodes.append("请只输出3个可能的问题，以','为分割\n");
        possibleNodes.append("示例输出：[1. 问题1],[2. 问题2],[3. 问题3]\n\n");
        possibleNodes.append("【用户关心的节点】：\n");
        // 为每个节点寻找可能的邻居节点
        for (List<String> nameWeight: nodeInQuestion){
            String name = nameWeight.get(0);
            EagerResult relatedNeighbors = graphService.getNeighbors(name);
            String relatedNeighbor = "";   // 与节点相关的邻居
            for(Record neighbor: relatedNeighbors.records()){
                for(Value value: neighbor.values()){
                    relatedNeighbor = value.get("name").asString();
                    if(relatedNeighbor.equals("null")){
                        continue;
                    }
                    possibleNodes.append(relatedNeighbor).append(",");
                }
            }
        }
        System.out.println("Possible Nodes: "+possibleNodes.toString());
        // 生成可能的问题
        String possibleQuestion = llm_GLMClient.askClient(possibleNodes.toString(), Constants.ModelChatGLM4);
        if(possibleQuestion == null){
            System.out.println("possibleQuestion is null");
            return result;
        }
        String[] possibleQuestionList = possibleQuestion.split(",");
        for (String possibleQues: possibleQuestionList){
            if (count >= limit) break;
            int startIndex = possibleQues.indexOf('[') + 1;
            int endIndex = possibleQues.indexOf(']');
            String middleString = possibleQues.substring(startIndex, endIndex);
            result.add(middleString);
            count++;
        }
        // 打印
        System.out.println("Possible Questions: ");
        for (String ques: result){
            System.out.println(ques);
        }
        return result;
    }

    // 新增生成并执行SQL查询的方法
    @Async
    public void generateAndExecuteSQL(String question, String uuid, String username) {
        System.out.println("CurrentUser: " + username);

        // 获取当前用户对象
        User user = userRepository.findByUsername(username);
        if (user == null) {
            throw new RuntimeException("User not found");
        }

        // 获取或创建Conversation对象
        Optional<Conversation> conversationOptional = conversationRepository.findById(uuid);
        Conversation conversation;
        if (conversationOptional.isPresent()) {
            conversation = conversationOptional.get();
        } else {
            conversation = new Conversation();
            conversation.setId(uuid);
            conversation.setUser(user);
            conversation = conversationRepository.save(conversation);
        }
        // 调用Chat类生成SQL查询的JSON响应
        Chat chat1 = new Chat();
        // String jsonResponse = chat1.getAnswer_sql(question, "src/main/resources/Prompt/NL2SQL.txt");
        String jsonResponse = chat1.getAnswer_temp2(question, "src/main/resources/Prompt/NL2SQL.txt");
        System.out.println("jsonResponse: " + jsonResponse);

        // 从响应中提取SQL查询
        String sqlQuery = SQLExtractor.extractSQL(jsonResponse);
        System.out.println("SQL query: " + sqlQuery);

        // 检查sqlQuery是否为空或不合法
        if (sqlQuery.isEmpty() || !sqlQuery.trim().toLowerCase().startsWith("select") || !isSafeQuery(sqlQuery)) {
            String result = "生成的SQL查询为空或不合法。请确保您的提问是有效的自然语言请求，并且仅查询数据（例如，使用SELECT语句）。";
            System.out.println("result"+result);
            send(uuid, result, "SQL-result");

            // 保存QuestionAndAnswer对象
            QuestionAndAnswer qa = new QuestionAndAnswer();
            qa.setQuestion(question);
            qa.setAnswer(result);
            qa.setConversation(conversation);
            questionAndAnswerRepository.save(qa);
            return;
        }
        // 判断是否安全

        // 执行生成的SQL查询
        List<Map<String, Object>> results = executeSQLQuery(sqlQuery);
        if(results.isEmpty()){
            String result = "生成的SQL查询为空或不合法。请确保您的提问是有效的自然语言请求，并且仅查询数据（例如，使用SELECT语句）。";
            System.out.println("result"+result);
            send(uuid, result, "SQL-result");

            // 保存QuestionAndAnswer对象
            QuestionAndAnswer qa = new QuestionAndAnswer();
            qa.setQuestion(question);
            qa.setAnswer(result);
            qa.setConversation(conversation);
            questionAndAnswerRepository.save(qa);
            return;
        }
        String result = formatResultsAsText(results);
        // 这边再过一轮大模型
        Chat chat2 = new Chat();
        String final_response = chat2.getAnswer_sql("您提出的问题是： " + question + "我们系统给出的答案是：" + result,"src/main/resources/Prompt/SQL2NL.txt");


        System.out.println("SQL result: " + final_response);
        send(uuid, final_response, "SQL-result");

        // 保存QuestionAndAnswer对象
        QuestionAndAnswer qa = new QuestionAndAnswer();
        qa.setQuestion(question);
        qa.setAnswer(final_response);
        qa.setConversation(conversation);
        questionAndAnswerRepository.save(qa);
        // 将查询结果转换为字符串返回
    }

    private boolean isSafeQuery(String query) {
        String lowerQuery = query.toLowerCase();
        // 检查是否是SELECT查询
        if (!lowerQuery.startsWith("select")) {
            return false;
        }

        // 检查是否包含不允许的模式
        String[] disallowedPatterns = {"information_schema", "pg_catalog", "sqlite_master", "mysql"};
        for (String pattern : disallowedPatterns) {
            if (lowerQuery.contains(pattern)) {
                return false;
            }
        }

        return true;
    }



    private List<Map<String, Object>> executeSQLQuery(String query) {
        List<Map<String, Object>> results = new ArrayList<>();
        try (Connection connection = DriverManager.getConnection(dbUrl, dbUsername, dbPassword);
             Statement statement = connection.createStatement();
             ResultSet resultSet = statement.executeQuery(query)) {

            ResultSetMetaData metaData = resultSet.getMetaData();
            int columnCount = metaData.getColumnCount();

            while (resultSet.next()) {
                Map<String, Object> row = new HashMap<>();
                for (int i = 1; i <= columnCount; i++) {
                    row.put(metaData.getColumnName(i), resultSet.getObject(i));
                }
                results.add(row);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return results;
    }

    private String formatResultsAsText(List<Map<String, Object>> results) {
        StringBuilder sb = new StringBuilder();
        for (Map<String, Object> row : results) {
            sb.append(row.toString()).append("\n");
        }
        return sb.toString();
    }

    @Override
    public boolean addFeedback(String cvId, Integer attitude, String comment) {
        Optional<Conversation> cvOp = conversationRepository.findById(cvId);
        if (cvOp.isPresent()){

            Conversation cv = cvOp.get();
            String curLogin = StpUtil.getLoginIdAsString();
            if (!curLogin.equals(cv.getUser().getUsername())){
                return false;
            }
            List<QuestionAndAnswer> questionAndAnswers = cv.getQuestionAndAnswers();
            QuestionAndAnswer qa = questionAndAnswers.stream().max(Comparator.comparing(QuestionAndAnswer::getId))
                    .orElse(null);
            if (qa == null)return false;
            FeedbackPO fbp = new FeedbackPO();
            fbp.setId(qa.getId());
            fbp.setComment(comment == null ? "" : comment);
            fbp.setAttitude(attitude);
            feedbackRepository.save(fbp);
            return true;
        }
        return false;
    }

    private class CommentFrequency {
        private String comment;
        private int frequency;

        public CommentFrequency(String comment, int frequency) {
            this.comment = comment;
            this.frequency = frequency;
        }

        public String getComment() {
            return comment;
        }

        public void setComment(String comment) {
            this.comment = comment;
        }

        public int getFrequency() {
            return frequency;
        }

        public void setFrequency(int frequency) {
            this.frequency = frequency;
        }
    }

    private String getFeedbackSummarize(int topn){
        List<CommentFrequency> commentFrequencies = getCommentFrequency( topn);
        StringBuilder feedBackSummarize = new StringBuilder("此外，根据过往的问答结果，有以下高频的反馈内容：\n");
        int idx = 0;
        for (CommentFrequency cmf: commentFrequencies){
            if (idx > 0){
                feedBackSummarize.append("；\n");
            }
            if (cmf.comment.length() < 50) {
                feedBackSummarize.append(cmf.comment);
            } else {
                feedBackSummarize.append(cmf.comment, 0, 50).append("...");
            }
            idx++;
        }
        feedBackSummarize.append("请你注意上述反馈的内容，在这次的反馈中不要出现类似的问题");
        return feedBackSummarize.toString();
    }

    @Transactional(readOnly = true)
    public List<CommentFrequency> getCommentFrequency(int topN) {
        List<FeedbackPO> feedbackList = feedbackRepository.findAll();
        Map<String, Integer> commentFrequencyMap = new HashMap<>();

        for (FeedbackPO feedback : feedbackList) {
            String comment = feedback.getComment();
            if (comment != null && !comment.isEmpty()) {
                commentFrequencyMap.put(comment, commentFrequencyMap.getOrDefault(comment, 0) + 1);
            }
        }

        List<CommentFrequency> commentFrequencyList = commentFrequencyMap.entrySet().stream()
                .map(entry -> new CommentFrequency(entry.getKey(), entry.getValue()))
                .sorted((o1, o2) -> {
                    int freqCompare = o2.getFrequency() - o1.getFrequency();
                    return freqCompare != 0 ? freqCompare : o1.getComment().compareTo(o2.getComment());
                })
                .collect(Collectors.toList());

        return commentFrequencyList.size() > topN ? commentFrequencyList.subList(0, topN) : commentFrequencyList;
    }
}
