// Path: com/example/news_server/service/UserService.java

package com.example.news_server.service.impls;

import com.example.news_server.PO.User;
import com.example.news_server.VO.UserVO;
import com.example.news_server.repository.UserRepository;
import com.example.news_server.service.UserService;

import java.util.Collection;
import java.util.Collections;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements  UserService {

    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;

    @Autowired
    public UserServiceImpl(UserRepository userRepository, PasswordEncoder passwordEncoder) {
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
    }

    public User register(UserVO userVO) {
        // 需要应对用户已存在的情况
        User existingUser = userRepository.findByUsername(userVO.getUsername());
        if (existingUser != null) {
            // Handle user already exists scenario
            // You can throw an exception, return an error message, or take any other appropriate action
            // For example:
            throw new IllegalArgumentException("User already exists");
        }
        
        User user = new User();
        user.setUsername(userVO.getUsername());
        user.setPassword(passwordEncoder.encode(userVO.getPassword()));
        return userRepository.save(user);
    }

    public User findByUsername(String username) {
        return userRepository.findByUsername(username);
    }

    // @Override
    // public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
    //     User user = userRepository.findByUsername(username);
    //     if (user == null) {
    //         throw new UsernameNotFoundException("User not found");
    //     }
    //     return org.springframework.security.core.userdetails.User.withUsername(user.getUsername())
    //             .password(user.getPassword())
    //             .authorities("ROLE_USER")
    //             .build();
    // }

    //     @Override
    // public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
    //     // 查询数据库或其他存储系统以获取用户
    //     User user = userRepository.findByUsername(username);
    //     if (user == null) {
    //         throw new UsernameNotFoundException("User not found");
    //     }
    //     return new org.springframework.security.core.userdetails.User(
    //             user.getUsername(),
    //             user.getPassword(),
    //             getAuthorities(user)
    //     );
    // }

    @Override
    public boolean authenticate(UserVO userVO) {
        User user = userRepository.findByUsername(userVO.getUsername());
        return user != null && passwordEncoder.matches(userVO.getPassword(), user.getPassword());
    }

    @Override
    public User findById(Long id) {
        Optional<User>user = userRepository.findById(id);
        return user.orElse(null);
    }

    // // 定义getAuthorities方法来获取用户的权限
    // private Collection<? extends GrantedAuthority> getAuthorities(User user) {
    //     return Collections.singletonList(new SimpleGrantedAuthority("ROLE_USER"));
    // }
}
