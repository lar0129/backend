package com.example.news_server.service.impls;

import com.example.news_server.PO.Keyword;
import com.example.news_server.VO.NewsListVO;
import com.example.news_server.VO.NewsPageVO;
import com.example.news_server.VO.PatchNewsVO;
import com.example.news_server.VO.SummaryNews;
import com.example.news_server.repository.NewsRepository;
import com.example.news_server.PO.News;
import com.example.news_server.repository.NewsSpecification;
import com.example.news_server.service.NewsService;
import com.example.news_server.utils.classify.ClassifyModelUtils;
import com.example.news_server.utils.credibility.CredibilityLLMAnalysis;
import com.example.news_server.utils.enums.Category;
import com.example.news_server.utils.nlp.DataAnalysisUtils;
import com.example.news_server.utils.nlp.FuzzSearchUtils;
import com.hankcs.hanlp.HanLP;
import com.hankcs.hanlp.seg.Segment;
import com.hankcs.hanlp.seg.common.Term;
import jakarta.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.example.news_server.utils.nlp.FuzzSearchUtils.*;

@Service
public class NewsServiceImpl implements NewsService {
    private final NewsRepository newsRepository;

    @Autowired
    public NewsServiceImpl(NewsRepository newsRepo) {
        this.newsRepository = newsRepo;
    }

    @Override
    @Transactional
    public boolean deleteNewsById(Long id) {
        boolean success = true;
        try {
            newsRepository.deleteById(id);
        } catch (Exception e){
            System.err.println("Error when deleting news with id: " + id);
            success = false;
        }
        return success;
    }


    @Override
    public NewsPageVO getNewsPage(int size, int page, String source, Date startDate, Date endDate, String category) {
        Pageable pageable = PageRequest.of(page, size, Sort.by("publishDate").descending());
        if (endDate != null){
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(endDate);
            calendar.set(Calendar.HOUR_OF_DAY, 23);
            calendar.set(Calendar.MINUTE, 59);
            calendar.set(Calendar.SECOND, 59);
            calendar.set(Calendar.MILLISECOND, 999);
            endDate = calendar.getTime();
        }
        Specification<News> specification = Specification.where(NewsSpecification.isFromSource(source))
                .and(NewsSpecification.laterThanStart(startDate))
                .and(NewsSpecification.earlierThanEnd(endDate))
                .and(NewsSpecification.inCategory(category));
        Page<News> newsPage = newsRepository.findAll(specification, pageable);

        NewsPageVO newsPageVO = new NewsPageVO();
        if (!newsPage.hasContent()){
            newsPageVO.page = Collections.emptyList();
        } else {
            List<News> contents = newsPage.getContent();
            newsPageVO.page = getSummaryNewsList(contents);
        }
        newsPageVO.count = newsPage.getTotalPages();
        return newsPageVO;

    }

    @Override
    public NewsListVO getTimedNews(Date startDate, Date endDate) {
        Specification<News> specification = Specification.where(NewsSpecification.laterThanStart(startDate))
                .and(NewsSpecification.earlierThanEnd(endDate));
        List<News> newsList = newsRepository.findAll(specification);

        NewsListVO newsListVO = new NewsListVO();
        if (newsList.isEmpty()){
            newsListVO.list = Collections.emptyList();
        } else {
            List<News> contents = newsList;
            newsListVO.list = getSummaryNewsList(contents);
        }
        return newsListVO;
    }

    @Override
    public String getNewsContent(Long id) {
        Optional<News> news = newsRepository.findById(id);
        if (news.isEmpty()){
            return "";
        }
        return news.get().getContent();
    }

    @Override
    public List<SummaryNews> findNewsBySentence(String sentence, Long top){
        // Fix the sentence
        // 对搜索语句进行错误/近义处理
        String sentenceFixed = sentenceFix(sentence);
        System.out.println("Fixed sentence: " + sentenceFixed);

        List<News> newsList = new ArrayList<>();
        try {
            if (sentenceFixed.length() < MIN_LENGTH){
                // Search the news by word 短字符根据like进行全文搜索
                newsList = newsRepository.findNewsByTitleLike("%"+sentenceFixed+"%").subList(0,top.intValue());
            }
            else {
                // Search the news by sentence 根据SQL索引进行全文搜索
                newsList = newsRepository.findTitleBySentence(sentenceFixed, top);
                if (newsList.isEmpty()) {
                    System.out.println("Fixed sentence is not found, try to search with original sentence: " + sentence);
                    newsList = newsRepository.findTitleBySentence(sentence, top);
                }
                if (newsList.isEmpty()) {
                    System.out.println("title search failed, try to search with content: " + sentence);
                    newsList = newsRepository.findBySentence(sentence, top);
                }
            }

        }catch (Exception e){
            System.err.println("Error when searching news with sentence: " + sentence);
            return Collections.emptyList();
        }
        return getSummaryNewsList(newsList);
    }

    // 对搜索语句进行错误/近义NLP处理
    String sentenceFix(String sentence){
        if (sentence.length() < MIN_LENGTH){
            return sentence;
        }
        // 过滤停用词
        List<String> filteredTerms = FuzzSearchUtils.getInstance().filterStopWords(sentence);

        // 拼音转中文
        List<String> pinyinList = FuzzSearchUtils.getInstance().pinyinToChinese(filteredTerms);

        // 生成近义词
        List<String> synonymList = FuzzSearchUtils.getInstance().synonymProcess(pinyinList);
//        List<String> synonymList = pinyinList;
        // 拼接处理后的搜索语句
        StringBuilder sb = new StringBuilder();
        for (String word : synonymList) {
            sb.append(word).append(" ");
        }

        return sb.toString().trim();
    }



    @Override
    @Transactional
    public boolean alterNews(Long id, PatchNewsVO patchNewsVO) {
        Optional<News> newsOptional = newsRepository.findById(id);
        if (newsOptional.isEmpty()){
            return false;
        }
        News news = newsOptional.get();
        news.setTitle(patchNewsVO.getTitle());
        news.setContent(patchNewsVO.getContent());
        news.setPublishDate(patchNewsVO.getDate());
        news.setSource(patchNewsVO.getSource());
        news.setUrl(patchNewsVO.getUrl());
        news.setCategory(Category.parseString(patchNewsVO.getCategory()).getId());
        news.setKeywords(patchNewsVO.getKeywords());
        boolean success = true;
        try{
            newsRepository.save(news);
        } catch (Exception e){
            System.err.println("Alter news with id " + id + " and title " + patchNewsVO.getTitle() + " failed!");
            success = false;
        }
        return success;
    }

    @Override
    public String classifyNewsById(Long id){
        Optional<News> newsOptional = newsRepository.findById(id);
        if (newsOptional.isEmpty()){
            return null;
        }
        News news = newsOptional.get();
        int category = news.getCategory();
        if (category == -1 || category == 0){
            ClassifyModelUtils.newsClassify(news);
            newsRepository.save(news);
            return "Classify success as " + Category.parseInteger(news.getCategory()).getDescription();
        }
        else {
            return "News has been classified as " + Category.parseInteger(category).getDescription();
        }
    }

    @Override
    public void classifyLatestNews(int size, int page) {
        Page<News> latestNews = newsRepository.findAllByOrderByIdDesc(PageRequest.of(page, size));
        for (News news : latestNews) {
            if(news.getKeywords().isEmpty()){
                ClassifyModelUtils.newsClassify(news);
                newsRepository.save(news);
            }
        }
    }

    @Override
    public Double credibilityNewsById(Long id) {
        Optional<News> newsOptional = newsRepository.findById(id);
        if (newsOptional.isEmpty()){
            return null;
        }
        News news = newsOptional.get();
        Double credibility = CredibilityLLMAnalysis.getNewsCredibilityByNews(news);
        news.setCredibility(credibility);
        newsRepository.save(news);
        return credibility;
    }

    @Override
    public void credibilityLatestNews(int size, int page) {
        Page<News> latestNews = newsRepository.findAllByOrderByIdDesc(PageRequest.of(page, size));
        for (News news : latestNews) {
            if(news.getCredibility() != null) continue;
            Double credibility = CredibilityLLMAnalysis.getNewsCredibilityByNews(news);
            System.out.println("Credibility of news with id " + news.getId() + " is " + credibility);
            news.setCredibility(credibility);
            newsRepository.save(news);
        }
    }

    @Override
    public Boolean extractLatestNewsKeyWords(int size, int page) {
        Page<News> latestNews = newsRepository.findAllByOrderByIdDesc(PageRequest.of(page, size));
        for (News news : latestNews) {
//            if(!news.getKeywordList().isEmpty()) continue;
            List<String> keywords = DataAnalysisUtils.getInstance().extractKeyword(news.getTitle(),3);
            List<String> people = DataAnalysisUtils.getInstance().extractName(news.getTitle());
            List<String> places = DataAnalysisUtils.getInstance().extractPlace(news.getTitle());
            List<Keyword> keywordsSaved = getKeywords(keywords, 1, news.getPublishDate());
            List<Keyword> peopleSaved = getKeywords(people, 2, news.getPublishDate());
            List<Keyword> placesSaved = getKeywords(places, 3, news.getPublishDate());
            List<Keyword> aggregatedList = Stream.of(keywordsSaved, peopleSaved, placesSaved)
                    .flatMap(List::stream)
                    .toList();
            news.setKeywords(keywords.get(0));
            news.getKeywordList().clear();
            news.getKeywordList().addAll(aggregatedList);
            try {
                newsRepository.save(news);
            } catch (Exception e){
                System.err.println("Error when saving news with id: " + news.getId());
                return false;
            }
        }
        return true;
    }

    public List<Keyword> getKeywords(List<String> keywordList, int type, Date date) {
        List<Keyword> keywordSavedList = new ArrayList<>();
        for(String keyword : keywordList) {
            Keyword keywordSaved = new Keyword();
            keywordSaved.setKeyword(keyword);
            keywordSaved.setType(type);
            keywordSaved.setPublishDate(date);
            keywordSavedList.add(keywordSaved);
        }
        return keywordSavedList;
    }

    @Override
    public List<SummaryNews> findNewsByCategory(String category) {
        int categoryId = Category.parseString(category).getId();
        List<News> newsList = newsRepository.findByCategory(categoryId);
        return getSummaryNewsList(newsList);
    }

    private List<SummaryNews> getSummaryNewsList(List<News> newsList){
        List<SummaryNews> summaryNewsList = new ArrayList<>();
        for (News news : newsList) {
            SummaryNews summaryNews = new SummaryNews(news.getId(), news.getTitle(), news.getPublishDate(),
                    news.getSource(), news.getUrl(), Category.parseInteger(news.getCategory()).getDescription(), news.getKeywords(), news.getCredibility());
            summaryNewsList.add(summaryNews);
        }
        return summaryNewsList;
    }

    // 其他操作方法

}
