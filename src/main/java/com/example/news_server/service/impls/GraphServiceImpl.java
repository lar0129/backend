package com.example.news_server.service.impls;

import java.util.Date;
import java.text.SimpleDateFormat;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;

import org.neo4j.driver.Driver;
import org.neo4j.driver.EagerResult;
import org.neo4j.driver.Value;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import com.example.news_server.repository.EventRepository;
import com.example.news_server.repository.IdeaRepository;
import com.example.news_server.repository.MyNeo4jDriver;
import com.example.news_server.repository.OrganizationRepository;
import com.example.news_server.repository.PersonRepository;
import com.example.news_server.service.GraphService;
import com.example.news_server.utils.gsonSupport.GsonTimeSupport;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

@Service
public class GraphServiceImpl implements GraphService {

    private final PersonRepository personRepository;
    private final OrganizationRepository organizationRepository;
    private final IdeaRepository ideaRepository;
    private final EventRepository eventRepository;
    private final MyNeo4jDriver myNeo4jDriver;
    private Gson gson = new GsonBuilder().registerTypeAdapter(ZonedDateTime.class, new GsonTimeSupport()).create();

    public GraphServiceImpl(PersonRepository personRepository, OrganizationRepository organizationRepository,
            IdeaRepository ideaRepository, EventRepository eventRepository, MyNeo4jDriver myNeo4jDriver) {
        this.personRepository = personRepository;
        this.organizationRepository = organizationRepository;
        this.ideaRepository = ideaRepository;
        this.eventRepository = eventRepository;
        this.myNeo4jDriver = myNeo4jDriver;
    }

    @Override
    public String getGraphByNodeName(String nodeName) throws Exception {
        Driver driver = myNeo4jDriver.getDriver();
        String cypherQuery = "MATCH (n {name: $nodeName})-[r*0..3]-(m) "
                + "WITH collect(Distinct m) AS nodes, collect(Distinct r) AS relationship "
                + "Unwind relationship As nested " + "Unwind nested As rel "
                + "With nodes, collect(Distinct rel) As rels "
                + "RETURN nodes, rels " + "LIMIT 50";
        // 执行查询
        EagerResult result = driver.executableQuery(cypherQuery).withParameters(Map.of("nodeName", nodeName)).execute();
        List<Value> records = result.records().get(0).values();
        // Gson gson = new Gson();
        String jsonString = gson.toJson(records);
        return jsonString;
    }

    @Override
    public String getGraphByNodeNameLimited(String nodeName) throws Exception {
        Driver driver = myNeo4jDriver.getDriver();
        String cypherQuery = "MATCH (n {name: $nodeName})-[r*0..3]-(m) "
                + "WITH n, r, m "
                + "LIMIT 50 "
                + "WITH collect(Distinct m) AS nodes, collect(Distinct r) AS relationship "
                + "Unwind relationship As nested " + "Unwind nested As rel "
                + "With nodes, collect(Distinct rel) As rels "
                + "RETURN nodes, rels ";
        // 执行查询
        EagerResult result = driver.executableQuery(cypherQuery).withParameters(Map.of("nodeName", nodeName)).execute();
        List<Value> records = result.records().get(0).values();
        // Gson gson = new Gson();
        String jsonString = gson.toJson(records);
        return jsonString;
    }

    @Override
    public String getGraphByNewsId(String newsId) throws Exception {
        // 通过newsId获取图谱
        // cypher语句确实很强大，完全不是简单的一点抽象就能涵盖的
        // MATCH (n)-[r*0..3]-(m)
        // WHERE "184" IN n.related_news
        // WITH collect(Distinct m) AS nodes, Collect(Distinct r) AS relationship
        // Unwind relationship As nested
        // Unwind nested As rel
        // With nodes, Collect(Distinct rel) As rels
        // RETURN nodes, rels LIMIT 50
        Driver driver = myNeo4jDriver.getDriver();
        String cypherQuery = "MATCH (n)-[r*0..3]-(m) " +
                "WHERE $newsId IN n.related_news " +
                "WITH collect(Distinct m) AS nodes, collect(Distinct r) AS relationship " +
                "Unwind relationship As nested " +
                "Unwind nested As rel " +
                "With nodes, collect(Distinct rel) As rels " +
                "RETURN nodes, rels " +
                "LIMIT 50";
        // 执行查询
        EagerResult result = driver.executableQuery(cypherQuery).withParameters(Map.of("newsId", newsId)).execute();
        List<Value> records = result.records().get(0).values();
        // Gson gson = new GsonBuilder().registerTypeAdapter(ZonedDateTime.class, new
        // GsonTimeSupport()).create();
        String jsonString = gson.toJson(records);
        return jsonString;
    }

    @Override
    public String getGraphByNewsIdLimited(String newsId) throws Exception {
        Driver driver = myNeo4jDriver.getDriver();
        String cypherQuery = "MATCH (n)-[r*0..3]-(m) "
                + "WHERE $newsId IN n.related_news "
                + "WITH n, r, m "
                + "LIMIT 50 "
                + "WITH collect(Distinct m) AS nodes, collect(Distinct r) AS relationship "
                + "Unwind relationship As nested " + "Unwind nested As rel "
                + "With nodes, collect(Distinct rel) As rels "
                + "RETURN nodes, rels ";
        // 执行查询
        EagerResult result = driver.executableQuery(cypherQuery).withParameters(Map.of("newsId", newsId)).execute();
        List<Value> records = result.records().get(0).values();
        // Gson gson = new Gson();
        String jsonString = gson.toJson(records);
        return jsonString;
    }

    @Override
    public String getHotestPerson(String startTime, String endTime) throws Exception {
        // MATCH (n)-[r]-()
        // WITH n, count(r) AS degree
        // WHERE
        // all(x IN n.time WHERE x >= datetime('2024-04-16T16:54:21Z') AND x <=
        // datetime('2024-04-17T16:54:21Z'))
        // With n, degree
        // ORDER BY degree DESC
        // RETURN n, degree
        // LIMIT 1
        Driver driver = myNeo4jDriver.getDriver();
        String cypherQuery = "MATCH (n: Person)-[r]-() " +
                "WITH n, count(r) AS degree " +
                "WHERE any(x IN n.time WHERE x >= datetime($startTime) AND x <= datetime($endTime)) " +
                "With n, degree " +
                "ORDER BY degree DESC " +
                "RETURN n, degree " +
                "LIMIT 1";
        EagerResult result = driver.executableQuery(cypherQuery)
                .withParameters(Map.of("startTime", startTime, "endTime", endTime)).execute();
        List<Value> records = result.records().get(0).values();
        String jsonString = gson.toJson(records);
        return jsonString;
    }

    @Override
    public String getHotestEvent(String startTime, String endTime) throws Exception {
        Driver driver = myNeo4jDriver.getDriver();
        String cypherQuery = "MATCH (n: Event)-[r]-() " +
                "WITH n, count(r) AS degree " +
                "WHERE any(x IN n.time WHERE x >= datetime($startTime) AND x <= datetime($endTime)) " +
                "With n, degree " +
                "ORDER BY degree DESC " +
                "RETURN n, degree " +
                "LIMIT 1";
        EagerResult result = driver.executableQuery(cypherQuery)
                .withParameters(Map.of("startTime", startTime, "endTime", endTime)).execute();
        List<Value> records = result.records().get(0).values();
        String jsonString = gson.toJson(records);
        return jsonString;
    }

    @Override
    public String getHotestOrganization(String startTime, String endTime) throws Exception {
        Driver driver = myNeo4jDriver.getDriver();
        String cypherQuery = "MATCH (n: Organization)-[r]-() " +
                "WITH n, count(r) AS degree " +
                "WHERE any(x IN n.time WHERE x >= datetime($startTime) AND x <= datetime($endTime)) " +
                "With n, degree " +
                "ORDER BY degree DESC " +
                "RETURN n, degree " +
                "LIMIT 1";
        EagerResult result = driver.executableQuery(cypherQuery)
                .withParameters(Map.of("startTime", startTime, "endTime", endTime)).execute();
        List<Value> records = result.records().get(0).values();
        String jsonString = gson.toJson(records);
        return jsonString;
    }

    @Override
    public String getHotestIdea(String startTime, String endTime) throws Exception {
        Driver driver = myNeo4jDriver.getDriver();
        String cypherQuery = "MATCH (n: Idea)-[r]-() " +
                "WITH n, count(r) AS degree " +
                "WHERE any(x IN n.time WHERE x >= datetime($startTime) AND x <= datetime($endTime)) " +
                "With n, degree " +
                "ORDER BY degree DESC " +
                "RETURN n, degree " +
                "LIMIT 1";
        EagerResult result = driver.executableQuery(cypherQuery)
                .withParameters(Map.of("startTime", startTime, "endTime", endTime)).execute();
        List<Value> records = result.records().get(0).values();
        String jsonString = gson.toJson(records);
        return jsonString;
    }

    @Override
    public String getGraphByTime(String startTime, String endTime) throws Exception {
        Driver driver = myNeo4jDriver.getDriver();
        String cypherQuery = "MATCH (n)-[r*0..3]-(m) " +
                "WHERE any(x IN n.time WHERE x >= datetime($startTime) AND x <= datetime($endTime)) " +
                "WITH n, r, m " +
                "LIMIT 1000 " +
                "WITH collect(Distinct m) AS nodes, collect(Distinct r) AS relationship " +
                "Unwind relationship As nested " +
                "Unwind nested As rel " +
                "With nodes, collect(Distinct rel) As rels " +
                "RETURN nodes, rels ";
        EagerResult result = driver.executableQuery(cypherQuery)
                .withParameters(Map.of("startTime", startTime, "endTime", endTime)).execute();
        List<Value> records = result.records().get(0).values();
        String jsonString = gson.toJson(records);
        return jsonString;
    }

    @Override
    public List<Long> getRelatedNewsByNews(Long id) throws Exception {
        // MATCH (n)-[r]-(m) WHERE 184 IN n.related_news WITH m, r LIMIT 50 Unwind
        // m.related_news as related Return collect(DISTINCT related)
        Driver driver = myNeo4jDriver.getDriver();
        String cypherQuery = "MATCH (n)-[r]-(m) " +
                "WHERE $newsId IN n.related_news " +
                "WITH m " +
                "LIMIT 50 " +
                "Unwind m.related_news as related " +
                "RETURN collect(Distinct related)";
        EagerResult result = driver.executableQuery(cypherQuery).withParameters(Map.of("newsId", id)).execute();
        List<Object> relatedNews = result.records().get(0).values().get(0).asList();
        List<Long> parsedNewsIds = new ArrayList<>();
        for (Object news : relatedNews) {
            parsedNewsIds.add(Long.parseLong(news.toString()));
        }
        return parsedNewsIds;
    }

    @Override
    public Boolean pushNREV2(ArrayList<ArrayList<String>> nreData, Long id, Date date) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX", Locale.CHINA);
        boolean[] flag = { true }; // 这样做会有线程安全问题，但是我就是要改，so be it.
        nreData.forEach(nrelist -> {
            if (nrelist.size() != 5) {
                System.err.println("NRE data format error: Unwanted length " + nrelist.size());
            }
            try {
                String TypeA = nrelist.get(1);
                String TypeB = nrelist.get(4);
                String NameA = nrelist.get(0);
                String NameB = nrelist.get(3);
                double Strength = Double.parseDouble(nrelist.get(2));
                Driver driver = myNeo4jDriver.getDriver();
                String relationshipType = TypeA + "Effects" + TypeB;
                String cypherQueryWithDate = String.format("MERGE (n1:%s {name: $nodeName1}) " +
                        "MERGE (n2:%s {name: $nodeName2}) " +
                        "CREATE (n1)-[r:%s]->(n2) SET r.strength = $strength " +
                        "WITH n1, n2, $newsId AS newsId, $datetime AS date " +
                        "SET n1.related_news = CASE " +
                        "WHEN n1.related_news IS NULL THEN [newsId] " +
                        "ELSE n1.related_news + [newsId] " +
                        "END, " +
                        "n2.related_news = CASE " +
                        "WHEN n2.related_news IS NULL THEN [newsId] " +
                        "ELSE n2.related_news + [newsId] " +
                        "END," +
                        "n1.time = CASE " +
                        "WHEN n1.time IS NULL THEN [datetime(date)] " +
                        "ELSE n1.time + [datetime(date)] " +
                        "END," +
                        "n2.time = CASE " +
                        "WHEN n2.time IS NULL THEN [datetime(date)] " +
                        "ELSE n2.time + [datetime(date)] " +
                        "END", TypeA, TypeB, relationshipType);
                String cypherQueryWithoutDate = String.format("MERGE (n1:%s {name: $nodeName1}) " +
                        "MERGE (n2:%s {name: $nodeName2}) " +
                        "CREATE (n1)-[r:%s]->(n2) SET r.strength = $strength " +
                        "WITH n1, n2, $newsId AS newsId " +
                        "SET n1.related_news = CASE " +
                        "WHEN n1.related_news IS NULL THEN [newsId] " +
                        "ELSE n1.related_news + [newsId] " +
                        "END, " +
                        "n2.related_news = CASE " +
                        "WHEN n2.related_news IS NULL THEN [newsId] " +
                        "ELSE n2.related_news + [newsId] " +
                        "END", TypeA, TypeB, relationshipType);
                EagerResult result = null;
                if (date != null)
                    result = driver.executableQuery(cypherQueryWithDate)
                            .withParameters(Map.of("nodeName1", NameA, "nodeName2", NameB, "strength", Strength,
                                    "newsId", id.toString(), "datetime", dateFormat.format(date)))
                            .execute();
                else
                    result = driver.executableQuery(cypherQueryWithoutDate)
                            .withParameters(Map.of("TypeA", TypeA, "TypeB", TypeB, "nodeName1", NameA, "nodeName2",
                                    NameB, "relationshipType", relationshipType, "strength", Strength, "newsId",
                                    id.toString()))
                            .execute();
                // System.out.println(result.toString());

            } catch (Exception e) {
                System.err.println("NRE data inserting error: " + e.getMessage());
                flag[0] = false;
            }
        });
        System.out.println("NRE data: " + id + nreData.toString() + " proceeded.");
        return flag[0];
    }

    public List<Value> getGraphFromQAExtracted(List<List<String>> arr){

        // 1. 首先选取权重最高的五个实体（不满五个就直接用全部）
        ArrayList<String> selectedEntities = new ArrayList<>();
        arr.sort((a, b) -> Double.compare(Double.parseDouble(b.get(1)), Double.parseDouble(a.get(1))));
        for(int i = 0; i < 5 && i < arr.size(); i++){
            selectedEntities.add(arr.get(i).get(0));
        }

        /*
         * 咱们采用最短路径查询
         * WITH ['俄罗斯', '美国', '中国'] AS inputNodes
         * UNWIND inputNodes AS startNode
         * UNWIND inputNodes AS endNode
         * WITH startNode, endNode
         * WHERE startNode <> endNode
         * MATCH (start {name: startNode}), (end {name: endNode}),
         * path = shortestPath((start)-[*]-(end))
         * WITH collect(path) AS paths
         * UNWIND paths AS p
         * UNWIND nodes(p) AS n
         * UNWIND relationships(p) AS r
         * WITH DISTINCT n, apoc.coll.toSet(n.related_news) AS unique_related_news
         * RETURN collect(DISTINCT n) AS nodes, 
         *       collect(DISTINCT r) AS edges, 
         *       collect(DISTINCT {node: n.name, related_news: unique_related_news}) AS node_related_news
         */

        // 2. 构造cypher查询语句
        String cypherQuery = "WITH $nodes AS nodes " +
        "UNWIND nodes AS startNode " +
        "UNWIND nodes AS endNode " +
        "WITH startNode, endNode " +
        "WHERE startNode <> endNode " +
        "MATCH (start {name: startNode}), (end {name: endNode}), " +
        "path = shortestPath((start)-[*]-(end)) " +
        "WITH collect(path) AS paths " +
        "UNWIND paths AS p " +
        "UNWIND nodes(p) AS n " +
        "UNWIND relationships(p) AS r " +
        "WITH DISTINCT n, r, apoc.coll.toSet(n.related_news) AS unique_related_news " + 
        "RETURN collect(DISTINCT n) AS nodes, collect(DISTINCT r) AS edges, collect(DISTINCT {node: n.name, related_news: unique_related_news}) AS node_related_news";
        Driver driver = myNeo4jDriver.getDriver();
        EagerResult result = driver.executableQuery(cypherQuery).withParameters(Map.of("nodes", selectedEntities)).execute();
        List<Value> records = result.records().get(0).values();
//        for(Value val: records){
//            System.err.println(val);
//        }
        return records;
    }

    // 查找某个节点周围最近的5个节点
    @Override
    public EagerResult getNeighbors(String nodeName) {
        // 1. 构造cypher查询语句
        String cypherQuery = "MATCH (n)\n" +
                "WHERE (n:Organization OR n:Person) AND n.name CONTAINS '" + nodeName+
                "'\n" +
                "WITH n\n" +
                "MATCH (n)-[r]-(neighbor)\n" +
                "WHERE (neighbor:Organization OR neighbor:Person) AND r.strength IS NOT NULL\n" +
                "RETURN n AS node, neighbor, r\n" +
                "ORDER BY r.strength DESC\n" +
                "LIMIT 5";
        Driver driver = myNeo4jDriver.getDriver();
        EagerResult result = driver.executableQuery(cypherQuery).withParameters(Map.of("nodeName", nodeName)).execute();
        return result;
    }
}
