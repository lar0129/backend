package com.example.news_server.service.impls;

import com.example.news_server.PO.Keyword;
import com.example.news_server.VO.CrawledNewsVO;
import com.example.news_server.repository.KeywordRepository;
import com.example.news_server.repository.NewsRepository;
import com.example.news_server.PO.News;
import com.example.news_server.service.CrawlerService;
import com.example.news_server.service.SseService;
import com.example.news_server.utils.classify.ClassifyModelUtils;
import com.example.news_server.utils.credibility.CredibilityLLMAnalysis;
import com.example.news_server.utils.enums.Category;
import com.example.news_server.utils.nlp.DataAnalysisUtils;
import com.google.gson.Gson;

import jakarta.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
public class CrawlerServiceImpl implements CrawlerService {
    private final NewsRepository newsRepository;
    private final KeywordRepository keywordRepository;
    @Autowired
    CrawlerServiceImpl(NewsRepository newsRepo, KeywordRepository keywordRepository){
        this.newsRepository = newsRepo;
        this.keywordRepository = keywordRepository;
    }

    @Override
    public List<News> addNews(List<CrawledNewsVO> newsVOList) {
        List<News> newsList = new ArrayList<>();
        for (CrawledNewsVO newsVO : newsVOList) {
            News newsSaved = new News();
            newsSaved.setSource(newsVO.getSource());
            newsSaved.setContent(newsVO.getContent());
            newsSaved.setTitle(newsVO.getTitle());
            newsSaved.setPublishDate(newsVO.getDate());
            newsSaved.setUrl(newsVO.getUrl());
            if (newsVO.getCategory() != null) {
                newsSaved.setCategory(Category.parseString(newsVO.getCategory()).getId());
            } else {
                ClassifyModelUtils.newsClassify(newsSaved);
            }

            // 传统方法提取关键词，存在keyword表中
            // Gson gson = new Gson();
            // System.out.println(gson.toJson(newsVO)); 
            List<String> keywords = DataAnalysisUtils.getInstance().extractKeyword(newsVO.getTitle(),3);
            if(keywords.size() == 0){
                keywords.add("暂无关键词");
            }
            List<String> people = DataAnalysisUtils.getInstance().extractName(newsVO.getTitle());
            List<String> places = DataAnalysisUtils.getInstance().extractPlace(newsVO.getTitle());
            List<Keyword> keywordsSaved = getKeywords(keywords, 1, newsVO.getDate());
            List<Keyword> peopleSaved = getKeywords(people, 2, newsVO.getDate());
            List<Keyword> placesSaved = getKeywords(places, 3, newsVO.getDate());
            List<Keyword> aggregatedList = Stream.of(keywordsSaved, peopleSaved, placesSaved)
                    .flatMap(List::stream)
                    .collect(Collectors.toList());

            newsSaved.setKeywords(keywords.get(0));
            newsSaved.setKeywordList(aggregatedList);
            // 提取新闻可信度
//            Double credibility = CredibilityLLMAnalysis.getNewsCredibilityByNews(newsSaved);
//            newsSaved.setCredibility(credibility);

            newsList.add(newsSaved);
        }

        return saveAll(newsList);
    }

    public List<News> saveAll(List<News> newsList) {
        int count = newsList.size();
        List<News> newsListSaved = new ArrayList<>();
        if (!trySaveAll(newsList, newsListSaved)){
            System.err.println("Try inserting independently");
            for (News news : newsList) {
                if (!trySaveOne(news, newsListSaved)){
                    count--;
                }
            }
        }
        return newsListSaved;

    }


    @Transactional
    private boolean trySaveAll(List<News> newsList, List<News> newsListSaved){
        boolean success = true;
        try{
            newsListSaved.addAll(newsRepository.saveAll(newsList)); // 尝试批量插入
        } catch (Exception e){
            System.err.println("insert all with duplicate data!");
            success = false;
        }

        return success;

    }

    @Transactional
    private boolean trySaveOne(News news, List<News> newsListSaved){
        boolean success = true;
        try {
            newsListSaved.add(newsRepository.save(news)); // 尝试批量插入
        } catch (DataIntegrityViolationException e) {
            System.err.println("News with title: " + news.getTitle() + " duplicated!");
            success = false;
        }
        return success;
    }

    public List<Keyword> getKeywords(List<String> keywordList, int type, Date date) {
        List<Keyword> keywordSavedList = new ArrayList<>();
        for(String keyword : keywordList) {
            Keyword keywordSaved = new Keyword();
            keywordSaved.setKeyword(keyword);
            keywordSaved.setType(type);
            keywordSaved.setPublishDate(date);
            keywordSavedList.add(keywordSaved);
        }
        return keywordSavedList;
    }

//    public void sendMessage(List<News> newsList) {
//        sseService.sendUpdateNews(newsList);
//        sseService.sendUpdateDailyAmount();
//    }
}
