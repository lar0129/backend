package com.example.news_server.service.impls;

import com.example.news_server.PO.ErrorId;
import com.example.news_server.repository.ErrorIdRepository;
import com.example.news_server.service.ErrorDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ErrorDataServiceImpl implements ErrorDataService {

    private final ErrorIdRepository errorIdRepository;

    @Autowired
    public ErrorDataServiceImpl(ErrorIdRepository errorIdRepository) {
        this.errorIdRepository = errorIdRepository;
    }

    @Override
    public long getErrorDataCount() {
        return errorIdRepository.count();
    }

    @Override
    public void saveErrorId(String errorId) {
        ErrorId error = new ErrorId();
        error.setErrorId(errorId);
        errorIdRepository.save(error);
    }

    @Override
    public List<ErrorId> getAllErrorIds() {
        return (List<ErrorId>) errorIdRepository.findAll();
    }

    // 其他方法的实现...
}
