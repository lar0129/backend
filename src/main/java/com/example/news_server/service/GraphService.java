package com.example.news_server.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.neo4j.driver.EagerResult;
import org.neo4j.driver.Value;
import org.springframework.scheduling.annotation.Async;

public interface GraphService {

    // @Async
    // public CompletableFuture<Boolean>  pushNRE(ArrayList<ArrayList<String>> nreData, Long id);

    // @Async
    // public CompletableFuture<Boolean> pushNRE(ArrayList<ArrayList<String>> nreData, Long id, Date date);

    public Boolean pushNREV2(ArrayList<ArrayList<String>> nreData, Long id, Date date);

    public String getGraphByNewsId(String newsId) throws Exception;

    public String getGraphByNewsIdLimited(String newsId) throws Exception;

    public String getGraphByNodeName(String nodeName) throws Exception;

    public String getGraphByNodeNameLimited(String nodeName) throws Exception;

    public String getHotestPerson(String startTime, String endTime) throws Exception;

    public String getHotestEvent(String startTime, String endTime) throws Exception;

    public String getHotestOrganization(String startTime, String endTime) throws Exception;

    public String getHotestIdea(String startTime, String endTime) throws Exception;

    public String getGraphByTime(String startTime, String endTime) throws Exception;

    public List<Long> getRelatedNewsByNews(Long id) throws Exception;

    public List<Value> getGraphFromQAExtracted(List<List<String>> arr);

    public EagerResult getNeighbors(String nodeName);
    
}
