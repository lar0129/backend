package com.example.news_server.service;

import com.example.news_server.VO.data.KeywordAnalysisVO;
import com.example.news_server.VO.data.SummaryNewsAmountVO;
import com.example.news_server.VO.data.SummaryPlacesVO;

import java.util.Date;
import java.util.List;

public interface DataService {
    // 以下为数据分析
    Integer getNewsAmountData(Date startDate, Date endDate, String category);

    List<SummaryNewsAmountVO> getAllNewsAmountData(Date startDate, Date endDate);

    // 获取一周内最热关键词
    KeywordAnalysisVO getHotWordsData(Date startDate, Date endDate,Integer type);

    KeywordAnalysisVO getHotEventsByNeo4j(Date startDate, Date endDate);

    KeywordAnalysisVO getHotPersonByNeo4j(Date startDate, Date endDate);

    List<SummaryPlacesVO> getPlacesData(Date startDate, Date endDate);
}
