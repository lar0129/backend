package com.example.news_server.service;

import com.example.news_server.PO.News;
import com.example.news_server.VO.CrawledNewsVO;
import org.springframework.stereotype.Service;

import java.util.List;

public interface CrawlerService {
    List<News> addNews(List<CrawledNewsVO> newsVOList);
}
