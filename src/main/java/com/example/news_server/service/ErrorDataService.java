package com.example.news_server.service;

import com.example.news_server.PO.ErrorId;

import java.util.List;

public interface ErrorDataService {
    long getErrorDataCount();

    void saveErrorId(String errorId);

    List<ErrorId> getAllErrorIds();
}
