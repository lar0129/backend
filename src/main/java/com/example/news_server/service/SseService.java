package com.example.news_server.service;

import com.example.news_server.PO.News;
import org.springframework.web.servlet.mvc.method.annotation.SseEmitter;

import java.util.List;

public interface SseService {
    SseEmitter connectNewsSSE(String uuid);
    SseEmitter connectDailyAmountSSE(String uuid);

    void sendMessage(String message);

    void sendUpdateNews();

    void sendUpdateDailyAmount();
}