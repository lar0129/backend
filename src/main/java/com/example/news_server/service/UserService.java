package com.example.news_server.service;

import com.example.news_server.PO.User;
import com.example.news_server.VO.UserVO;

public interface UserService {
    
    public User register(UserVO userVO);
    public User findByUsername(String username);
    public boolean authenticate(UserVO userVO);

    public User findById(Long id);
}
