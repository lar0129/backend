package com.example.news_server.VO;

import lombok.Getter;

import java.util.Date;

public class PatchNewsVO {

    @Getter
    String title;
    @Getter
    String content;
    @Getter
    Date date;

    @Getter
    String source;

    @Getter
    String url;

    @Getter
    String category;

    @Getter
    String keywords;

}
