package com.example.news_server.VO;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Date;

@AllArgsConstructor
public class SummaryNews {
    @Getter
    Long id;
    @Getter
    String title;
    @Getter
    Date date;
    @Getter
    String source;
    @Getter
    String url;
    @Getter
    String category;
    @Getter
    String keywords;
    @Getter
    Double credibility;

}
