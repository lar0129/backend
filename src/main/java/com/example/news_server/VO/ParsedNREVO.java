package com.example.news_server.VO;

import java.util.Date;
import java.util.ArrayList;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class ParsedNREVO {

    @JsonProperty("data")
    private ArrayList<ArrayList<String>> data;

    @JsonProperty("id")
    private Long id;

    @JsonProperty("date")
    private Date date;
    
}
