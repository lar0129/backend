package com.example.news_server.VO;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter // 如果您需要设置值
public class CrawledNewsVO {
    @JsonProperty("news_title")
    private String title;

    @JsonProperty("news_source")
    private String source;

    @JsonProperty("news_time")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    private Date date;

    @JsonProperty("news_content")
    private String content;

    @JsonProperty("news_url")
    private String url;

    @JsonProperty("category")
    private String category;
}


/*
  deprecated class def, which may be removed in the future
  */
//public class CrawledNewsVO {
//    @Getter
//    @CsvBindByName(column = "news_title")
//    String title;
//    @Getter
//    @CsvBindByName(column = "news_source")
//    String source;
//    @Getter
//    @CsvBindByName(column = "news_time")
//    @CsvDate("yyyy-MM-dd HH:mm:ss") // 指定日期格式
//    Date date;
//    @Getter
//    @CsvBindByName(column = "news_content")
//    String content;
//    @Getter
//    @CsvBindByName(column = "news_url")
//    String url;
//}
