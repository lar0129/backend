package com.example.news_server.VO.data;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
public class ConversationFeedbackVO {
    @Getter
    private String uuid;

    @Getter
    private Integer attitude;

    @Getter
    private String comment;
}
