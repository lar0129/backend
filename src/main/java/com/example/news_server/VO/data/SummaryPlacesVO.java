package com.example.news_server.VO.data;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class SummaryPlacesVO {
    String placeName;
    Long count;
    Double longitude;
    Double latitude;
}