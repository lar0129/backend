package com.example.news_server.VO.data;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@AllArgsConstructor
@Getter
@Setter
public class NewsAmountVO {
    Integer amount;
    Date date;
}
