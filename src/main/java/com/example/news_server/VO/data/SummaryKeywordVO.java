package com.example.news_server.VO.data;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
public class SummaryKeywordVO {
    @Getter
    String keywordName;
    @Getter
    Long count;
}
