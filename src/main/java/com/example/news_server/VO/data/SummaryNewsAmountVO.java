package com.example.news_server.VO.data;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@AllArgsConstructor
@Getter
@Setter
public class SummaryNewsAmountVO {
    List<NewsAmountVO> newsAmount;
    String category;
    Long total;
}
