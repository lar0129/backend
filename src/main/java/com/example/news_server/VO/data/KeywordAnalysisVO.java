package com.example.news_server.VO.data;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Date;
import java.util.List;

@AllArgsConstructor
public class KeywordAnalysisVO {
    @Getter
    List<SummaryKeywordVO> keywords;
    @Getter
    Long total;
    @Getter
    String type;
    @Getter
    Date AnalysisDate; // 从该日期往前分析N日的数据
}
