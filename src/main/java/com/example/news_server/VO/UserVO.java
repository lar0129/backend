package com.example.news_server.VO;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
public class UserVO {
    @Getter
    String username;

    @Getter
    String password;
}
