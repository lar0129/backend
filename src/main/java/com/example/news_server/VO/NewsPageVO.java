package com.example.news_server.VO;

import java.util.List;

public class NewsPageVO {
    public List<SummaryNews> page;
    public int count;
}
