package com.example.news_server.VO;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class QuestionAndAnswerVO {
    private Long id;
    private String question;
    private String answer;

    public QuestionAndAnswerVO(Long id, String question, String answer) {
        this.id = id;
        this.question = question;
        this.answer = answer;
    }

}
