package com.example.news_server.VO;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class QuestionVO {
    String question;
    String uuid;
}
