package com.example.news_server;

import org.neo4j.cypherdsl.core.renderer.Configuration;
import org.neo4j.cypherdsl.core.renderer.Dialect;
import org.neo4j.driver.Driver;
import org.neo4j.driver.GraphDatabase;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.neo4j.Neo4jProperties;
import org.springframework.context.annotation.Bean;
// import org.springframework.context.annotation.Configuration;
import org.springframework.data.neo4j.core.Neo4jClient;
import org.springframework.data.neo4j.repository.config.EnableNeo4jRepositories;
@org.springframework.context.annotation.Configuration
@EnableNeo4jRepositories(basePackages = "com.example.news_server")
public class Neo4jConfig {

    @Autowired
    private Neo4jProperties neo4jProperties;

    // @Bean
    // Neo4jClient neo4jClient() {
    //     Driver driver = GraphDatabase.driver(neo4jProperties.getUri());
    //     return Neo4jClient.create(driver);
    // }

    @Bean
    Configuration cypherDslConfiguration() {
        return Configuration.newConfig()
                .withDialect(Dialect.NEO4J_5).build();
    }
}
